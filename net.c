#include <endian.h>
#include "ft_ping.h"

/*
	convert value from host to network byte order
*/
uint16_t ft_htons(uint16_t n) {
	#if BYTE_ORDER == BIG_ENDIAN
		return (n);
	#else
		return ((uint16_t) ((((n) >> 8) & 0xff) | (((n) & 0xff) << 8)));
	#endif
}

/*
	convert network from host to value byte order
*/
uint16_t ft_ntohs(uint16_t n) {
	#if BYTE_ORDER == BIG_ENDIAN
		return (n);
	#else
		return ((uint16_t) ((((n) >> 8) & 0xff) | (((n) & 0xff) << 8)));
	#endif
}