#include "ft_ping.h"

/*
	configure socket opts at ip level
*/
int	configure_socket(int sock_fd) {
	const uint8_t	ttl_value = pvar.opts.ttl;

	if (setsockopt(sock_fd, SOL_IP, IP_TTL, &ttl_value, sizeof(uint8_t)) == -1) {
		perror("configure socket1: ");
		exit(EXIT_FAILURE);
	}
	return 0;
}

/*
	Send ping
	Configure icmp packet, send packet, treat packet as sended
	handles timeout and interval calling alarm.
*/
int send_ping(void) {
	static uint16_t sequence = 1;
	struct sockaddr *addr_target = (struct sockaddr *)pvar.dom_info.addrinf->ai_addr;
	t_icmp  icmp = {0};
	int sended;

	configure_icmp_echo(&icmp, sequence++);
	alarm(1);
	errno = 0;
	if ((sended = sendto(pvar.sockfd, &icmp, ICMP_MINLEN + pvar.opts.data_size, 0,
	addr_target, sizeof(struct sockaddr))) <= 0) {
		perror("error when sending packet: ");
		return PINGR_ERR;
	}
	pvar.stats.pckt_sended++;
	return 0;
}


void handle_catch_ping(int sig) {
	if (sig == SIGALRM) {
		if (pvar.opts.outstanding) {
			const uint32_t npackets = pvar.stats.pckt_sended;
			if (pvar.stats.pckt_last_received < npackets)
				printf("no answer yet for icmp_seq=%u\n", npackets);
		}
		if (pvar.opts.count) {
			if (pvar.stats.pckt_sended == pvar.opts.count) {
				terminate();
				return ;
			}
		}
		send_ping();
	}
}

/*
	collect and update global stats based on a new ping result
*/
void collect_stats(t_ping_result *res) {
	const float rtt = get_elapsed_time_ms(res->rtt_time);
	if (pvar.stats.pckt_received == 1) {
		pvar.stats.min = rtt;
		pvar.stats.max = rtt;
	} else {
		if (rtt > pvar.stats.max)
			pvar.stats.max = rtt;
		if (rtt < pvar.stats.min)
			pvar.stats.min = rtt;
	}
	pvar.stats.sum += rtt;
	pvar.stats.sum2 += (rtt * rtt);
}

/*
	catch, treat, and filter icmp packets before displays
*/
int catch_ping(void) {
	t_ping_result result;
	struct sockaddr addr_target;
	struct msghdr msg = {0};
	struct iovec iov[5];
	struct timeval start_time = {0};
	struct s_icmp_data data = {0};

	iov[0].iov_base = &result.iphdr;
	iov[0].iov_len = sizeof(struct ip);
	iov[1].iov_base = &result.icmphdr;
	iov[1].iov_len = sizeof(struct icmphdr);
	iov[2].iov_base = &data;
	iov[2].iov_len = sizeof(struct s_icmp_data);

	msg.msg_iov = iov;
	msg.msg_iovlen = 3;
	msg.msg_name = (struct sockaddr *)&addr_target;
	msg.msg_namelen = sizeof(struct sockaddr);

	if (recvmsg(pvar.sockfd, &msg, 0) <= 0) {
		if (errno == ETIMEDOUT) {
			printf("packet timedout\n");
			return (PINGR_TIMEO);
		} else {
			perror("critical error when listening for packet: ");
			return (PINGR_ERR);
		}
	}

	// store from destination TTL
	result.ttl = result.iphdr.ip_ttl;

	if (result.icmphdr.type == ICMP_ECHO)
		return 0;

	if (result.icmphdr.type == ICMP_ECHOREPLY) {

		// ensure echo reply targeting our ping instance
		if((uint16_t)ft_ntohs(result.icmphdr.un.echo.id) != (uint16_t)getpid()) {
			return 0;
		}

		// compute rtt is possible
		if(pvar.opts.compute_rtt) {
			ft_memcpy(&start_time, &data.un.data[0], sizeof(struct timeval));
			gettimeofday(&result.rtt_time, NULL);
			tvsub(&result.rtt_time, &start_time);
			collect_stats(&result);
		}

		// increment packet received only if echoreply identifier is
		// the same as the last echo sended
		uint16_t seq = ft_ntohs(result.icmphdr.un.echo.sequence);
		pvar.stats.pckt_last_received = seq;
		result.sequence = seq;
		if (seq <= pvar.stats.pckt_sended) {
			pvar.stats.pckt_received += 1;
		}
	} else {
		// as for echo reply, ensure error packet targeting our ping instance
		// we check the identifier in the source echo packet contained by
		// data portion
		if((uint16_t)ft_ntohs(data.un.origin.icmphdr.un.echo.id) != (uint16_t)getpid()) {
			return 0;
		}

		// retrieve packet sequence by getting origin packet sequence.
		// all icmp error packets returns in their in queue
		// almost the original ip header and icmp header
		result.sequence = ft_ntohs(data.un.origin.icmphdr.un.echo.sequence);
		pvar.stats.errors += 1;
	}

	if (!pvar.opts.quiet) {
		display_ping_stat(&result);
	}
	return 0;
}

/*
	just the ping loop (see ping loop diagram)
*/
void handle_ping_loop() {
	signal(SIGALRM, handle_catch_ping);
	send_ping();
	for (;;) {
		catch_ping();
	}
}