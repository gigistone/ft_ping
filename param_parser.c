#include "ft_ping.h"

#define PARSER_WIDTH_ERR "err in parser verify width\n"

/*
	check width of a value
	if width is not correct
	an error is shown and entire program exit
*/
static int verify_value_width(int value, int valtype, const char *ascii) {
	int max_val = -1;
	int min_val = 1;

	switch(valtype) {
		case POPT_TTL: {
			max_val = TTL_MAX;
			break;
		}
		case POPT_COUNT: {
			max_val = INT32_MAX;
			break;
		}
		case POPT_PCKT_SIZE: {
			min_val = PACKET_SIZE_MIN;
			max_val = PACKET_SIZE_MAX;
			break;
		}
		default: {
			fprintf(stderr, ""PARSER_WIDTH_ERR"");
			exit(EXIT_FAILURE);
		}
	}
	if (value < min_val || value > max_val) {
			fprintf(stderr,
				""PING_ARG_ERR_PREFIX" '%s': out of range: %i <= value <= %i\n",
				ascii, min_val, max_val);
			exit(EXIT_FAILURE);
	}
	return (0);
}

/*
	parse value of an option
	return error if no line found
	or if value is in bad format
*/
static int parse_value(const char *val, int valtype) {
	int res;
	char *endptr;

	errno = 0;
	res = ft_strtol(val, &endptr, 10);
	if (errno != 0 || *endptr != '\0') {
		fprintf(stderr, ""PING_ARG_ERR_PREFIX" '%s'\n", val);
		exit(PING_EXIT_ARG_FAILURE);
	}
	verify_value_width(res, valtype, val);
	switch(valtype) {
		case POPT_TTL: {
			pvar.opts.ttl = (uint8_t)res;
			break;
		}
		case POPT_PCKT_SIZE: {
			pvar.opts.data_size = (uint16_t)res;
			break;
		}
		case POPT_COUNT: {
			pvar.opts.count = (uint32_t)res;
			break;
		}
		default: return (-1);
	}
	return (0);
}

/*
	read value option recursivly
	if value is inline, try to parse
	next line.
	return -1 on error
*/
static int read_value_opt(const char **params, char *cur_opt, int valtype, int multiline) {
	if (*params == NULL) {
			fprintf(stderr, ""PROG_NAME": option requires an argument -- '%c'\n", valtype);
			display_help();
			exit(PING_EXIT_OTHER_FAILURE);
		return -1;
	}
	if (multiline == 1)
		cur_opt = (char *)*params;
	else if (multiline == 0 && *(cur_opt) == '\0')
		return read_value_opt(params + 1, NULL, valtype, 1);
	if (parse_value(cur_opt, valtype) == -1)
		return (-1);
	return (multiline == 0 ? 1 : 2);
}

/*
	parse one or more options
	if an option has a value, try to
	parse it.
	value options are inline or multiline
	return the number of lines read
	return -1 on error
*/
static int parse_opts(const char **params) {
	const char *s = *(params) + 1;
	while (*s) {
		if (*s == POPT_VERBOSE)
			pvar.opts.verbose = 1;
		else if (*s == POPT_HELP)
			pvar.opts.show_help = 1;
		else if (*s == POPT_OUTSTAND)
			pvar.opts.outstanding = 1;
		else if (*s == POPT_QUIET)
			pvar.opts.quiet = 1;
		else if (*s == POPT_PCKT_SIZE)
			return (read_value_opt(params, (char *)(s + 1), POPT_PCKT_SIZE, 0));
		else if (*s == POPT_TTL)
			return (read_value_opt(params, (char *)(s + 1), POPT_TTL, 0));
		else if (*s == POPT_COUNT)
			return (read_value_opt(params, (char *)(s + 1), POPT_COUNT, 0));
		else {
			fprintf(stderr, ""PROG_NAME": invalid option -- '%c'\n", *s);
			display_help();
			exit(PING_EXIT_OTHER_FAILURE);
			return (-1);
		}
		s++;
	}
	return (1);
}

/*
	parse all params and initialise the t_ping_opt struct
	with all options parsed.
	@param params the array containing params
	@param len the length of the array
	@return Result of parse process
*/
enum e_parse_result parse_params(const char **params, size_t len)
{
	size_t	i = 0;
	int	read_line;

	while (i < len)
	{
		if (params[i][0] == '-')
		{
			if ((read_line = parse_opts(params + i)) == -1)
				return (PARSE_RES_ERROR);
			i += read_line;
		}
		else {
			pvar.opts.domain = (char *)params[i];
			i++;
		}
	}
	if (pvar.opts.show_help)
		return PARSE_RES_ERROR;
	if (pvar.opts.domain == NULL)
		return (PARSE_RES_NO_DEST);
	if (pvar.opts.data_size < sizeof(struct timeval))
		pvar.opts.compute_rtt = 0;
	return (PARSE_RES_OK);
}
