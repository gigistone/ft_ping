/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_stdio.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 19:40:45 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/06 20:22:21 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_STDIO_H

# define LFT_STDIO_H

void	ft_putchar(char c);
void	ft_putstr(const char *str);
void	ft_putendl(const char *str);
void	ft_putnbr(int nbr);

void	ft_putchar_fd(char c, int fd);
void	ft_putstr_fd(const char *str, int fd);
void	ft_putendl_fd(const char *str, int fd);
void	ft_putnbr_fd(int nbr, int fd);

int		get_next_line(int fd, char **line);

int		ft_printf_fd(const char *fmt, int fd, ...);

#endif
