/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_ctype.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 22:11:17 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 01:29:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_CTYPE_H

# define LFT_CTYPE_H

int	ft_islower(int c);
int	ft_isupper(int c);
int	ft_isalpha(int c);

int	ft_isdigit(int c);
int	ft_isalnum(int c);
int	ft_ispunct(int c);

int	ft_isspace(int c);
int	ft_isblank(int c);

int	ft_isgraph(int c);
int	ft_isprint(int c);

int	ft_iscntrl(int c);
int	ft_isascii(int c);

int	ft_toupper(int c);
int	ft_tolower(int c);

#endif
