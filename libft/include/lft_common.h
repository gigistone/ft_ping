/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_common.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:03:40 by lperson-          #+#    #+#             */
/*   Updated: 2020/10/22 12:53:54 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_COMMON_H

# define LFT_COMMON_H

# include <stddef.h>

typedef struct	s_vector {
	void	**data;
	size_t	len;
	size_t	capacity;
}				t_vector;

size_t			ft_str_array_len(char *strs[]);
char			**ft_str_array_cpy(char *dst[], char *src[]);
char			**ft_str_array_dup(char *strs[]);
int				ft_str_array_append(char **strs[], const char *str);
char			**ft_str_array_join(char *strs0[], char *strs1[]);
void			ft_str_array_del(char **strs[]);

t_vector		*ft_vector_new(size_t capacity);
t_vector		*ft_vector_from(void **data, size_t len, size_t capacity);
void			*ft_vector_get(t_vector *vector, int i);
size_t			ft_vector_len(t_vector *vector);
void			**ft_vector_data(t_vector *vector);
size_t			ft_vector_capacity(t_vector *vector);
void			*ft_vector_set(t_vector *vector, void *elem, int i);
int				ft_vector_push_front(t_vector *vector, void *elem);
int				ft_vector_push_back(t_vector *vector, void *elem);
int				ft_vector_insert(t_vector *vector, void *elem, int i);
void			*ft_vector_pop_front(t_vector *vector);
void			*ft_vector_pop_back(t_vector *vector);
void			*ft_vector_erase(t_vector *vector, int i);
void			ft_vector_clear(t_vector **vector, void (*freef)(void *));

#endif
