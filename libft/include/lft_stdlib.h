/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_stdlib.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 21:49:57 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/16 20:47:05 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_STDLIB_H

# define LFT_STDLIB_H

# include <stddef.h>

void	*ft_calloc(size_t count, size_t size);
void	*ft_crealloc(void *ptr, size_t old_size, size_t new_size);
void	ft_memdel(void **address_ptr);

int		ft_atoi(const char *str);
long	ft_strtol(const char *str, char **endstr, int base);

char	*ft_itoa(int nbr);

int		ft_abs(int nbr);
long	ft_labs(long nbr);

char	*ft_getenv(const char *name);
int		ft_setenv(const char *name, const char *value, int overwrite);
int		ft_unsetenv(const char *name);
void	destroy_local_environ(void);

#endif
