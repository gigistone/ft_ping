/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_string.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/17 14:35:11 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/07 12:08:09 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_STRING_H

# define LFT_STRING_H

# include <stddef.h>

void	ft_bzero(void *dst, size_t bytes);
void	*ft_memset(void *dst, int c, size_t bytes);

void	*ft_memcpy(void *dst, const void *src, size_t bytes);
void	*ft_memccpy(void *dst, const void *src, int c, size_t bytes);
void	*ft_memmove(void *dst, const void *src, size_t bytes);

void	*ft_memchr(const void *src, int c, size_t bytes);
int		ft_memcmp(const void *mem1, const void *mem2, size_t bytes);

char	*ft_strnew(size_t size);
void	ft_strdel(char **address_str);
void	ft_strclr(char *str);

size_t	ft_strlen(const char *str);
size_t	ft_strnlen(const char *str, size_t maxlen);
size_t	ft_strclen(const char *str, const char *set);

char	*ft_strcpy(char *dst, const char *src);
char	*ft_strncpy(char *dst, const char *src, size_t maxlen);
size_t	ft_strlcpy(char *dst, const char *src, size_t dstsize);

char	*ft_strcat(char *dst, const char *src);
char	*ft_strncat(char *dst, const char *src, size_t maxlen);
size_t	ft_strlcat(char *dst, const char *src, size_t dstsize);

char	*ft_strjoin(const char *str1, const char *str2);
char	*ft_strcjoin(const char *str1, const char *str2, const char *set);

char	*ft_strdup(const char *str);
char	*ft_strndup(const char *str, size_t maxlen);
char	*ft_strcdup(const char *str, const char *set);

char	*ft_strchr(const char *str, int c);
char	*ft_strrchr(const char *str, int c);
int		ft_strindex(const char *str, int c);

char	*ft_strstr(const char *big, const char *little);
char	*ft_strnstr(const char *big, const char *little, size_t maxlen);

int		ft_strcmp(const char *str1, const char *str2);
int		ft_strncmp(const char *str1, const char *str2, size_t maxlen);

int		ft_strequ(const char *str1, const char *str2);
int		ft_strnequ(const char *str1, const char *str2, size_t maxlen);

void	ft_striter(char *str, int (*f)(int));
void	ft_striteri(char *str, int (*f)(unsigned int, int));

char	*ft_strmap(const char *str, int (*f)(int));
char	*ft_strmapi(const char *str, int (*f)(unsigned int, int));

char	*ft_substr(const char *str, unsigned int start, size_t maxlen);
char	*ft_strtrim(const char *str, const char *set);
char	**ft_split(const char *str, const char *set);

#endif
