[![pipeline status](https://gitlab.com/btkey/libft/badges/master/pipeline.svg)](https://gitlab.com/btkey/libft/-/commits/master)
[![coverage report](https://gitlab.com/btkey/libft/badges/master/coverage.svg)](https://gitlab.com/btkey/libft/-/commits/master)
# LIBFT PROJECT

## The tiny swiss army knife for 42 students and more
This project is base on libft project created by 42 school. The goal is to build your own c standard library.
Each student of school 42 has his own libft, despite everything we decided to create this common project in order to build a high-performance libft, easily testable, aiming to offer a wide range of features, all in a jovial and community spirit. open.


## Implementation

|**HEADERS**|**DESCRIPTION**|
|--|--|
|LFT_STDLIB.H|  numeric conversion functions, pseudo-random numbers generation functions, memory allocation, process control functions|
|LFT_STDIO.H| core input and output functions|
|LFT_CTYPE.H|functions used to classify characters by their types or to convert between upper and lower case in a way that is independent of the used character set|
|LFT_STRING.H| string-handling functions|


## How to use

The use of this library collection is not recommended. It is only useful in a pedagogical framework within the school 42.
We recommend not to use this library in production. These bookstores are used for personal and academic purposes. Furthermore, it is not possible to draw full power from all the features offered by the c language because the 42 school coding standard severely restricts access to these features.


## How to contribute
You can join our group of students and become an official contributor or submit your requests in your corner, "un pour tous, tous pour un".
Before making a request, be sure to read the rules for group work (WIP)


## **
Founded by BTK (behind the keyboard) group.
<p align="center">
  <img src="https://www.cpcwiki.eu/imgs/6/6d/MarkRandallCPC464_old_logo.jpg" width="200" alt="42 AI Logo" />
</p>


## Official Contributors
 - Benjamin Boutoille ~ [gitlab](https://gitlab.com/gigistone)
 - Lylian Person Gay ~ [gitlab](https://gitlab.com/LylianPerson-Gay)

