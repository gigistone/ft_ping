/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 13:02:52 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 13:07:20 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_isprint, should_return_true_when_space)
{
	cr_expect(ft_isprint(' '));
}

Test(ft_isprint, should_return_true_when_a)
{
	cr_expect(ft_isprint('1'));
}

Test(ft_isprint, should_return_false_when_127)
{
	cr_expect_not(ft_isprint(127));
}

Test(ft_isprint, should_return_false_when_zero)
{
	cr_expect_not(ft_isprint('\0'));
}
