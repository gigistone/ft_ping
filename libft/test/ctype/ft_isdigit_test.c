/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 23:37:34 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 23:51:19 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_isdigit, should_return_true_when_1)
{
	cr_expect(ft_isdigit('1'));
}

Test(ft_isdigit, should_return_true_when_0)
{
	cr_expect(ft_isdigit('0'));
}

Test(ft_isdigit, should_return_true_when_9)
{
	cr_expect(ft_isdigit('9'));
}

Test(ft_isdigit, should_return_true_when_4)
{
	cr_expect(ft_isdigit('4'));
}

Test(ft_isdigit, should_return_false_when_null_terminating_bytes)
{
	cr_expect_not(ft_isdigit('\0'));
}

Test(ft_isdigit, should_return_false_when_a)
{
	cr_expect_not(ft_isdigit('a'));
}

Test(ft_isdigit, should_return_false_when_space)
{
	cr_expect_not(ft_isdigit(' '));
}
