/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 22:13:40 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 22:19:44 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <limits.h>
#include "lft_ctype.h"

Test(ft_isspace, should_return_true_when_form_feed)
{
	cr_expect(ft_isspace('\f'));
}

Test(ft_isspace, should_return_true_when_newline)
{
	cr_expect(ft_isspace('\n'));
}

Test(ft_isspace, should_return_true_when_carriage_return)
{
	cr_expect(ft_isspace('\r'));
}

Test(ft_isspace, should_return_true_when_horizontal_tab)
{
	cr_expect(ft_isspace('\t'));
}

Test(ft_isspace, should_return_true_when_vertical_tab)
{
	cr_expect(ft_isspace('\v'));
}

Test(ft_isspace, should_return_true_when_space)
{
	cr_expect(ft_isspace(' '));
}

Test(ft_isspace, should_return_false_when_a)
{
	cr_expect_not(ft_isspace('a'));
}

Test(ft_isspace, should_return_false_when_zero)
{
	cr_expect_not(ft_isspace(0));
}

Test(ft_isspace, should_return_false_when_int_min)
{
	cr_expect_not(ft_isspace(INT_MIN));
}
