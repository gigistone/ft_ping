/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isblank_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 13:18:12 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 13:26:59 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_isblank, should_return_true_when_space)
{
	cr_expect(ft_isblank(' '));
}

Test(ft_isblank, should_return_true_when_tab)
{
	cr_expect(ft_isblank('\t'));
}

Test(ft_isblank, should_return_false_when_newline)
{
	cr_expect_not(ft_isblank('\n'));
}


Test(ft_isblank, should_return_false_when_null_terminating_byte)
{
	cr_expect_not(ft_isblank('\0'));
}
