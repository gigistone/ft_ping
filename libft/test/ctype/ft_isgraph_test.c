/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isgraph_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 13:43:47 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 14:18:16 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_isgraph, should_return_false_when_space)
{
	cr_expect_not(ft_isgraph(' '));
}

Test(ft_isgraph, should_return_true_when_a)
{
	cr_expect(ft_isgraph('1'));
}

Test(ft_isgraph, should_return_false_when_127)
{
	cr_expect_not(ft_isgraph(127));
}

Test(ft_isgraph, should_return_false_when_zero)
{
	cr_expect_not(ft_isgraph('\0'));
}
