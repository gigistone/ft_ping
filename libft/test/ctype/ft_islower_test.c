/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_islower_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 14:32:09 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 14:34:17 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_islower, should_return_true_when_a)
{
	cr_expect(ft_islower('a'));
}

Test(ft_islower, should_return_false_when_Z)
{
	cr_expect_not(ft_islower('Z'));
}

Test(ft_islower, should_return_false_when_A)
{
	cr_expect_not(ft_islower('A'));
}

Test(ft_islower, should_return_true_when_z)
{
	cr_expect(ft_islower('z'));
}

Test(ft_islower, should_return_false_when_G)
{
	cr_expect_not(ft_islower('G'));
}

Test(ft_islower, should_return_false_when_1)
{
	cr_expect_not(ft_islower('1'));
}

Test(ft_islower, should_return_false_when_terminating_null_byte)
{
	cr_expect_not(ft_islower(0));
}
