/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isupper_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 14:47:37 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 14:48:57 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_isupper, should_return_false_when_a)
{
	cr_expect_not(ft_isupper('a'));
}

Test(ft_isupper, should_return_true_when_Z)
{
	cr_expect(ft_isupper('Z'));
}

Test(ft_isupper, should_return_true_when_A)
{
	cr_expect(ft_isupper('A'));
}

Test(ft_isupper, should_return_false_when_z)
{
	cr_expect_not(ft_isupper('z'));
}

Test(ft_isupper, should_return_true_when_G)
{
	cr_expect(ft_isupper('G'));
}

Test(ft_isupper, should_return_false_when_1)
{
	cr_expect_not(ft_isupper('1'));
}

Test(ft_isupper, should_return_false_when_terminating_null_byte)
{
	cr_expect_not(ft_isupper(0));
}
