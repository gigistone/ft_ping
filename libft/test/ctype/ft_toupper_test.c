/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 01:17:10 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 01:27:49 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_toupper, should_return_a_in_uppercase)
{
	cr_expect_eq(ft_toupper('a'), 'A');
}

Test(ft_toupper, should_return_h_in_uppercase)
{
	cr_expect_eq(ft_toupper('h'), 'H');
}

Test(ft_toupper, should_return_z_in_uppercase)
{
	cr_expect_eq(ft_toupper('z'), 'Z');
}

Test(ft_toupper, should_return_A_in_uppercase)
{
	cr_expect_eq(ft_toupper('A'), 'A');
}

Test(ft_toupper, should_return_H_in_uppercase)
{
	cr_expect_eq(ft_toupper('H'), 'H');
}

Test(ft_toupper, should_return_null_terminating_byte)
{
	cr_expect_eq(ft_toupper('\0'), '\0');
}

Test(ft_toupper, should_return_one)
{
	cr_expect_eq(ft_toupper('1'), '1');
}
