/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 12:42:48 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 12:55:23 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_isascii, should_return_true_when_a)
{
	cr_expect(ft_isascii('a'));
}

Test(ft_isascii, should_return_true_when_space)
{
	cr_expect(ft_isascii(' '));
}

Test(ft_isascii, should_return_true_when_null_terminating_byte)
{
	cr_expect(ft_isascii('\0'));
}

Test(ft_isascii, should_return_true_when_del_character)
{
	cr_expect(ft_isascii(127));
}

Test(ft_isascii, should_return_false_when_255)
{
	cr_expect_not(ft_isascii(255));
}

Test(ft_isascii, should_return_false_when_minus_one)
{
	cr_expect_not(ft_isascii(-1));
}
