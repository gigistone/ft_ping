/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 23:43:49 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 23:51:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"


Test(ft_isalnum, should_return_true_when_a)
{
	cr_expect(ft_isalnum('a'));
}

Test(ft_isalnum, should_return_true_when_Z)
{
	cr_expect(ft_isalnum('Z'));
}

Test(ft_isalnum, should_return_true_when_A)
{
	cr_expect(ft_isalnum('A'));
}

Test(ft_isalnum, should_return_true_when_z)
{
	cr_expect(ft_isalnum('z'));
}

Test(ft_isalnum, should_return_true_when_G)
{
	cr_expect(ft_isalnum('G'));
}

Test(ft_isalnum, should_return_false_when_terminating_null_byte)
{
	cr_expect_not(ft_isalnum(0));
}

Test(ft_isalnum, should_return_true_when_1)
{
	cr_expect(ft_isalnum('1'));
}

Test(ft_isalnum, should_return_true_when_0)
{
	cr_expect(ft_isalnum('0'));
}

Test(ft_isalnum, should_return_true_when_9)
{
	cr_expect(ft_isalnum('9'));
}

Test(ft_isalnum, should_return_true_when_4)
{
	cr_expect(ft_isalnum('4'));
}
