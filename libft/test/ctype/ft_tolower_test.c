/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 01:29:16 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 01:31:21 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_tolower, should_return_A_in_lowercase)
{
	cr_expect_eq(ft_tolower('A'), 'a');
}

Test(ft_tolower, should_return_H_in_lowercase)
{
	cr_expect_eq(ft_tolower('H'), 'h');
}

Test(ft_tolower, should_return_Z_in_lowercase)
{
	cr_expect_eq(ft_tolower('Z'), 'z');
}

Test(ft_tolower, should_return_a)
{
	cr_expect_eq(ft_tolower('a'), 'a');
}

Test(ft_tolower, should_return_h)
{
	cr_expect_eq(ft_tolower('h'), 'h');
}

Test(ft_tolower, should_return_null_terminating_byte)
{
	cr_expect_eq(ft_tolower('\0'), '\0');
}

Test(ft_tolower, should_return_one)
{
	cr_expect_eq(ft_tolower('1'), '1');
}
