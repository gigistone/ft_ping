/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ispunct_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 14:57:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 14:59:57 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_ispunct, should_return_true_when_colon)
{
	cr_expect(ft_ispunct('.'));
}

Test(ft_ispunct, should_return_true_when_pipe)
{
	cr_expect(ft_ispunct('|'));
}

Test(ft_ispunct, should_return_true_when_equal)
{
	cr_expect(ft_ispunct('='));
}

Test(ft_ispunct, should_return_true_when_backslah)
{
	cr_expect(ft_ispunct('\\'));
}

Test(ft_ispunct, should_return_false_when_a)
{
	cr_expect_not(ft_ispunct('a'));
}

Test(ft_ispunct, should_return_false_when_zero)
{
	cr_expect_not(ft_ispunct('0'));
}

Test(ft_ispunct, should_return_false_when_null_terminating_byte)
{
	cr_expect_not(ft_ispunct('\0'));
}
