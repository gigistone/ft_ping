/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iscntrl_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 13:31:34 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 13:37:37 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_iscntrl, should_return_true_when_null_terminating_byte)
{
	cr_expect(ft_iscntrl('\0'));
}

Test(ft_iscntrl, should_return_true_when_unit_separator)
{
	cr_expect(ft_iscntrl(31));
}

Test(ft_iscntrl, should_return_true_when_del_character)
{
	cr_expect(ft_iscntrl(127));
}

Test(ft_iscntrl, should_return_false_when_space)
{
	cr_expect_not(ft_iscntrl(' '));
}

Test(ft_iscntrl, should_return_false_when_a)
{
	cr_expect_not(ft_iscntrl('a'));
}
