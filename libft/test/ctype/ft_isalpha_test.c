/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 23:29:20 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 23:32:45 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_ctype.h"

Test(ft_isalpha, should_return_true_when_a)
{
	cr_expect(ft_isalpha('a'));
}

Test(ft_isalpha, should_return_true_when_Z)
{
	cr_expect(ft_isalpha('Z'));
}

Test(ft_isalpha, should_return_true_when_A)
{
	cr_expect(ft_isalpha('A'));
}

Test(ft_isalpha, should_return_true_when_z)
{
	cr_expect(ft_isalpha('z'));
}

Test(ft_isalpha, should_return_true_when_G)
{
	cr_expect(ft_isalpha('G'));
}

Test(ft_isalpha, should_return_false_when_1)
{
	cr_expect_not(ft_isalpha('1'));
}

Test(ft_isalpha, should_return_false_when_terminating_null_byte)
{
	cr_expect_not(ft_isalpha(0));
}
