/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_erase_test.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 17:39:57 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 18:06:24 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_erase, should_return_the_element_erase)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_erase(new, 1), "General");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase, should_erase_the_element_specified_at_index)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	ft_vector_erase(new, 1);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Kenoby");
	cr_expect_null(ft_vector_get(new, 2));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase, should_erase_the_last_element_when_negative_one)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_erase(new, -1), "Kenoby");
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "General");
	cr_expect_null(ft_vector_get(new, 2));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase,\
should_erase_the_last_element_when_positive_i_out_of_range)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_erase(new, 10000), "Kenoby");
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "General");
	cr_expect_null(ft_vector_get(new, 2));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase, should_erase_the_first_element_when_0)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_erase(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "General");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Kenoby");
	cr_expect_null(ft_vector_get(new, 2));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase,\
should_erase_the_first_element_when_negative_i_out_of_range)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_erase(new, -100000), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "General");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Kenoby");
	cr_expect_null(ft_vector_get(new, 2));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase, should_return_null_when_no_element_to_erase)
{
	t_vector	*new;

	new = ft_vector_new(10);
	cr_expect_not_null(new);
	cr_expect_null(ft_vector_erase(new, 1));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase, should_decrement_len)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	ft_vector_erase(new, 1000);
	cr_expect_eq(ft_vector_len(new), 2);
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_erase, should_divide_capacity_when_too_big)
{
	char		*strs[3] = {"Hello", "General", "Kenoby"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 3, 10);
	cr_expect_not_null(new);
	ft_vector_erase(new, -100000);
	cr_expect_eq(ft_vector_capacity(new), 5);
	ft_vector_clear(&new, NULL);
}
