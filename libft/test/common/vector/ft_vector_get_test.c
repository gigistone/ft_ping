/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_get_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 20:17:19 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 20:30:41 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_get, should_return_the_element_at_index_i)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "World!");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_get, should_return_the_last_element_when_minus_one)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_get(new, -1), "World!");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_get, should_return_null_when_index_out_of_range)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_null(ft_vector_get(new, 2));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_get, should_return_null_when_negative_index_out_of_range)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_null(ft_vector_get(new, -3));
	ft_vector_clear(&new, NULL);
}
