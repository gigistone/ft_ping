/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_pop_front_test.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 16:43:32 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 17:10:42 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_pop_front, should_return_the_first_element)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_pop_front(new), "Hello");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_pop_front, should_return_null_if_no_elements_to_pop_out)
{
	t_vector	*new;

	new = ft_vector_new(10);
	cr_expect_not_null(new);
	cr_expect_null((char*)ft_vector_pop_front(new));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_pop_front, should_erase_first_element)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_pop_front(new);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "World!");
	cr_expect_null((char*)ft_vector_get(new, 1));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_pop_front, should_decrement_len)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_pop_front(new);
	cr_expect_eq(ft_vector_len(new), 1);
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_pop_front, should_divide_capacity_by_double_if_too_big)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_pop_front(new);
	cr_expect_eq(ft_vector_capacity(new), 5);
	ft_vector_clear(&new, NULL);
}
