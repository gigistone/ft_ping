/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_from_test.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 20:38:45 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:10:58 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_from, should_create_a_new_vector_with_values_passed)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_eq(new->len, 2);
	cr_expect_eq(new->capacity, 10);
}

Test(ft_vector_from, should_set_capacity_to_the_double_of_len_when_not_enough)
{
		char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 0);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_eq(new->len, 2);
	cr_expect_eq(new->capacity, 4);
}
