/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_new_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 19:56:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 20:02:50 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_new, should_create_a_new_vector_with_the_spec_capacity)
{
	t_vector	*new;

	new = ft_vector_new(10);
	cr_expect_not_null(new);
	cr_expect_eq(new->capacity, 10);
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_new, should_init_data_to_zero)
{
	t_vector	*new;
	void		*data[10] = {NULL, };

	new = ft_vector_new(10);
	cr_expect_not_null(new);
	cr_expect_arr_eq(new->data, data, sizeof(data));
	ft_vector_clear(&new, NULL);
}
