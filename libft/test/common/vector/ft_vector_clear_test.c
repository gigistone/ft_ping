/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_clear_test.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 20:03:04 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 20:57:39 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_common.h"

static void	test_free(void *elem)
{
	if (elem)
		free(elem);
}

Test(ft_vector_clear, should_free_and_set_vector_ptr_to_null)
{
	t_vector	*new;

	new = ft_vector_new(10);
	cr_expect_not_null(new);
	ft_vector_clear(&new, test_free);
	cr_expect_null(new);
}

Test(ft_vector_clear, should_not_crash_when_passing_null_to_free_function)
{
	t_vector	*new;

	new = ft_vector_new(10);
	cr_expect_not_null(new);
	ft_vector_clear(&new, NULL);
	cr_expect_null(new);
}
