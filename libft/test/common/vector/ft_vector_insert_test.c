/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_insert_test.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 03:03:52 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 16:29:38 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_insert, should_insert_element_at_the_begin_when_index_is_0)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_insert(new, "Boyyy", 0);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Boyyy");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 2), "World!");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_insert, should_insert_element_at_end_when_index_eq_or_sup_to_len)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_insert(new, "Boyyy", 1000);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "World!");
	cr_expect_str_eq((char*)ft_vector_get(new, 2), "Boyyy");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_insert, should_insert_element_at_specified_index)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_insert(new, "Boyyy", 1);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Boyyy");
	cr_expect_str_eq((char*)ft_vector_get(new, 2), "World!");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_insert, should_insert_element_before_last_when_minus_one)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_insert(new, "Boyyy", -1);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Boyyy");
	cr_expect_str_eq((char*)ft_vector_get(new, 2), "World!");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_insert, should_insert_at_beginning_when_negative_i_out_of_range)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_insert(new, "Boyyy", -1000);
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Boyyy");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 2), "World!");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_insert, should_increment_len)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_insert(new, "Boyyy", 1);
	cr_expect_eq(ft_vector_len(new), 3);
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_insert, should_double_capacity_when_not_enough_memory)
{
	t_vector	*new;

	new = ft_vector_new(0);
	cr_expect_not_null(new);
	ft_vector_insert(new, "Hello", 0);
	ft_vector_insert(new, "Boy", 1);
	cr_expect_eq(ft_vector_capacity(new), 2);
	ft_vector_clear(&new, NULL);
}
