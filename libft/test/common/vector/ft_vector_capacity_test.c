/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_capacity_test.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 21:24:50 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:26:17 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_capacity, should_return_the_memory_capacity_of_the_vector)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_eq(ft_vector_capacity(new), new->capacity);
	ft_vector_clear(&new, NULL);
}
