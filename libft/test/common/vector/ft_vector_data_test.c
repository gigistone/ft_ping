/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_data_test.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 21:28:22 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:29:12 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_data, should_return_the_address_of_the_datas_of_vector)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_eq(ft_vector_data(new), new->data);
	ft_vector_clear(&new, NULL);
}
