/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_set_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 21:35:09 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:52:04 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_set, should_set_the_value_of_i_to_new_ptr)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_set(new, "General Kenoby", 1);
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "General Kenoby");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_set,\
should_set_the_value_of_the_last_arg_to_new_ptr_when_minus_one)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_set(new, "General Kenoby", -1);
	cr_expect_str_eq((char*)ft_vector_get(new, -1), "General Kenoby");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_set, should_return_negative_number_when_index_out_of_range)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_null(ft_vector_set(new, "General Kenoby", 2));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_set,\
should_return_null_when_negative_index_out_of_range)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_null(ft_vector_set(new, "General Kenoby", -3));
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_set, should_return_old_value_when_set_new_value)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	cr_expect_str_eq((char*)ft_vector_set(new, "General Kenoby", -1),\
	"World!");
	ft_vector_clear(&new, NULL);
}
