/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_push_front_test.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 01:50:58 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 02:24:46 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_push_front, should_add_new_entry_at_begin_of_vector)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_push_front(new, "I say");
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "I say");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 2), "World!");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_push_front, should_increment_len_when_push_front)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_push_front(new, "I say");
	cr_expect_eq(ft_vector_len(new), 3);
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_push_front, should_double_capacity_when_not_enough_place)
{
	t_vector	*new;

	new = ft_vector_new(0);
	cr_expect_not_null(new);
	ft_vector_push_front(new, "World!");
	ft_vector_push_front(new, "Hello");
	cr_expect_eq(ft_vector_capacity(new), 2);
	ft_vector_clear(&new, NULL);
}
