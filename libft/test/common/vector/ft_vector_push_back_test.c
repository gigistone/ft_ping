/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_push_back_test.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 02:33:00 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 03:01:28 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_vector_push_back, should_add_new_element_at_the_end_of_vector)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_push_back(new, "General Kenoby");
	cr_expect_str_eq((char*)ft_vector_get(new, 0), "Hello");
	cr_expect_str_eq((char*)ft_vector_get(new, 1), "World!");
	cr_expect_str_eq((char*)ft_vector_get(new, 2), "General Kenoby");
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_push_back, should_increment_len)
{
	char		*strs[2] = {"Hello", "World!"};
	t_vector	*new;

	new = ft_vector_from((void**)strs, 2, 10);
	cr_expect_not_null(new);
	ft_vector_push_back(new, "General Kenoby");
	cr_expect_eq(ft_vector_len(new), 3);
	ft_vector_clear(&new, NULL);
}

Test(ft_vector_push_back, should_update_capacity_when_not_enough_memory)
{
	t_vector	*new;

	new = ft_vector_new(0);
	cr_expect_not_null(new);
	ft_vector_push_back(new, "Hello");
	ft_vector_push_back(new, "World!");
	cr_expect_eq(ft_vector_capacity(new), 2);
	ft_vector_clear(&new, NULL);
}
