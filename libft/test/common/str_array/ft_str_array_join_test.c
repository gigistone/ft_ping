/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_join_test.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 12:54:15 by lperson-          #+#    #+#             */
/*   Updated: 2020/10/22 13:24:52 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_str_array_join, should_join_two_full_strs_array)
{
	char	*expected[5] = {"Hello", "world", "and", "Satan", NULL};
	char	*strs1[3] = {"Hello", "world", NULL};
	char	*strs2[3] = {"and", "Satan", NULL};
	char	**result;
	int		i;

	result = ft_str_array_join(strs1, strs2);
	for (i = 0; expected[i]; i++)
		cr_expect_str_eq(result[i], expected[i]);
	cr_expect_null(result[i]);
	free(result);
}

Test(ft_str_array_join, should_join_one_full_array_with_one_empty_array)
{
	char	*expected[3] = {"Hello", "world", NULL};
	char	*strs1[3] = {"Hello", "world", NULL};
	char	*strs2[1] = {NULL};
	char	**result;
	int		i;

	result = ft_str_array_join(strs1, strs2);
	for (i = 0; expected[i]; i++)
		cr_expect_str_eq(result[i], expected[i]);
	cr_expect_null(result[i]);
	free(result);
}

Test(ft_str_array_join, should_join_two_empty_arrays)
{
	char	*expected[1] = {NULL};
	char	*strs1[1] = {NULL};
	char	*strs2[1] = {NULL};
	char	**result;
	int		i;

	result = ft_str_array_join(strs1, strs2);
	for (i = 0; expected[i]; i++)
		cr_expect_str_eq(result[i], expected[i]);
	cr_expect_null(result[i]);
	free(result);
}
