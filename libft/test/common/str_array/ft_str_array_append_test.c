/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_append_test.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 16:11:00 by lperson-          #+#    #+#             */
/*   Updated: 2020/10/22 14:27:26 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <string.h>
#include "lft_common.h"

Test(ft_str_array_append, should_append_a_string_to_the_array)
{
	char		**dst = ft_str_array_dup((char*[3]){"Hello", "World", NULL});
	char		*append = strdup("Gillowel");
	char		*expected[4] = {"Hello", "World", append, NULL};
	int			i;

	ft_str_array_append(&dst, append);
	for (i = 0; expected[i]; i++)
		cr_expect_str_eq(dst[i], expected[i]);
	cr_expect_null(dst[i]);
	ft_str_array_del(&dst);
}

Test(ft_str_array_append, should_append_a_string_to_the_array_when_str_empty)
{
	char		**dst = ft_str_array_dup((char*[1]){NULL});
	char		*append = strdup("Hello");
	char		*expected[2] = {"Hello", NULL};
	int			i;

	ft_str_array_append(&dst, append);
	for (i = 0; expected[i]; i++)
		cr_expect_str_eq(dst[i], expected[i]);
	cr_expect_null(dst[i]);
	ft_str_array_del(&dst);
}

Test(ft_str_array_append, should_append_a_empty_string_to_the_array)
{
	char		**dst = ft_str_array_dup((char*[1]){NULL});
	char		*append = strdup("");
	char		*expected[2] = {"", NULL};
	int			i;

	ft_str_array_append(&dst, append);
	for (i = 0; expected[i]; i++)
		cr_expect_str_eq(dst[i], expected[i]);
	cr_expect_null(dst[i]);
	ft_str_array_del(&dst);
}

Test(ft_str_array_append, should_init_str_array_when_empty)
{
	char		**dst = NULL;
	char		*append = strdup("Hello");
	char		*expected[2] = {"Hello", NULL};
	int			i;

	ft_str_array_append(&dst, append);
	for (i = 0; expected[i]; i++)
		cr_expect_str_eq(dst[i], expected[i]);
	cr_expect_null(dst[i]);
	ft_str_array_del(&dst);
}
