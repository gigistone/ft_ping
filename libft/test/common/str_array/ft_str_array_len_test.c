/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_len_test.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:07:18 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 05:10:43 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_str_array_len, should_return_the_number_of_strings)
{
	char	*test[3] = {"Hello", "World!", NULL};

	cr_expect_eq(ft_str_array_len(test), 2);
}

Test(ft_str_array_len, should_return_zero_when_empty)
{
	char	*test[1] = {NULL};

	cr_expect_eq(ft_str_array_len(test), 0);
}

Test(ft_str_array_len, should_return_one_when_one_string)
{
	char	*test[2] = {"Hello, World", NULL};

	cr_expect_eq(ft_str_array_len(test), 1);
}
