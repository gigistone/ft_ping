/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_dup_test.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:34:01 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 06:17:55 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_str_array_dup, should_allocate_a_new_array)
{
	char	**dst;
	char	*src[2] = {"Hello, World!", NULL};

	dst = ft_str_array_dup(src);
	cr_assert_not_null(dst);
	ft_str_array_del(&dst);
}

Test(ft_str_array_dup, should_duplicate_each_string_of_srcs)
{
	char	**dst;
	char	*src[3] = {"Hello", "World!", NULL};

	dst = ft_str_array_dup(src);
	cr_assert_not_null(dst);
	for (int i = 0; dst[i]; i++)
	{
		cr_expect_neq(dst[i], src[i]);
		cr_expect_str_eq(dst[i], src[i]);
	}
	ft_str_array_del(&dst);
}

Test(ft_str_array_dup, should_duplicate_none_string_of_srcs_when_empty)
{
	char	**dst;
	char	*src[1] = {NULL};

	dst = ft_str_array_dup(src);
	cr_assert_not_null(dst);
	cr_expect_null(dst[0]);
	ft_str_array_del(&dst);
}
