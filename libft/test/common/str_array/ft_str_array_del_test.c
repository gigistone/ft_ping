/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_del_test.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:49:55 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 06:18:17 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_str_array_del, should_set_array_to_NULL_and_free_all)
{
	char	*src[2] = {"Hello, World!", NULL};
	char	**dup = ft_str_array_dup(src);

	ft_str_array_del(&dup);
	cr_expect_null(dup);
}
