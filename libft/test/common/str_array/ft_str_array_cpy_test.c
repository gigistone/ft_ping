/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_cpy_test.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:18:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/10/21 16:14:22 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_common.h"

Test(ft_str_array_cpy, should_cpy_each_ptr_of_another_str_array)
{
	char	*dst[3] = {NULL, NULL, NULL};
	char	*src[3] = {"Hello", "World!", NULL};

	ft_str_array_cpy(dst, src);
	for (int i = 0; src[i]; i++)
		cr_expect_eq(dst[i], src[i]);
}

Test(ft_str_array_cpy, should_cpy_empty_when_str_array_empty)
{
	char	*dst[1] = {"Hello, World!"};
	char	*src[1] = {NULL};

	ft_str_array_cpy(dst, src);
	cr_expect_null(dst[0]);
}

Test(ft_str_array_cpy, should_return_the_address_of_the_array)
{
	char	*dst[2];
	char	*src[2] = {"Hello, World!", NULL};

	cr_expect_eq(ft_str_array_cpy(dst, src), dst);
}
