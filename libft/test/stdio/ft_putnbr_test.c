/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 20:21:25 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 20:31:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <limits.h>
#include "lft_stdio.h"

TestSuite(ft_putnbr, .init = cr_redirect_stdout);

Test(ft_putnbr, should_output_42_when_42)
{
	ft_putnbr(42);
	cr_expect_stdout_eq_str("42");
}

Test(ft_putnbr, should_output_10_when_10)
{
	ft_putnbr(10);
	cr_expect_stdout_eq_str("10");
}

Test(ft_putnbr, should_output_0_when_0)
{
	ft_putnbr(0);
	cr_expect_stdout_eq_str("0");
}

Test(ft_putnbr, should_output_minus_1_when_minus_1)
{
	ft_putnbr(-1);
	cr_expect_stdout_eq_str("-1");
}

Test(ft_putnbr, should_output_int_max_when_int_max)
{
	ft_putnbr(INT_MAX);
	cr_expect_stdout_eq_str("2147483647");
}

Test(ft_putnbr, should_output_int_min_when_int_min)
{
	ft_putnbr(INT_MIN);
	cr_expect_stdout_eq_str("-2147483648");
}
