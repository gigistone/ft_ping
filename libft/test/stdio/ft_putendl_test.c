/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 20:08:24 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 20:15:48 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "lft_stdio.h"

TestSuite(ft_putendl, .init=cr_redirect_stdout);

Test(ft_putendl, should_output_hello_world_with_a_new_line)
{
	ft_putendl("Hello, World!");
	cr_expect_stdout_eq_str("Hello, World!\n");
}
