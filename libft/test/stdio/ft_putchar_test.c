/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 19:48:23 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 19:59:47 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "lft_stdio.h"

TestSuite(ft_putchar, .init = cr_redirect_stdout);

Test(ft_putchar, should_output_char_when_a)
{
	ft_putchar('a');
	cr_expect_stdout_eq_str("a");
}

Test(ft_putchar, should_output_char_when_null_terminating_byte)
{
	ft_putchar('\t');
	cr_expect_stdout_eq_str("\t");
}
