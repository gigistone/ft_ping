/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd_test.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:32:58 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:33:27 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "lft_stdio.h"

TestSuite(ft_putstr_fd, .init=cr_redirect_stderr);

Test(ft_putstr_fd, should_output_hello_world)
{
	ft_putstr_fd("Hello, World!", 2);
	cr_expect_stderr_eq_str("Hello, World!");
}
