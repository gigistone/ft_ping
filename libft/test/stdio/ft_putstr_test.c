/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 20:01:09 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 20:02:41 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "lft_stdio.h"

TestSuite(ft_putstr, .init=cr_redirect_stdout);

Test(ft_putstr, should_output_hello_world)
{
	ft_putstr("Hello, World!");
	cr_expect_stdout_eq_str("Hello, World!");
}
