/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd_fd_test.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:42:30 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:42:44 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <limits.h>
#include "lft_stdio.h"

TestSuite(ft_putnbr_fd, .init = cr_redirect_stderr);

Test(ft_putnbr_fd, should_output_42_when_42)
{
	ft_putnbr_fd(42, 2);
	cr_expect_stderr_eq_str("42");
}

Test(ft_putnbr_fd, should_output_10_when_10)
{
	ft_putnbr_fd(10, 2);
	cr_expect_stderr_eq_str("10");
}

Test(ft_putnbr_fd, should_output_0_when_0)
{
	ft_putnbr_fd(0, 2);
	cr_expect_stderr_eq_str("0");
}

Test(ft_putnbr_fd, should_output_minus_1_when_minus_1)
{
	ft_putnbr_fd(-1, 2);
	cr_expect_stderr_eq_str("-1");
}

Test(ft_putnbr_fd, should_output_int_max_when_int_max)
{
	ft_putnbr_fd(INT_MAX, 2);
	cr_expect_stderr_eq_str("2147483647");
}

Test(ft_putnbr_fd, should_output_int_min_when_int_min)
{
	ft_putnbr_fd(INT_MIN, 2);
	cr_expect_stderr_eq_str("-2147483648");
}
