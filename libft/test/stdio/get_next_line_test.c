/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 14:00:00 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/21 16:19:35 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>

#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <stdlib.h>
#include <stdio.h>

#include "lft_stdio.h"

static FILE *cpy_file(const char *filename)
{
	FILE	*tmp;
	char	*line;
	int		fd;
	int		ret;

	fd = open(filename, O_RDONLY);
	tmp = tmpfile();
	if (!tmp)
	{
		if (fd >= 0)
			close(fd);
		return (tmp);
	}
	while ((ret = get_next_line(fd, &line)) == 1)
	{
		fprintf(tmp, "%s\n", line);
		free(line);
	}
	if (ret < 0)
	{
		fclose(tmp);
		if (fd)
			close(fd);
		return (NULL);
	}
	fprintf(tmp, "%s", line);
	free(line);
	close(fd);
	rewind(tmp);
	return (tmp);
}

Test(get_next_line, should_return_zero_when_reach_EOF)
{
	char	*line;
	int		fd;

	fd = open("test/stdio/files_tests/hello_world.txt", O_RDONLY);
	if (fd < 0)
		return ;
	cr_expect_eq(get_next_line(fd, &line), 0);
	if (line)
		free(line);
	close(fd);
}

Test(get_next_line, should_read_hello_world_from_hello_world_txt)
{
	char	*line;
	int		fd;

	fd = open("test/stdio/files_tests/hello_world.txt", O_RDONLY);
	if (fd < 0)
		return ;
	get_next_line(fd, &line);
	if (line)
	{
		cr_expect_str_eq(line, "Hello, World!");
		free(line);
	}
	close(fd);
}

Test(get_next_line, should_read_empty_string_when_read_empty_line_txt)
{
	char	*line;
	int		fd;

	fd = open("test/stdio/files_tests/empty_line.txt", O_RDONLY);
	if (fd < 0)
		return ;
	get_next_line(fd, &line);
	if (line)
	{
		cr_expect_str_empty(line);
		free(line);
	}
	close(fd);
}

Test(get_next_line, should_return_one_when_read_empty_line_txt)
{
	char	*line;
	int		fd;

	fd = open("test/stdiofiles_tests/empty_line.txt", O_RDONLY);
	if (fd < 0)
		return ;
	cr_expect_eq(get_next_line(fd, &line), 1);
	if (line)
		free(line);
	close(fd);
}

Test(get_next_line, should_set_line_to_null_when_read_inexistant_file)
{
	char	*line;
	int		fd;

	fd = open("Stallman > Torvald", O_RDONLY);
	if (fd < 0)
		return ;
	get_next_line(fd, &line);
	cr_expect_null(line);
}

Test(get_next_line, should_return_minus_one_when_read_inexistant_file)
{
	char	*line;
	int		fd;

	fd = open("Stallman > Torvald", O_RDONLY);
	if (fd < 0)
		return ;
	cr_expect_eq(get_next_line(fd, &line), -1);
}

Test(get_next_line, should_read_empty_string_when_read_empty_file)
{
	char	*line;
	int		fd;

	fd = open("test/stdio/files_tests/empty_file.txt", O_RDONLY);
	if (fd < 0)
		return ;
	get_next_line(fd, &line);
	if (line)
	{
		cr_expect_str_empty(line);
		free(line);
	}
	close(fd);
}

Test(get_next_line, should_return_zero_when_read_empty_file)
{
	char	*line;
	int		fd;

	fd = open("test/stdio/files_tests/empty_file.txt", O_RDONLY);
	if (fd < 0)
		return ;
	cr_expect_eq(get_next_line(fd, &line), 0);
	if (line)
		free(line);
	close(fd);
}

Test(get_next_line, should_read_entire_file_when_loop_and_read_linus_mail)
{
	FILE	*cpy;
	FILE	*file;

	cpy = cpy_file("test/stdio/files_tests/linus_mail.txt");
	if (cpy)
	{
		file = fopen("test/stdio/files_tests/linus_mail.txt", "r");
		if (file)
		{
			cr_expect_file_contents_eq(cpy, file);
			fclose(file);
		}
		fclose(cpy);
	}
}

Test(get_next_line, should_read_multiple_filedescriptors_at_the_same_time)
{
	int		fds[2];
	char	*line;

	fds[0] = open("test/stdio/files_tests/hello_world.txt", O_RDONLY);
	if (fds[0] < 0)
		return ;
	fds[1] = open("test/stdio/files_tests/linus_mail.txt", O_RDONLY);
	if (fds[1] < 0)
	{
		close(fds[0]);
		return ;
	}
	get_next_line(fds[0], &line);
	cr_assert_not_null(line);
	cr_expect_str_eq(line, "Hello, World!");
	free(line);
	get_next_line(fds[1], &line);
	cr_assert_not_null(line);
	cr_expect_str_eq(line, "Hello everybody out there using minix -");
	free(line);
	cr_expect_eq(get_next_line(fds[0], &line), 0);
	cr_assert_not_null(line);
	cr_expect_str_empty(line);
	free(line);
	cr_expect_eq(get_next_line(fds[1], &line), 1);
	cr_expect_not_null(line);
	cr_expect_str_empty(line);
	free(line);
	close(fds[0]);
	close(fds[1]);
}
