/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:26:46 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:31:11 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "lft_stdio.h"

TestSuite(ft_putchar_fd, .init = cr_redirect_stderr);

Test(ft_putchar_fd, should_output_char_when_a)
{
	ft_putchar_fd('a', 2);
	cr_expect_stderr_eq_str("a");
}

Test(ft_putchar_fd, should_output_char_when_null_terminating_byte)
{
	ft_putchar_fd('\t', 2);
	cr_expect_stderr_eq_str("\t");
}
