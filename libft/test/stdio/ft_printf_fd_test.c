/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fd_test.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 11:31:27 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 14:16:50 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include <unistd.h>
#include <stdint.h>
#include "lft_stdio.h"

#define STR_HELPER(number) #number
#define STR(define) STR_HELPER(define)

TestSuite(ft_printf_fd, .init = cr_redirect_stdout);

Test(ft_printf_fd, should_simple_output_hello_world_when_no_format)
{
	const char *str = "Hello, World!";

	ft_printf_fd(str, STDOUT_FILENO);
	cr_expect_stdout_eq_str(str);
}

Test(ft_printf_fd, should_return_length_of_hello_world_when_no_format)
{
	const char *str = "Hello, World!";

	cr_expect_eq(ft_printf_fd(str, STDOUT_FILENO), 13);
}

Test(ft_printf_fd, should_return_0_when_empty_string)
{
	const char *str = "";

	cr_expect_eq(ft_printf_fd(str, STDOUT_FILENO), 0);
}

Test(ft_printf_fd, should_return_negative_value_when_false_fd)
{
	const char *str = "Hello, World!";

	cr_expect_lt(ft_printf_fd(str, -1), 0);
}

Test(ft_printf_fd, should_print_modulo_when_double_modulo_arg)
{
	const char	*str = "Hello, World!%%";

	ft_printf_fd(str, STDOUT_FILENO);
	cr_expect_stdout_eq_str("Hello, World!%");
}

Test(ft_printf_fd, should_print_format_string_when_format_error)
{
	const char	*str = "%Hello, World!";

	ft_printf_fd(str, STDOUT_FILENO);
	cr_expect_stdout_eq_str(str);
}

Test(ft_printf_fd, should_avoid_flags_and_field_width_when_double_modulo)
{
	const char	*str = "Hello, %300000%!";

	ft_printf_fd(str, STDOUT_FILENO);
	cr_expect_stdout_eq_str("Hello, %!");
}

Test(ft_printf_fd, \
should_print_format_string_and_modulo_when_error_and_double_modulo)
{
	const char	*str = "%%Hello, %World!";

	ft_printf_fd(str, STDOUT_FILENO);
	cr_expect_stdout_eq_str("%Hello, %World!");
}

Test(ft_printf_fd, should_output_char_specified)
{
	const char	c = 'o';

	ft_printf_fd("Hell%c, W%crld!", STDOUT_FILENO, c, c);
	cr_expect_stdout_eq_str("Hello, World!");
}

Test(ft_printf_fd, should_output_char_left_padding)
{
	const char	c = 'A';

	ft_printf_fd("%-4c\n", STDOUT_FILENO, c);
	cr_expect_stdout_eq_str("A   \n");
}

Test(ft_printf_fd, should_field_with_3_spaces_when_char)
{
	const char	c = 'A';

	ft_printf_fd("%4c\n", STDOUT_FILENO, c);
	cr_expect_stdout_eq_str("   A\n");
}

Test(ft_printf_fd, should_field_with_3_spaces_when_char_and_from_va_list)
{
	const char	c = 'A';

	ft_printf_fd("%*c", STDOUT_FILENO, 4, c);
	cr_expect_stdout_eq_str("   A\n");
}

Test(ft_printf_fd, should_field_with_3_spaces_when_char_and_from_num_pos)
{
	const char	c = 'A';

	ft_printf_fd("%*2$c", STDOUT_FILENO, c, 4);
	cr_expect_stdout_eq_str("   A\n");
}

Test(ft_printf_fd, should_output_char_with_3_spaces_when_two_num_pos)
{
	const char	c = 'A';

	ft_printf_fd("%2$*3$c\n", STDOUT_FILENO, "Hey you", c, 4);
	cr_expect_stdout_eq_str("   A\n");
}

Test(ft_printf_fd, should_ignore_precision_numercis_flags_when_char)
{
	const char	c = 'A';

	ft_printf_fd("%#+4.2c\n", STDOUT_FILENO, c);
	cr_expect_stdout_eq_str("   A\n");
}

Test(ft_printf_fd, should_output_char_when_0)
{
	const char	c = 0;

	cr_expect_eq(ft_printf_fd("%c", STDOUT_FILENO, c), 1);
}

Test(ft_printf_fd, should_output_hello_world_when_string_substitution)
{
	const char	*str = "World!";

	ft_printf_fd("Hello, %s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello, World!");
}

Test(ft_printf_fd, should_padd_3_space_when_str)
{
	const char	*str = "World!";

	ft_printf_fd("Hello, %8s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello,   World!");
}

Test(ft_printf_fd, should_padd_3_space_to_left_when_str)
{
	const char	*str = "World!";

	ft_printf_fd("Hello, %-9s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello, World!   ");
}

Test(ft_printf_fd, should_select_arguments_from_varray)
{
	const char	*str = "World!";

	ft_printf_fd("Hello, %5$*2$s", STDOUT_FILENO, "hey", -9, "you", "too", str);
	cr_expect_stdout_eq_str("Hello, World!   ");
}

Test(ft_printf_fd, should_display_2_char_from_string_when_precision)
{
	const char	*str = "World!";

	ft_printf_fd("Hello, %.2s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello, Wo");
}

Test(ft_printf_fd, should_display_2_char_from_string_and_padding)
{
	const char	*str = "World!";

	ft_printf_fd("Hello, %4.2s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello,   Wo");
}

Test(ft_printf_fd, should_display_2_char_from_string_and_left_padding)
{
	const char	*str = "World!";

	ft_printf_fd("Hello, %-4.2s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello, Wo  ");
}

Test(ft_printf_fd, should_display_null_string_when_null)
{
	const char	*str = NULL;

	ft_printf_fd("Hello, %s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello, (null)");
}

Test(ft_printf_fd, should_truncate_null_string_when_precision)
{
	const char	*str = NULL;

	ft_printf_fd("Hello, %.2s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello, (n");
}

Test(ft_printf_fd, should_padd_and_truncate_null_string)
{
	const char	*str = NULL;

	ft_printf_fd("Hello, %-4.2s", STDOUT_FILENO, str);
	cr_expect_stdout_eq_str("Hello, (n  ");
}

Test(ft_printf_fd, should_print_a_number)
{
	int	nbr = 42;

	ft_printf_fd("%d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("42");
}

Test(ft_printf_fd, should_print_a_negative_number)
{
	int	nbr = -42;

	ft_printf_fd("%d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("-42");
}

Test(ft_printf_fd, should_print_zero)
{
	int	nbr = 0;

	ft_printf_fd("%d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("0");
}

Test(ft_printf_fd, should_space_one_when_neg_and_field_to_4)
{
	int	nbr = -42;

	ft_printf_fd("%4d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str(" -42");
}

Test(ft_printf_fd, should_space_two_when_space_padd_and_field_to_4)
{
	int	nbr = 42;

	ft_printf_fd("% 4d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("  42");
}

Test(ft_printf_fd, should_space_one_when_sign_padd_and_field_to_4)
{
	int	nbr = 42;

	ft_printf_fd("%+4d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str(" +42");
}

Test(ft_printf_fd, should_fill_one_zero_when_zero_padd_and_field_to_3)
{
	int	nbr = 42;

	ft_printf_fd("%03d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("042");
}

Test(ft_printf_fd, should_fill_one_zero_when_precision_to_3)
{
	int	nbr = 42;

	ft_printf_fd("%.3d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("042");
}

Test(ft_printf_fd, should_padd_and_fill_with_zero_when_precision_and_zf)
{
	int	nbr = 42;

	ft_printf_fd("%04.3d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str(" 042");
}

Test(ft_printf_fd, should_padd_and_output_nothing_when_0_and_precision_0)
{
	int	nbr = 0;

	ft_printf_fd("%3.d", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("   ");
}

Test(ft_printf_fd, should_output_intmax_t_nbr)
{
	intmax_t	nbr = INTMAX_MAX;

	ft_printf_fd("(%jdL)", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str(STR(INTMAX_MAX));
}

Test(ft_printf_fd, should_output_the_minimum_number)
{
	intmax_t	nbr = INTMAX_MIN;

	ft_printf_fd("(%jdL-1)", STDOUT_FILENO, nbr + 1);
	cr_expect_stdout_eq_str(STR(INTMAX_MIN));
}

Test(ft_printf_fd, should_output_255_in_hexa_min)
{
	uintmax_t	nbr = 255;

	ft_printf_fd("%x", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("ff");
}

Test(ft_printf_fd, should_output_255_in_hexa_max)
{
	uintmax_t	nbr = 255;

	ft_printf_fd("%X", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("FF");
}

Test(ft_printf_fd, should_output_255_in_hexa_min_prefixed_by_0x)
{
	uintmax_t	nbr = 255;

	ft_printf_fd("%#x", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("0xff");
}

Test(ft_printf_fd, should_output_0_and_not_prefix_hexa)
{
	uintmax_t	nbr = 0;

	ft_printf_fd("%#x", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("0");
}

Test(ft_printf_fd, should_output_10_when_8_in_octal)
{
	uintmax_t	nbr = 8;

	ft_printf_fd("%o", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("10");
}

Test(ft_printf_fd, should_prefix_with_0_when_8_octal_prefixed)
{
	uintmax_t	nbr = 8;

	ft_printf_fd("%#o", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("010");
}

Test(ft_printf_fd, should_prefix_with_2_0_when_8_octal_prefixed_and_precision_4)
{
	uintmax_t	nbr = 8;

	ft_printf_fd("%#.4o", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str("0010");
}

Test(ft_printf_fd,\
should_prefix_with_2_0_and_padd_with_3_when_8_octal_prefixed_and_precision_and_field_width)
{
	uintmax_t	nbr = 8;

	ft_printf_fd("%#5.4o", STDOUT_FILENO, nbr);
	cr_expect_stdout_eq_str(" 0010");
}

Test(ft_printf_fd, should_output_string_and_hexa_and_decimal)
{
	uintmax_t	nbr = 255;
	const char	*str = "hexadecimal";
	const char	*str2 = "decimal";

	ft_printf_fd("%i en %s donne %#X en %s.", STDOUT_FILENO, nbr, str2, \
	nbr, str);
	cr_expect_stdout_eq_str("255 en decimal donne 0XFF en hexadecimal.");
}
