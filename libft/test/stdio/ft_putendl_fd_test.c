/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd_test.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:37:51 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:38:18 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <criterion/redirect.h>
#include "lft_stdio.h"

TestSuite(ft_putendl_fd, .init=cr_redirect_stderr);

Test(ft_putendl_fd, should_output_hello_world_with_a_new_line)
{
	ft_putendl_fd("Hello, World!", 2);
	cr_expect_stderr_eq_str("Hello, World!\n");
}
