/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 16:41:26 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 17:32:49 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strcat, should_return_address_of_dst)
{
	char	dst[50] = "Hello,";

	cr_expect_str_eq(ft_strcat(dst, " World!"), "Hello, World!");
}

Test(ft_strcat, should_concatenate_the_two_strings)
{
	char	dst[50] = "Hello,";

	ft_strcat(dst, " World!");
	cr_expect_str_eq(dst, "Hello, World!");
}

Test(ft_strcat, should_concatenate_two_empty_string)
{
	char	dst[50] = "";

	ft_strcat(dst, "");
	cr_expect_str_empty(dst);
}

Test(ft_strcat, should_concatenate_dst_with_empty_string)
{
	char	dst[50] = "Hello,";

	ft_strcat(dst, "");
	cr_expect_str_eq(dst, "Hello,");
}

Test(ft_strcat, should_concatenate_empty_string_with_src)
{
	char	dst[50] = "";

	ft_strcat(dst, " World!");
	cr_expect_str_eq(dst, " World!");
}
