/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcjoin_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 20:56:03 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 20:59:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strcjoin, should_join_the_two_strings)
{
	char	*tst;

	tst = ft_strcjoin("Hello,", " World!", "");
	if (tst)
	{
		cr_expect_str_eq(tst, "Hello, World!", "");
		free(tst);
	}
}

Test(ft_strcjoin, should_join_the_two_string_when_one_empty_string)
{
	char	*tst;

	tst = ft_strcjoin("Hello, ", "", "");
	if (tst)
	{
		cr_expect_str_eq(tst, "Hello, ");
		free(tst);
	}
}

Test(ft_strcjoin, should_join_empty_string_when_two_empty_string)
{
	char	*tst;

	tst = ft_strcjoin("", "", "");
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}

Test(ft_strcjoin, should_join_strings_until_find_character_in_set)
{
	char	*tst;

	tst = ft_strcjoin("Hello,", " World!", "!");
	if (tst)
	{
		cr_expect_str_eq(tst, "Hello, World");
		free(tst);
	}
}

Test(ft_strcjoin, should_join_strings_until_find_character_in_set2)
{
	char	*tst;

	tst = ft_strcjoin("Hello,", " World!", " ,!");
	if (tst)
	{
		cr_expect_str_eq(tst, "Hello");
		free(tst);
	}
}
