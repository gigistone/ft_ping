/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclen_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 17:39:21 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 17:43:41 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strclen, should_return_the_len_of_the_string_when_hello_world)
{
	cr_expect(ft_strclen("Hello, World!", "") == 13);
}

Test(ft_strclen, should_return_zero_when_empty_string)
{
	cr_expect_eq(ft_strclen("", ""), 0);
}

Test(ft_strclen, should_return_one_when_one_character_string)
{
	cr_expect_eq(ft_strclen("\n", ""), 1);
}

Test(ft_strclen, should_return_five_when_found_character_set_string)
{
	cr_expect_eq(ft_strclen("Hello, World!", ",!"), 5);
}

Test(ft_strclen, should_return_zero_when_string_begin_with_character_set)
{
	cr_expect_eq(ft_strclen("\tHello, World!\n", " \t\n!,"), 0);
}
