/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 10:44:29 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 11:04:19 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strlcpy, should_return_the_len_src)
{
	char	tst[50];

	cr_expect_eq(ft_strlcpy(tst, "Hello, World!", sizeof(tst)), 13);
}

Test(ft_strlcpy, should_cpy_string_when_enough_space)
{
	char	tst[50];

	ft_strlcpy(tst, "Hello, World!", sizeof(tst));
	cr_expect_str_eq(tst, "Hello, World!");
}

Test(ft_strlcpy, should_just_cpy_empty_string_if_dstsize_one)
{
	char	tst[50];

	ft_strlcpy(tst, "Hello, World!", 1);
	cr_expect_str_empty(tst);
}

Test(ft_strlcpy, should_truncate_string_when_dstsize_not_enough)
{
	char	tst[50];

	ft_strlcpy(tst, "Hello, World!", 6);
	cr_expect_str_eq(tst, "Hello");
}

Test(ft_strlcpy, should_return_len_of_src_when_dstsize_not_enough)
{
	char	tst[50];

	cr_expect_eq(ft_strlcpy(tst, "Hello, World!", 6), 13);
}

Test(ft_strlcpy, should_do_nothing_when_dstsize_eq_zero)
{
	char	tst[50] = "Hello, World!";

	ft_strlcpy(tst, "Goodbye, my lover.", 0);
	cr_expect_str_eq(tst, "Hello, World!");
}
