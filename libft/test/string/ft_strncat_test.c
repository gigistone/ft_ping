/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 17:52:34 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 18:23:48 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strncat, should_return_the_address_of_dst)
{
	char	dst[50] = "Hello,";

	cr_expect_eq(ft_strncat(dst, " World!", 14), dst);
}

Test(ft_strncat, should_concatenate_two_string_when_enough_space_in_dstsize)
{
	char	dst[50] = "Hello,";

	ft_strncat(dst, " World!", sizeof(dst));
	cr_expect_str_eq(dst, "Hello, World!");
}

Test(ft_strncat, should_truncate_src_when_not_enough_space)
{
	char	dst[50] = "Hello,";

	ft_strncat(dst, " World!", 6);
	cr_expect_str_eq(dst, "Hello, World");
}

Test(ft_strncat, should_concatenate_two_empty_string)
{
	char	dst[50] = "";

	ft_strncat(dst, "", 12);
	cr_expect_str_empty(dst);
}

Test(ft_strncat, should_concatenate_dst_with_empty_string)
{
	char	dst[50] = "Hello,";

	ft_strncat(dst, "", 12);
	cr_expect_str_eq(dst, "Hello,");
}

Test(ft_strncat, should_concatenate_empty_string_with_src)
{
	char	dst[50] = "";

	ft_strncat(dst, " World!", 12);
	cr_expect_str_eq(dst, " World!");
}
