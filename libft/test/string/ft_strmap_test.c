/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 14:10:43 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 14:12:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <ctype.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strmap, should_put_string_to_toupper_when_passing_toupper)
{
	char	*tst;

	tst = ft_strmap("Hello, World!", toupper);
	if (tst)
	{
		cr_expect_str_eq(tst, "HELLO, WORLD!");
		free(tst);
	}
}

Test(ft_strmap, should_do_nothing_when_empty_string)
{
	char	*tst;

	tst = ft_strmap("", tolower);
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}
