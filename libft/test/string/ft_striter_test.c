/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 13:36:07 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 19:31:26 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <ctype.h>
#include "lft_string.h"

Test(ft_striter, should_put_string_to_toupper_when_passing_toupper)
{
	char	tst[] = "Hello, World!";

	ft_striter(tst, toupper);
	cr_expect_str_eq(tst, "HELLO, WORLD!");
}

Test(ft_striter, should_do_nothing_when_empty_string)
{
	char	tst[] = "";

	ft_striter(tst, tolower);
	cr_expect_str_empty(tst);
}
