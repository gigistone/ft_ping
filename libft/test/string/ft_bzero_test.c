/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero_test.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 02:05:01 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 16:55:33 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_bzero, should_erase_data_in_dst_for_the_first_five_bytes)
{
	int	dst = 1;

	ft_bzero(&dst, sizeof(int));
	cr_expect_eq(dst, 0);
}

Test(ft_bzero, should_not_erase_data_when_zero_in_bytes)
{
	int	dst = 1;

	ft_bzero(&dst, 0);
	cr_expect_neq(dst, 0);
}
