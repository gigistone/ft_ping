/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 02:03:15 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 17:52:36 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strdup, should_allocate_new_string)
{
	char	*dup;

	dup = ft_strdup("Hello, World!");
	cr_expect_not_null(dup);
	free(dup);
}

Test(ft_strdup, should_duplicate_string)
{
	char	*dup;

	dup = ft_strdup("Hello, World!");
	cr_expect_not_null(dup);
	if (dup)
	{
		cr_expect_str_eq(dup, "Hello, World!");
		free(dup);
	}
}

Test(ft_strdup, should_duplicate_empty_string)
{
	char	*dup;

	dup = ft_strdup("");
	cr_expect_not_null(dup);
	if (dup)
	{
		cr_expect_str_empty(dup);
		free(dup);
	}
}
