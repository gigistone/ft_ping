/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcdup_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 17:49:47 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 17:53:51 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strcdup, should_allocate_new_string)
{
	char	*dup;

	dup = ft_strcdup("Hello, World!", "");
	cr_expect_not_null(dup);
	free(dup);
}

Test(ft_strcdup, should_duplicate_string)
{
	char	*dup;

	dup = ft_strcdup("Hello, World!", "");
	if (dup)
	{
		cr_expect_str_eq(dup, "Hello, World!");
		free(dup);
	}
}

Test(ft_strcdup, should_duplicate_empty_string)
{
	char	*dup;

	dup = ft_strcdup("", "");
	if (dup)
	{
		cr_expect_str_empty(dup);
		free(dup);
	}
}

Test(ft_strcdup,\
should_duplicate_empty_string_when_found_character_of_set_at_begin)
{
	char	*dup;

	dup = ft_strcdup("\tHello, World!\n", " \t\n!,");
	if (dup)
	{
		cr_expect_str_empty(dup);
		free(dup);
	}
}

Test(ft_strcdup,\
should_duplicate_first_part_of_string_when_found_character_set)
{
	char	*dup;

	dup = ft_strcdup("Hello, World!", " \t\n!,");
	if (dup)
	{
		cr_expect_str_eq(dup, "Hello");
		free(dup);
	}
}