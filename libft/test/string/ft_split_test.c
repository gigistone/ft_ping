/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_test.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 17:14:28 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 18:04:28 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_split, should_split_string_into_2d_str_tabs_using_character_set)
{
	char	**tst;

	tst = ft_split("\tHello, World!\n", " \t\n,!");
	if (tst)
	{
		cr_expect_str_eq(tst[0], "Hello");
		cr_expect_str_eq(tst[1], "World");
		cr_expect_null(tst[2]);
		free(tst[0]);
		free(tst[1]);
		free(tst);
	}
}

Test(ft_split, should_split_string_into_2d_str_tabs_using_character_set2)
{
	char	**tst;

	tst = ft_split("\tHello, General Kenoby!\n", " \t\n,!");
	if (tst)
	{
		cr_expect_str_eq(tst[0], "Hello");
		cr_expect_str_eq(tst[1], "General");
		cr_expect_str_eq(tst[2], "Kenoby");
		cr_expect_null(tst[3]);
		free(tst[0]);
		free(tst[1]);
		free(tst[2]);
		free(tst);
	}
}

Test(ft_split,\
should_split_string_into_2d_str_tabs_using_character_set_when_one_word)
{
	char	**tst;

	tst = ft_split("\tHello\n", " \t\n,!");
	if (tst)
	{
		cr_expect_str_eq(tst[0], "Hello");
		cr_expect_null(tst[1]);
		free(tst[0]);
		free(tst);
	}
}

Test(ft_split,\
should_split_string_into_2d_str_tabs_using_character_set_when_only_charset)
{
	char	**tst;

	tst = ft_split("\t!!!\n", " \t\n!,");
	if (tst)
	{
		cr_expect_null(tst[0]);
		free(tst);
	}
}

Test(ft_split,\
should_split_string_into_2d_str_tabs_using_character_set_when_empty_string)
{
	char	**tst;

	tst = ft_split("", " \t\n!,");
	if (tst)
	{
		cr_expect_null(tst[0]);
		free(tst);
	}
}

Test(ft_split,\
should_split_string_into_2d_str_tabs_using_character_set_when_empty_set_string)
{
	char	**tst;

	tst = ft_split("\tHello, World!\n", "");
	if (tst)
	{
		cr_expect_str_eq(tst[0], "\tHello, World!\n");
		cr_expect_null(tst[1]);
		free(tst[0]);
		free(tst[1]);
		free(tst);
	}
}
