/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 21:19:30 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 21:31:45 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strncmp, should_return_zero_when_string_are_equals)
{
	cr_expect_eq(ft_strncmp("Hello, World!", "Hello, World!", 14), 0);
}

Test(ft_strncmp, should_return_zero_when_string_are_equals_on_first_bytes)
{
	cr_expect_eq(ft_strncmp("Hello, World!", "Hello, General Kenoby", 6), 0);
}

Test(ft_strncmp, should_return_less_than_zero_when_string1_is_lesser)
{
	cr_expect_eq(ft_strncmp("Hello, 1", "Hello, 3", 8), -2);
}

Test(ft_strncmp, should_return_more_than_zero_when_string1_is_greater)
{
	cr_expect_eq(ft_strncmp("Hello, 3", "Hello, 1", 8), 2);
}

Test(ft_strncmp, should_return_zero_when_empty_strings)
{
	cr_expect_eq(ft_strncmp("", "", 200), 0);
}

Test(ft_strncmp, should_return_lesser_than_zero_when_emptry_string1)
{
	cr_expect_eq(ft_strncmp("", "0", 200), -48);
}

Test(ft_strncmp, should_return_greater_than_zero_when_emptry_string2)
{
	cr_expect_eq(ft_strncmp("0", "", 200), 48);
}

Test(ft_strncmp, should_return_zero_when_zero_in_len)
{
	cr_expect_eq(ft_strncmp("Hey", "Nop", 0), 0);
}
