/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/17 22:58:22 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 01:42:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include <criterion/criterion.h>

Test(ft_memcpy, should_cpy_5_bytes_of_two_arrays_of_int)
{
	int	src[5] = {1, 2, 3, 4};
	int	dst[5];

	ft_memcpy(dst, src, 5);
	cr_expect_arr_eq(dst, src, 5);
}

Test(ft_memcpy, should_cpy_3_char_of_string)
{
	char	dst[4];

	ft_memcpy(dst, "hey", 4);
	cr_expect_str_eq(dst, "hey");
}

Test(ft_memcpy, should_cpy_int_to_another_int)
{
	int	src = 1;
	int	dst = 0;

	ft_memcpy(&dst, &src, sizeof(int));
	cr_expect(dst == src);
}

Test(ft_memcpy, should_return_the_addr_of_dst)
{
	int	dst = 0;
	int	src = 0;

	cr_expect_eq(ft_memcpy(&dst, &src, sizeof(int)), &dst);
}

Test(ft_memcpy, should_cpy_nothing_when_zero_in_bytes)
{
	int		dst = 0;

	ft_memcpy(&dst, "Hello, World!", 0);
	cr_expect_eq(dst, 0);
}
