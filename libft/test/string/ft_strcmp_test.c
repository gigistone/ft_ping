/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 03:21:20 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 03:23:20 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strcmp, should_return_zero_when_string_are_equals)
{
	cr_expect_eq(ft_strcmp("Hello, World!", "Hello, World!"), 0);
}

Test(ft_strcmp, should_return_less_than_zero_when_string1_is_lesser)
{
	cr_expect_eq(ft_strcmp("Hello, 1", "Hello, 3"), -2);
}

Test(ft_strcmp, should_return_more_than_zero_when_string1_is_greater)
{
	cr_expect_eq(ft_strcmp("Hello, 3", "Hello, 1"), 2);
}

Test(ft_strcmp, should_return_zero_when_empty_strings)
{
	cr_expect_eq(ft_strcmp("", ""), 0);
}

Test(ft_strcmp, should_return_lesser_than_zero_when_emptry_string1)
{
	cr_expect_eq(ft_strcmp("", "0"), -48);
}

Test(ft_strcmp, should_return_greater_than_zero_when_emptry_string2)
{
	cr_expect_eq(ft_strcmp("0", ""), 48);
}
