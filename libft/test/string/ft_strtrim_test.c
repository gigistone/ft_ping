/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 16:52:23 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 17:11:25 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strtrim, should_trim_character_set_of_begin_and_end_of_string)
{
	char	*tst;

	tst = ft_strtrim("    Hello, World!\t\n", " \t\n");
	if (tst)
	{
		cr_expect_str_eq(tst, "Hello, World!");
		free(tst);
	}
}

Test(ft_strtrim, should_return_empty_string_when_string_full_of_character_set)
{
	char	*tst;

	tst = ft_strtrim("\t\n    \n", " \t\n");
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}

Test(ft_strtrim, should_return_empty_string_when_empty_string)
{
	char	*tst;

	tst = ft_strtrim("", "\t\n");
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}
