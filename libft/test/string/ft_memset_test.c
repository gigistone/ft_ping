/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 01:29:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 01:48:41 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_memset, should_set_tab_with_one_on_first_3_bytes)
{
	int		dst[5];
	char	cmp[3] = {1, 1, 1};

	ft_memset(dst, 1, 3);
	cr_expect_arr_eq(dst, cmp, 3);
}

Test(ft_memset, should_return_the_address_of_dst)
{
	int	dst = 5;

	cr_expect_eq(ft_memset(&dst, 1, sizeof(int)), &dst);
}

Test(ft_memset, should_not_change_dst_when_bytes_eq_zero)
{
	char dst = 5;

	ft_memset(&dst, 1, 0);
	cr_expect_neq(dst, 1);
}

Test(ft_memset, should_write_an_overflow_number_when_bigger_than_255)
{
	char	dst = 1;

	ft_memset(&dst, 256, 1);
	cr_expect_eq(dst, 0);
}
