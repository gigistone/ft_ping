/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 02:20:01 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 03:02:49 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_memccpy, should_return_null_when_c_not_found)
{
	int	dst = 0;
	int	src = 1;

	cr_expect_null(ft_memccpy(&dst, &src, 12, sizeof(int)));
}

Test(ft_memccpy, should_cpy_all_the_first_bytes_when_c_not_found)
{
	int	dst = 0;
	int	src = 225;

	ft_memccpy(&dst, &src, 33, sizeof(int));
	cr_expect_eq(dst, src);
}

Test(ft_memccpy, should_return_the_addr_next_to_c_in_dst_when_c_if_found)
{
	char	dst[15];

	cr_expect_eq(*((char*)ft_memccpy(dst, "Hello, world!", ',', 15) - 1), ',');

}

Test(ft_memccpy, should_cpy_until_find_c)
{
	char	dst[15];
	char	*src;

	src = ft_memccpy(dst, "Hello, world!", ',', 15);
	*src = '\0';
	cr_expect_str_eq(dst, "Hello,");

}

Test(ft_memccpy, should_cpy_nothing_when_bytes_zero)
{
	int	dst = 0;
	int	src = 14;

	ft_memccpy(&dst, &src, 15, 0);
	cr_expect_neq(dst, 14);
}
