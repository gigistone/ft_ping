/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 16:18:40 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 16:23:19 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strcpy, should_return_the_adress_of_dst)
{
	char	dst[50];

	cr_expect_eq(ft_strcpy(dst, "Hello, World!"), dst);
}

Test(ft_strcpy, should_cpy_src_into_dst)
{
	char	dst[50];

	ft_strcpy(dst, "Hello, World!");
	cr_expect_str_eq(dst, "Hello, World!");
}

Test(ft_strcpy, should_cpy_src_empty_string_into_dst)
{
	char	dst[50];

	ft_strcpy(dst, "");
	cr_expect_str_empty(dst);
}
