/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 17:27:52 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 17:40:58 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strchr, should_return_the_address_of_c_in_string_if_found_it)
{
	cr_expect_eq(*ft_strchr("Hello, World!", ','), ',');
}

Test(ft_strchr, should_return_null_when_not_found_c_in_string)
{
	cr_expect_null(ft_strchr("Hello, World!", '1'));
}

Test(ft_strchr, should_return_the_address_of_zero_terminate_byte_in_string)
{
	cr_expect_str_empty(ft_strchr("Hello, World!", '\0'));
}
