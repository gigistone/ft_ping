/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 16:00:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 16:03:51 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strncpy, should_return_the_adress_of_dst)
{
	char	dst[50];

	cr_expect_eq(ft_strncpy(dst, "Hello, World!", 14), dst);
}

Test(ft_strncpy, should_cpy_src_into_dst)
{
	char	dst[50];

	ft_strncpy(dst, "Hello, World!", 14);
	cr_expect_str_eq(dst, "Hello, World!");
}

Test(ft_strncpy, should_cpy_src_empty_string_into_dst)
{
	char	dst[50];

	ft_strncpy(dst, "", 1);
	cr_expect_str_empty(dst);
}

Test(ft_strncpy, should_do_nothing_when_maxlen_equals_zero)
{
	char	*dst = "Hello";

	ft_strncpy(dst, "Hello, World!", 0);
	cr_expect_str_eq(dst, "Hello");
}
