/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 14:32:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 14:50:11 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <ctype.h>
#include <stdlib.h>
#include "lft_string.h"


static int	toupper_even(unsigned int i, int c)
{
	if (i % 2 == 0)
		return (toupper(c));
	return (c);
}

Test(ft_strmapi, should_put_string_to_toupper_when_passing_toupper)
{
	char	*tst;

	tst = ft_strmapi("Hello, World!", toupper_even);
	if (tst)
	{
		cr_expect_str_eq(tst, "HeLlO, WOrLd!");
		free(tst);
	}
}

Test(ft_strmapi, should_do_nothing_when_empty_string)
{
	char	*tst;

	tst = ft_strmapi("", toupper_even);
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}
