/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 15:42:49 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 16:09:44 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_substr, should_return_the_substring_world)
{
	char	*tst;

	tst = ft_substr("Hello, World!", 7, 5);
	if (tst)
	{
		cr_expect_str_eq(tst, "World");
		free(tst);
	}
}

Test(ft_substr, should_return_empty_string_when_len_zero)
{
	char	*tst;

	tst = ft_substr("Hello, World!", 7, 0);
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}

Test(ft_substr, should_return_the_end_of_string_when_excess_len)
{
	char	*tst;

	tst = ft_substr("Hello, World!", 7, 2000);
	if (tst)
	{
		cr_expect_str_eq(tst, "World!");
		free(tst);
	}
}

Test(ft_substr, should_return_empty_string_when_index_out_of_range)
{
	char	*tst;

	tst = ft_substr("Hello, World", 1000, 5);
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}
