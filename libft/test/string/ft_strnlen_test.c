/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnlen_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 02:23:16 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 02:28:45 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strnlen, should_return_the_len_of_the_string_when_hello_world)
{
	cr_expect_eq(ft_strnlen("Hello, World!", 300), 13);
}

Test(ft_strnlen, should_return_zero_when_empty_string)
{
	cr_expect_eq(ft_strnlen("", 300), 0);
}

Test(ft_strnlen, should_return_one_when_one_character_string)
{
	cr_expect_eq(ft_strnlen("\n", 300), 1);
}

Test(ft_strnlen, should_return_zero_when_zero_maxlen)
{
	cr_expect_eq(ft_strnlen("Hello, World!", 0), 0);
}

Test(ft_strnlen, should_return_maxlen_when_len_of_string_superior_to_maxlen)
{
	cr_expect_eq(ft_strnlen("Hello, World!", 5), 5);
}

Test(ft_strnlen, should_return_maxlen_when_len_of_string_equals_to_maxlen)
{
	cr_expect_eq(ft_strnlen("Hello", 5), 5);
}
