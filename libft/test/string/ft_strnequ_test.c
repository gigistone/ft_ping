/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 15:26:48 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 15:30:14 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strnequ, should_return_true_when_strings_are_equals)
{
	cr_expect(ft_strnequ("Hello, World!", "Hello, World!", 200));
}

Test(ft_strnequ, should_return_false_when_strings_are_not_equals)
{
	cr_expect_not(ft_strnequ("Hello, World", "Hello, General Kenoby", 200));
}

Test(ft_strnequ, should_return_true_when_two_empty_strings)
{
	cr_expect(ft_strnequ("", "", 200));
}

Test(ft_strnequ, should_return_false_when_one_string_empty)
{
	cr_expect_not(ft_strnequ("", "Hello, World", 200));
	cr_expect_not(ft_strnequ("Hello, World", "", 200));
}

Test(ft_strequ, should_return_true_when_first_chars_are_equals)
{
	cr_expect(ft_strnequ("Hello, World!", "Hello, general Kenoby", 7));
}

Test(ft_strnequ, should_return_true_when_zero_bytes)
{
	cr_expect(ft_strnequ("Hello", "Goodbye", 0));
}
