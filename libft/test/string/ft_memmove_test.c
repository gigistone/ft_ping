/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 09:15:06 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 09:50:42 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_memmove, should_cpy_bytes_when_areas_not_overlap)
{
	int	dst = 5;
	int	src = 2;

	ft_memmove(&dst, &src, sizeof(int));
	cr_expect_eq(dst, src);
}

Test(ft_memmove, should_return_the_address_of_dst)
{
	int	dst = 5;
	int	src = 2;

	cr_expect_eq(ft_memmove(&dst, &src, sizeof(int)), &dst);
}

Test(ft_memmove, should_move_bytes_when_areas_overlap)
{
	char	tst[50] = "Hello, World!";

	ft_memmove(tst + 5, tst, 6);
	cr_expect_str_eq(tst, "HelloHello,d!");
}

Test(ft_memmove, should_move_bytes_when_areas_overlap2)
{
	char	tst[50] = "123";

	ft_memmove(tst + 1, tst, 3);
	cr_expect_str_eq(tst, "1123");
}

Test(ft_memmove, should_not_move_anything_when_bytes_eq_zero)
{
	int	dst = 5;
	int	src = 2;

	ft_memmove(&dst, &src, 0);
	cr_expect_neq(dst, src);
}
