/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 02:38:51 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 03:12:18 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strndup, should_allocate_new_string)
{
	char	*dup;

	dup = ft_strndup("Hello, World!", 300);
	cr_expect_not_null(dup);
	free(dup);
}

Test(ft_strndup, should_duplicate_string)
{
	char	*dup;

	dup = ft_strndup("Hello, World!", 300);
	cr_expect_not_null(dup);
	if (dup)
	{
		cr_expect_str_eq(dup, "Hello, World!");
		free(dup);
	}
}

Test(ft_strndup, should_duplicate_empty_string)
{
	char	*dup;

	dup = ft_strndup("", 300);
	cr_expect_not_null(dup);
	if (dup)
	{
		cr_expect_str_empty(dup);
		free(dup);
	}
}

Test(ft_strndup, \
should_duplicate_max_characters_of_string_when_len_of_string_superior_at_maxlen)
{
	char	*dup;

	dup = ft_strndup("Hello, World!", 5);
	cr_expect_not_null(dup);
	if (dup)
	{
		cr_expect_str_eq(dup, "Hello");
		free(dup);
	}
}

Test(ft_strndup, should_duplicate_string_when_maxlen_equals_len_of_string)
{
	char	*dup;

	dup = ft_strndup("Hello, World!", 13);
	cr_expect_not_null(dup);
	if (dup)
	{
		cr_expect_str_eq(dup, "Hello, World!");
		free(dup);
	}
}

Test(ft_strndup, should_duplicate_empty_string_when_size_equals_zero)
{
	char	*dup;

	dup = ft_strndup("Hello, World!", 0);
	cr_expect_not_null(dup);
	if (dup)
	{
		cr_expect_str_empty(dup);
		free(dup);
	}
}
