/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 15:40:25 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 15:42:28 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strstr, should_find_substring_in_string_when_it_is_in_the_first_bytes)
{
	cr_expect_str_eq(ft_strstr("Hello, World!", "World"), "World!");
}

Test(ft_strstr, should_return_the_entire_string_when_substring_is_empty)
{
	cr_expect_str_eq(ft_strstr("Hello, World!", ""), "Hello, World!");
}

Test(ft_strstr, should_find_substring_when_two_substring_are_besides)
{
	char	*result;
	
	result = ft_strstr("Hello, World!World\n", "World\n");
	cr_expect_str_eq(result, "World\n");
}


Test(ft_strstr, should_return_null_when_big_is_emptry_string_and_other_not)
{
	cr_expect_null(ft_strstr("", "Hello, World!\n"));
}

Test(ft_strstr, should_find_substring_in_this_weird_test_string)
{
	char	*result;

	result = ft_strstr("HeyheyhEyheYHeyHeYHEY", "heyhEy");
	cr_expect_str_eq(result, "heyhEyheYHeyHeYHEY");
}

Test(ft_strstr, should_find_substring_in_this_weird_test_string2)
{
	char	*result;

	result = ft_strstr("HeyheyhEyheYHeyHeYHEY", "HeY");
	cr_expect_str_eq(result, "HeYHEY");
}

Test(ft_strstr, should_find_substring_in_this_weird_test_string3)
{
	char	*result;

	result = ft_strstr("hehheheeheyhhheeeyyy", "hey");
	cr_expect_str_eq(result, "heyhhheeeyyy");
}
