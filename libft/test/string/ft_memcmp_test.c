/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 10:11:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 10:19:53 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_memcmp, should_return_zero_when_area_are_equals)
{
	int	mem1 = 16000;
	int	mem2 = 16000;

	cr_expect_eq(ft_memcmp(&mem1, &mem2, sizeof(int)), 0);
}

Test(ft_memcmp, should_return_zero_when_first_bytes_are_equals)
{
	cr_expect_eq(ft_memcmp("Hello, World!", "Hello, Lylian!", 7), 0);
}

Test(ft_memcmp, should_return_zero_when_byte_eq_zero)
{
	cr_expect_eq(ft_memcmp("Hello, World!", "Bouh!", 0), 0);
}

Test(ft_memcmp, should_return_greater_than_zero_when_bytes_are_greater)
{
	cr_expect_eq(ft_memcmp("123", "121", 3), 2);
}

Test(ft_memcmp, should_return_lesser_than_zero_when_bytes_lesser)
{
	cr_expect_eq(ft_memcmp("123", "125", 3), -2);
}
