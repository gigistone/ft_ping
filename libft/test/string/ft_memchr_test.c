/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 09:55:23 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 10:01:38 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_memchr, should_return_NULL_when_not_found_c_in_mem)
{
	char	*src = "Hello, World!";

	cr_expect_null(ft_memchr(src, '#', 14));
}

Test(ft_memchr, should_return_NULL_when_not_found_c_first_bytes_in_mem)
{
	char	*src = "Hello, World!";

	cr_expect_null(ft_memchr(src, ',', 5));
}

Test(ft_memchr, should_return_the_address_of_c_in_mem_when_found_it)
{
	char	*src = "Hello, World!";

	cr_expect_eq(*(char*)ft_memchr(src, ',', 14), ',');
}

Test(ft_memchr, should_return_the_address_of_c_in_mem_when_found_it_in_the_byte)
{
	char	*src = "Hello, World!";

	cr_expect_eq(*(char*)ft_memchr(src, ',', 6), ',');
}

Test(ft_memchr, should_return_NULL_when_bytes_equal_zero)
{
	char	*src = "Hello, World!";

	cr_expect_null(ft_memchr(src, 'H', 0));
}
