/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 13:56:07 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 19:31:34 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <ctype.h>
#include "lft_string.h"

static int	toupper_even(unsigned int i, int c)
{
	if (i % 2 == 0)
		return (toupper(c));
	return (c);
}

Test(ft_striteri, should_put_string_to_toupper_when_passing_toupper)
{
	char	tst[] = "Hello, World!";

	ft_striteri(tst, toupper_even);
	cr_expect_str_eq(tst, "HeLlO, WOrLd!");
}

Test(ft_striteri, should_do_nothing_when_empty_string)
{
	char	tst[] = "";

	ft_striteri(tst, toupper_even);
	cr_expect_str_empty(tst);
}
