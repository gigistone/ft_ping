/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strindex_tst.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 12:07:16 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/07 12:10:27 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strindex, should_return_the_index_of_the_character_in_string_when_found)
{
	const char *str = "Hello, World!";

	cr_expect_eq(str[ft_strindex(str, ',')], ',');
}

Test(ft_strindex, \
should_return_negative_value_when_not_found_the_character_in_string)
{
	const char *str = "Hello, World!";

	cr_expect_lt(ft_strindex(str, '@'), 0);
}
