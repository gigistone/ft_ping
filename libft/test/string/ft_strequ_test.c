/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 15:15:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 15:21:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strequ, should_return_true_when_strings_are_equals)
{
	cr_expect(ft_strequ("Hello, World!", "Hello, World!"));
}

Test(ft_strequ, should_return_false_when_strings_are_not_equals)
{
	cr_expect_not(ft_strequ("Hello, World", "Hello, Obi-Wan"));
}

Test(ft_strequ, should_return_true_when_two_empty_strings)
{
	cr_expect(ft_strequ("", ""));
}

Test(ft_strequ, should_return_false_when_one_string_empty)
{
	cr_expect_not(ft_strequ("", "Hello, World"));
	cr_expect_not(ft_strequ("Hello, World", ""));
}
