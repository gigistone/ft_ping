/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 20:42:14 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 21:11:07 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strnstr, should_find_substring_in_string_when_it_is_in_the_first_bytes)
{
	cr_expect_str_eq(ft_strnstr("Hello, World!", "World", 14), "World!");
}

Test(ft_strnstr, should_return_the_entire_string_when_substring_is_empty)
{
	cr_expect_str_eq(ft_strnstr("Hello, World!", "", 14), "Hello, World!");
}

Test(ft_strnstr, should_find_substring_when_two_substring_are_besides)
{
	char	*result;
	
	result = ft_strnstr("Hello, World!World\n", "World\n", 20);
	cr_expect_str_eq(result, "World\n");
}

Test(ft_strnstr, should_not_find_substring_where_no_place_in_len)
{
	char	*result;

	result = ft_strnstr("Hello, World!World\n", "World\n", 18);
	cr_expect_null(result);
}

Test(ft_strnstr, should_return_null_when_big_is_emptry_string_and_other_not)
{
	cr_expect_null(ft_strnstr("", "Hello, World!\n", 1));
}

Test(ft_strnstr, should_return_null_when_len_is_zero)
{
	cr_expect_null(ft_strnstr("Hello, World!", "H", 0));
}

Test(ft_strnstr, should_find_substring_in_this_weird_test_string)
{
	char	*result;

	result = ft_strnstr("HeyheyhEyheYHeyHeYHEY", "heyhEy", 21);
	cr_expect_str_eq(result, "heyhEyheYHeyHeYHEY");
}

Test(ft_strnstr, should_find_substring_in_this_weird_test_string2)
{
	char	*result;

	result = ft_strnstr("HeyheyhEyheYHeyHeYHEY", "HeY", 21);
	cr_expect_str_eq(result, "HeYHEY");
}

Test(ft_strnstr, should_find_substring_in_this_weird_test_string3)
{
	char	*result;

	result = ft_strnstr("hehheheeheyhhheeeyyy", "hey", 20);
	cr_expect_str_eq(result, "heyhhheeeyyy");
}