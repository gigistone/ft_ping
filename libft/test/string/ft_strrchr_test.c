/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 17:41:09 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 17:45:04 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strrchr, should_return_the_last_address_of_c_in_string_when_found_it)
{
	cr_expect_eq(ft_strrchr("Hello, World!", 'l')[1], 'd');
}

Test(ft_strrchr, should_return_null_when_not_found_c_in_string)
{
	cr_expect_null(ft_strrchr("Hello, World!", '1'));
}

Test(ft_strrchr, should_return_the_address_of_null_terminating_byte_when_in_c)
{
	cr_expect_str_empty(ft_strrchr("Hello, World!", '\0'));
}
