/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 18:35:16 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 18:38:09 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strnew, should_allocate_a_new_string)
{
	char	*tst;

	tst = ft_strnew(10);
	cr_expect_not_null(tst);
	if (tst)
		free(tst);
}

Test(ft_strnew, should_allocate_new_empty_string)
{
	char	*tst;

	tst = ft_strnew(10);
	cr_expect_not_null(tst);
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}

Test(ft_strnew, should_allocate_new_empty_string_when_zero)
{
	char	*tst;

	tst = ft_strnew(10);
	cr_expect_not_null(tst);
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}
