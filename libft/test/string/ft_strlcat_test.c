/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 11:14:32 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 19:34:50 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strlcat, should_concatenate_two_string_when_enough_space_in_dstsize)
{
	char	dst[50] = "Hello,";

	ft_strlcat(dst, " World!", sizeof(dst));
	cr_expect_str_eq(dst, "Hello, World!");
}

Test(ft_strlcat, should_return_the_len_of_src_plus_dst_when_dstsize_enough)
{
	char	dst[50] = "Hello,";

	cr_expect_eq(ft_strlcat(dst, " World!", sizeof(dst)), 13);
}

Test(ft_strlcat, should_truncate_src_when_not_enough_space)
{
	char	dst[50] = "Hello,";

	ft_strlcat(dst, " World!", 13);
	cr_expect_str_eq(dst, "Hello, World");
}

Test(ft_strlcat, should_return_len_of_src_plus_len_of_dst_when_truncate_src)
{
	char	dst[50] = "Hello,";

	cr_expect_eq(ft_strlcat(dst, " World!", 13), 13);
}

Test(ft_strlcat, should_not_affect_dst_when_dstsize_inferior_to_len_dst)
{
	char	dst[50] = "Hello,";

	ft_strlcat(dst, " World!", 3);
	cr_expect_str_eq(dst, "Hello,");
}

Test(ft_strlcat, \
should_return_dstsize_plus_len_src_when_dstsize_inferior_to_len_dst)
{
	char	dst[50] = "Hello,";

	cr_expect_eq(ft_strlcat(dst, " World!", 3), 10);
}

Test(ft_strlcat, should_not_affect_dst_when_dstsize_eq_to_len_dst_plus_one)
{
	char	dst[50] = "Hello,";

	ft_strlcat(dst, " World!", 7);
	cr_expect_str_eq(dst, "Hello,");
}
