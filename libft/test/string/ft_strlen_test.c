/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/17 14:33:45 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 01:42:28 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_string.h"

Test(ft_strlen, should_return_the_len_of_the_string_when_hello_world)
{
	cr_expect(ft_strlen("Hello, World!") == 13);
}

Test(ft_strlen, should_return_zero_when_empty_string)
{
	cr_expect_eq(ft_strlen(""), 0);
}

Test(ft_strlen, should_return_one_when_one_character_string)
{
	cr_expect_eq(ft_strlen("\n"), 1);
}
