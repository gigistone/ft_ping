/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin_test.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 16:16:10 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 16:33:15 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_string.h"

Test(ft_strjoin, should_join_the_two_strings)
{
	char	*tst;

	tst = ft_strjoin("Hello,", " World!");
	if (tst)
	{
		cr_expect_str_eq(tst, "Hello, World!");
		free(tst);
	}
}

Test(ft_strjoin, should_join_the_two_string_when_one_empty_string)
{
	char	*tst;

	tst = ft_strjoin("Hello, ", "");
	if (tst)
	{
		cr_expect_str_eq(tst, "Hello, ");
		free(tst);
	}
}

Test(ft_strjoin, should_join_empty_string_when_two_empty_string)
{
	char	*tst;

	tst = ft_strjoin("", "");
	if (tst)
	{
		cr_expect_str_empty(tst);
		free(tst);
	}
}
