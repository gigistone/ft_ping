/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 13:22:22 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 13:31:53 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <string.h>
#include "lft_string.h"

Test(ft_strclr, should_set_string_to_zero_bytes)
{
	char	tst[13] = "Hello, World";
	char	expect[13] = "";

	ft_strclr(tst);
	cr_expect_arr_eq(tst, expect, 13);
}

Test(ft_strclr, should_set_string_to_zero_bytes_when_empty_string)
{
	char	*tst = "";

	ft_strclr(tst);
	cr_expect_str_empty(tst);
}
