/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lmin_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/21 23:36:10 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/21 23:38:31 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_math.h"

Test(ft_lmin, should_return_n1_when_n1_inferior)
{
	cr_expect_eq(ft_lmin(0, 1), 0);
}

Test(ft_lmin, should_return_n2_when_n2_inferior)
{
	cr_expect_eq(ft_lmin(0, -2), -2);
}

Test(ft_lmin, should_return_n1_when_n1_eq_n2)
{
	cr_expect_eq(ft_lmin(1, 1), 1);
}
