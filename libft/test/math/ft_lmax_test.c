/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lmax_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/21 23:41:34 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/21 23:42:48 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include "lft_math.h"

Test(ft_lmax, should_return_n1_when_n1_superior)
{
	cr_expect_eq(ft_lmax(1, 0), 1);
}

Test(ft_lmax, should_return_n2_when_n2_superior)
{
	cr_expect_eq(ft_lmax(-5, 2), 2);
}

Test(ft_lmax, should_return_n1_when_n1_eq_n2)
{
	cr_expect_eq(ft_lmax(1, 1), 1);
}
