/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtol_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/01 07:18:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 05:56:06 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>

#include <errno.h>
#include <limits.h>

#include "lft_stdlib.h"

Test(ft_strtol, should_return_42_when_42_in_base_0)
{
	cr_expect_eq(ft_strtol("42", NULL, 0), 42);
}

Test(ft_strtol, should_return_42_when_42_in_base_10)
{
	cr_expect_eq(ft_strtol("42", NULL, 10), 42);
}

Test(ft_strtol, should_set_strend_to_the_null_terminating_byte_when_only_digits)
{
	char	*str;

	ft_strtol(" \t\v-0X42", &str, 16);
	cr_expect_str_empty(str);
}

Test(ft_strtol, should_return_255_when_ff_in_base_16)
{
	cr_expect_eq(ft_strtol("ff", NULL, 16), 255);
}

Test(ft_strtol, should_return_255_when_ff_in_base_0_and_prefix_with_0X)
{
	cr_expect_eq(ft_strtol("0XFF", NULL, 16), 255);
}

Test(ft_strtol, should_return_14_when_77_in_base_8)
{
	cr_expect_eq(ft_strtol("77", NULL, 8), 63);
}

Test(ft_strtol, should_return_14_when_77_in_base_0_and_prefix_with_0)
{
	cr_expect_eq(ft_strtol("077", NULL, 8), 63);
}

Test(ft_strtol, should_skip_whitespaces_before_number)
{
	char	*str;

	cr_expect_eq(ft_strtol(" \t\n\f\v42", &str, 10), 42);
}

Test(ft_strtol, should_handle_sign)
{
	cr_expect_eq(ft_strtol("+42", NULL, 10), 42);
	cr_expect_eq(ft_strtol("-42", NULL, 10), -42);
}

Test(ft_strtol, should_handle_zero)
{
	cr_expect_eq(ft_strtol("0", NULL, 10), 0);
}

Test(ft_strtol, should_handle_long_min)
{
	cr_expect_eq(ft_strtol("-9223372036854775808", NULL, 10), LONG_MIN);
}

Test(ft_strtol, should_handle_long_max)
{
	cr_expect_eq(ft_strtol("9223372036854775807", NULL, 10), LONG_MAX);
}

Test(ft_strtol,\
should_return_the_number_and_set_endstr_when_find_non_digit_character)
{
	char	*str;

	cr_expect_eq(ft_strtol("\t42>EPITA", &str, 10), 42);
	cr_expect_str_eq(str, ">EPITA");
}

Test(ft_strtol, should_return_0_and_set_errno_when_non_valid_base)
{
	cr_expect_eq(ft_strtol("42", NULL, 1), 0);
	cr_expect_eq(errno, EINVAL);
	errno = 0;
	cr_expect_eq(ft_strtol("42", NULL, 37), 0);
	cr_expect_eq(errno, EINVAL);
	errno = 0;
}

Test(ft_strtol, should_return_long_min_when_underflow_and_set_errno_to_ERANGE)
{
	cr_expect_eq(ft_strtol("-123657483768545648813254647", NULL, 10), LONG_MIN);
	cr_expect_eq(errno, ERANGE);
}

Test(ft_strtol, should_return_long_min_when_overflow_and_set_errno_to_ERANGE)
{
	cr_expect_eq(ft_strtol("1236574837685456488234576897", NULL, 10), LONG_MAX);
	cr_expect_eq(errno, ERANGE);
}

Test(ft_strtol, should_set_endstr_to_the_first_non_digit_char_when_underflow)
{
	char	*str;

	ft_strtol("-123657483768545648813254647homoflexible", &str, 10);
	cr_expect_str_eq(str, "homoflexible");
}

Test(ft_strtol, should_set_errno_to_EINVAL_when_not_found_digits)
{
	ft_strtol("bibou", NULL, 10);
	cr_expect_eq(errno, EINVAL);
}
