/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 01:39:54 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/02 22:05:54 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <limits.h>
#include <stdlib.h>
#include "lft_stdlib.h"

Test(ft_calloc, should_return_allocate_memory_when_allocate_20_char)
{
	char	*tst;

	tst = ft_calloc(20, sizeof(char));
	cr_expect_not_null(tst);
	free(tst);
}

Test(ft_calloc, should_allocate_array_of_5_int_init_to_zeros)
{
	int	ref[5] = {0, 0, 0, 0};
	int	*tst;

	tst = ft_calloc(5, sizeof(int));
	cr_expect_not_null(tst);
	cr_expect_arr_eq(tst, ref, sizeof(ref));
	free(tst);
}

Test(ft_calloc, should_return_addr_when_allocate_zero)
{
	int	*tst;

	tst = ft_calloc(0, sizeof(char));
	cr_expect_not_null(tst);
	free(tst);
}
