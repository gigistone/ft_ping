/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs_test.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 21:38:30 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/02 21:50:58 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <limits.h>
#include "lft_stdlib.h"

Test(ft_abs, should_return_absolute_value_of_minus_42)
{
	cr_expect_eq(ft_abs(-42), 42);
}

Test(ft_abs, should_return_zero_when_zero)
{
	cr_expect_eq(ft_abs(0), 0);
}

Test(ft_abs, should_return_one_when_one)
{
	cr_expect_eq(ft_abs(1), 1);
}

Test(ft_abs, should_return_INT_MIN_when_INT_MIN)
{
	cr_expect_eq(ft_abs(INT_MIN), INT_MIN);
}

Test(ft_abs, should_return_INT_MAX_when_INT_MIN_when_minus_one)
{
	cr_expect_eq(ft_abs(INT_MIN + 1), INT_MAX);
}
