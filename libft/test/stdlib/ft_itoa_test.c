/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 18:56:34 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 19:36:11 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <limits.h>
#include <stdlib.h>
#include "lft_stdlib.h"

Test(ft_itoa, should_allocate_a_new_string)
{
	char	*str;

	str = ft_itoa(12);
	cr_expect_not_null(str);
	if (str)
		free(str);
}

Test(ft_itoa, should_return_number_in_a_string_when_42)
{
	char	*str;

	str = ft_itoa(42);
	if (str)
	{
		cr_expect_str_eq(str, "42");
		free(str);
	}
}

Test(ft_itoa, should_return_number_in_a_string_zero_when_zero)
{
	char	*str;

	str = ft_itoa(0);
	if (str)
	{
		cr_expect_str_eq(str, "0");
		free(str);
	}
}

Test(ft_itoa, should_return_number_in_a_string_when_minus_one)
{
	char	*str;

	str = ft_itoa(-1);
	if (str)
	{
		cr_expect_str_eq(str, "-1");
		free(str);
	}
}

Test(ft_itoa, should_return_int_min_in_a_string_when_int_min_one)
{
	char	*str;

	str = ft_itoa(INT_MIN);
	if (str)
	{
		cr_expect_str_eq(str, "-2147483648");
		free(str);
	}
}

Test(ft_itoa, should_return_int_max_in_a_string_when_int_max)
{
	char	*str;

	str = ft_itoa(INT_MAX);
	if (str)
	{
		cr_expect_str_eq(str, "2147483647");
		free(str);
	}
}
