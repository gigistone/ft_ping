/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 20:09:47 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/16 20:58:20 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <criterion/criterion.h>
#include "lft_stdlib.h"

Test(ft_setenv, should_set_env_correctly)
{
	const char	*name = "Hello";
	const char	*value = "World!";

	cr_expect_eq(ft_setenv(name, value, 0), 0);
	cr_expect_str_eq(ft_getenv(name), value);
	destroy_local_environ();
}

Test(ft_setenv, should_overwrite_env_properly_when_overwrite_on)
{
	const char	*name = "HOME";
	const char	*value = "World!";

	cr_expect_eq(ft_setenv(name, value, 1), 0);
	cr_expect_str_eq(ft_getenv(name), value);
	destroy_local_environ();
}

Test(ft_setenv, should_not_set_when_overwrite_off)
{
	const char	*name = "HOME";
	const char	*value = "World!";

	cr_expect_eq(ft_setenv(name, value, 0), 0);
	cr_expect_str_eq(ft_getenv(name), getenv(name));
	destroy_local_environ();
}

Test(ft_setenv,\
should_return_non_zero_when_name_failed_and_set_errno_to_einval_when_str_empty)
{
	const char	*name = "";
	const char	*value = "World!";

	cr_expect_neq(ft_setenv(name, value, 0), 0);
	cr_expect_null(ft_getenv(name));
	cr_expect_eq(errno, EINVAL);
	destroy_local_environ();
}

Test(ft_setenv,\
should_return_non_zero_when_name_failed_and_set_errno_to_einval_when_str_eq)
{
	const char	*name = "eqrh=equr";
	const char	*value = "World!";

	cr_expect_neq(ft_setenv(name, value, 0), 0);
	cr_expect_null(ft_getenv(name));
	cr_expect_eq(errno, EINVAL);
	destroy_local_environ();
}

Test(ft_setenv, shoud_set_env_when_value_empty)
{
	const char	*name = "Hey";
	const char	*value = "";

	cr_expect_eq(ft_setenv(name, value, 0), 0);
	cr_expect_str_empty(ft_getenv(name));
	destroy_local_environ();
}
