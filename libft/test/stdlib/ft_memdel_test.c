/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdel_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 18:28:11 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/02 22:06:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>
#include "lft_stdlib.h"

Test(ft_memdel, should_put_ptr_to_null)
{
	void	*tst = malloc(1);

	if (tst)
		ft_memdel(&tst);
	cr_expect_null(tst);
}
