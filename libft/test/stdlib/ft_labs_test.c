/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_labs_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 21:55:01 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/02 22:01:56 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <limits.h>
#include "lft_stdlib.h"

Test(ft_labs, should_return_absolute_value_of_minus_42)
{
	cr_expect_eq(ft_labs(-42), 42);
}

Test(ft_labs, should_return_zero_when_zero)
{
	cr_expect_eq(ft_labs(0), 0);
}

Test(ft_labs, should_return_one_when_one)
{
	cr_expect_eq(ft_labs(1), 1);
}

Test(ft_labs, should_return_INT_MIN_when_INT_MIN)
{
	cr_expect_eq(ft_labs(LONG_MIN), LONG_MIN);
}

Test(ft_labs, should_return_INT_MAX_when_INT_MIN_when_minus_one)
{
	cr_expect_eq(ft_labs(LONG_MIN + 1), LONG_MAX);
}
