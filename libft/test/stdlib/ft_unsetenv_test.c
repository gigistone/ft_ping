/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 20:45:01 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/16 20:53:01 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <criterion/criterion.h>
#include "lft_stdlib.h"

Test(ft_unsetenv, should_delete_env_when_exist)
{
	const char	*name = "HOME";

	cr_expect_eq(ft_unsetenv(name), 0);
	cr_expect_null(ft_getenv(name));
	destroy_local_environ();
}

Test(ft_unsetenv, should_return_0_and_do_nothing_when_not_find_env)
{
	const char	*name = "newrviewnr";

	cr_expect_eq(ft_unsetenv(name), 0);
	cr_expect_null(ft_getenv(name));
	destroy_local_environ();
}

Test(ft_unsetenv,\
should_return_non_zero_when_name_failed_and_set_errno_to_einval_when_str_empty)
{
	const char	*name = "";

	cr_expect_neq(ft_unsetenv(name), 0);
	cr_expect_eq(errno, EINVAL);
	destroy_local_environ();
}

Test(ft_unsetenv,\
should_return_non_zero_when_name_failed_and_set_errno_to_einval_when_str_eq)
{
	const char	*name = "eqrh=equr";

	cr_expect_neq(ft_unsetenv(name), 0);
	cr_expect_eq(errno, EINVAL);
	destroy_local_environ();
}

Test(ft_unsetenv,\
should_return_non_zero_when_name_failed_and_set_errno_to_einval_when_str_null)
{
	const char	*name = NULL;

	cr_expect_neq(ft_unsetenv(name), 0);
	cr_expect_eq(errno, EINVAL);
	destroy_local_environ();
}