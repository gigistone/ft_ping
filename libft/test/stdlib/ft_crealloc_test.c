/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_crealloc_test.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 22:02:06 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 22:09:03 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <string.h>
#include "lft_stdlib.h"

Test(ft_crealloc, should_reallocate_ptr_and_cpy_past_values)
{
	char	*str = strdup("Hello");

	str = ft_crealloc(str, strlen(str), strlen("Hello, World!"));
	cr_expect_not_null(str);
	strcat(str, ", World!");
	cr_expect_str_eq(str, "Hello, World!");
	free(str);
}

Test(ft_crealloc, should_truncate_string_when_reallocate_minus_memory)
{
	char	*str = strdup("Hello, World!");

	str = ft_crealloc(str, strlen(str), strlen("Hello"));
	cr_expect_not_null(str);
	cr_expect_str_eq(str, "Hello");
	free(str);
}

Test(ft_crealloc, should_set_memory_exceed_bytes_to_zero)
{
	char	cmp[10] = "Hello";
	char	*str = strdup("Hello");

	str = ft_crealloc(str, strlen(str), strlen(str) + 4);
	cr_expect_arr_eq(str, cmp, sizeof(cmp));
	free(str);
}
