/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getenv_test.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 11:14:52 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/16 11:40:31 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <stdlib.h>

#include "lft_stdlib.h"

Test(ft_getenv, ft_getenv_should_return_null_when_not_find)
{
	const char	*name = "OIUREHIEQUHFE31252CBEQR";

	cr_expect_null(ft_getenv(name));
	destroy_local_environ();
}

Test(ft_getenv, should_return_correct_value_when_find_env)
{
	const char	*name = "Hey";
	const char	*value = "Hello, World!";

	setenv(name, value, 1);
	cr_expect_str_eq(ft_getenv(name), value);
	destroy_local_environ();
}

Test(ft_getenv, should_return_null_when_not_find_env_name)
{
	const char	*name = "Hey";
	const char	*value = "Hello, World!\n";

	setenv("HeyA", value, 1);
	cr_expect_null(ft_getenv(name));
	destroy_local_environ();
}

Test(ft_getenv, should_return_correct_value_when_find_env_name)
{
	const char	*name = "Heyb";
	const char	*value = "Hello, World!\n";

	setenv("Heya", "truc", 1);
	setenv("Heyb", value, 1);
	cr_expect_str_eq(ft_getenv(name), value);
	destroy_local_environ();
}
