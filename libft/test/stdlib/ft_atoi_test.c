/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_test.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 21:50:46 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 22:36:01 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <criterion/criterion.h>
#include <limits.h>
#include "lft_stdlib.h"

Test(ft_atoi, should_return_nbr_when_clean_string)
{
	cr_expect_eq(ft_atoi("123"), 123);
}

Test(ft_atoi, should_return_negative_nbr_when_clean_string)
{
	cr_expect_eq(ft_atoi("-123"), -123);
}

Test(ft_atoi, should_return_zero_when_zero_clean_string)
{
	cr_expect_eq(ft_atoi("0"), 0);
}

Test(ft_atoi, should_return_zero_when_minus_zero)
{
	cr_expect_eq(ft_atoi("-0"), 0);
}

Test(ft_atoi, should_return_nbr_when_plus_clean_string)
{
	cr_expect_eq(ft_atoi("+123"), 123);
}

Test(ft_atoi, should_return_zero_when_multiple_signs)
{
	cr_expect_eq(ft_atoi("+-123"), 0);
}

Test(ft_atoi, should_return_nbr_when_whitespaces_before)
{
	cr_expect_eq(ft_atoi("\t\n\v\f-123"), -123);
}

Test(ft_atoi, should_return_zero_when_invalid_character)
{
	cr_expect_eq(ft_atoi("abc123"), 0);
}

Test(ft_atoi, should_return_nbr_before_invalid_character)
{
	cr_expect_eq(ft_atoi("\t\n\v\f -123b200"), -123);
}

Test(ft_atoi, should_return_nbr_when_int_min)
{
	cr_expect_eq(ft_atoi("-2147483648"), INT_MIN);
}

Test(ft_atoi, should_return_nbr_when_int_max)
{
	cr_expect_eq(ft_atoi("+2147483647"), INT_MAX);
}
