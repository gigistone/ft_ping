# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/17 11:29:03 by nail0x            #+#    #+#              #
#    Updated: 2020/10/22 13:23:13 by lperson-         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#This makefile produce libft.a
#libft.a is a static library that contains most of standard c functions.

NAME := libft.a

MAKE := make -s -C
MKDIR := mkdir -p
RM := rm -rf

AR := ar
ARFLAGS := -crs

CC := gcc
CFLAGS := -Wall -Wextra -Werror

CFLAGS += -g

PATHI := include
PATHS := srcs
PATHB := build
PATHT := test

#Find all subdirectories and update vpath.
SRCS_DIR := $(shell find $(PATHS) -type d)
export INCS_DIR := $(shell find $(PATHI) -type d)

vpath %.c $(foreach dir, $(SRCS_DIR), $(dir):)
vpath %.h $(foreach dir, $(INCS_DIR), $(dir):)

#Update include search for gcc.
CFLAGS += $(addprefix -I , $(INCS_DIR))

#Put srcs files and transcript into objs built in pathb.
SRCS := \
ft_calloc.c \
ft_crealloc.c \
ft_memdel.c \
ft_memset.c \
ft_bzero.c \
ft_memcpy.c \
ft_memccpy.c \
ft_memmove.c \
ft_memchr.c \
ft_memcmp.c \
ft_strnew.c \
ft_strdel.c \
ft_strclr.c \
ft_strlen.c \
ft_strnlen.c \
ft_strclen.c \
ft_strcpy.c \
ft_strncpy.c \
ft_strlcpy.c \
ft_strcat.c \
ft_strncat.c \
ft_strlcat.c \
ft_strjoin.c \
ft_strcjoin.c \
ft_strdup.c \
ft_strndup.c \
ft_strcdup.c \
ft_strchr.c \
ft_strrchr.c \
ft_strindex.c \
ft_strstr.c \
ft_strnstr.c \
ft_strcmp.c \
ft_strncmp.c \
ft_strequ.c \
ft_strnequ.c \
ft_striter.c \
ft_striteri.c \
ft_strmap.c \
ft_strmapi.c \
ft_substr.c \
ft_strtrim.c \
ft_split.c \
ft_islower.c \
ft_isupper.c \
ft_isalpha.c \
ft_isdigit.c \
ft_isalnum.c \
ft_ispunct.c \
ft_isspace.c \
ft_isblank.c \
ft_isprint.c \
ft_isgraph.c \
ft_iscntrl.c \
ft_isascii.c \
ft_tolower.c \
ft_toupper.c \
ft_atoi.c \
ft_itoa.c \
ft_strtol.c \
ft_abs.c \
ft_labs.c \
local_environ.c \
ft_getenv.c \
ft_setenv.c \
ft_unsetenv.c \
ft_lmin.c \
ft_lmax.c \
ft_putchar.c \
ft_putstr.c \
ft_putendl.c \
ft_putnbr.c \
ft_putchar_fd.c \
ft_putstr_fd.c \
ft_putendl_fd.c \
ft_putnbr_fd.c \
fds_new.c \
get_next_line.c \
stream_init.c \
stream_flush.c \
stream_add_byte.c \
stream_add_nbytes.c \
stream_add_string.c \
stream_add_nstring.c \
stream_add_number.c \
stream_add_unumber.c \
get_uintmax.c \
get_padding_arg.c \
get_specifier_value.c \
get_fmt_datas.c \
bases.c \
process_char.c \
process_str.c \
process_int.c \
process_uint.c \
process_fmt.c \
ft_printf_fd.c \
ft_str_array_len.c \
ft_str_array_cpy.c \
ft_str_array_dup.c \
ft_str_array_append.c \
ft_str_array_join.c \
ft_str_array_del.c \
ft_vector_new.c \
ft_vector_from.c \
ft_vector_get.c \
ft_vector_len.c \
ft_vector_capacity.c \
ft_vector_data.c \
ft_vector_set.c \
vector_realloc.c \
ft_vector_push_front.c \
ft_vector_push_back.c \
ft_vector_insert.c \
ft_vector_pop_front.c \
ft_vector_pop_back.c \
ft_vector_erase.c \
ft_vector_clear.c

OBJS := $(addprefix $(PATHB)/, $(SRCS:%.c=%.o))

.PHONY: all clean fclean re check

all: $(NAME)

$(NAME): $(OBJS)
	$(AR) $(ARFLAGS) $@ $^

$(PATHB)/%.o: %.c | $(PATHB)
	$(CC) $(CFLAGS) -c $< -o $@

$(PATHB):
	$(MKDIR) $@

clean:
	$(RM) $(PATHB)

fclean: clean
	$(RM) $(NAME)

re: fclean all

#include makefile extension for tests.
include $(PATHT)/mod.mk
