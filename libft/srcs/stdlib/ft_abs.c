/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_abs.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 21:34:46 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/02 21:59:56 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>

/*
** Compute the absolute value of an integer.
** ---
** dsc: Compute the absolute value of an integer you idiot.
** arg: the number to compute (int)
** ret: the absolute value of number. Be aware if it's INT_MIN just return
**		INT_MIN without changing him (because of integer overflow) (int)
** not: Beeeeeh be aware of INT_MIN.
*/

int	ft_abs(int nbr)
{
	return (nbr < 0 && nbr != INT_MIN ? -nbr : nbr);
}
