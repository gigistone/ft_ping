/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_internals.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 10:44:03 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/17 15:33:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ENV_INTERNALS_H

# define ENV_INTERNALS_H

char	**get_local_environ(void);
void	destroy_local_environ(void);
void	set_local_environ(char ***new_environ);

#endif
