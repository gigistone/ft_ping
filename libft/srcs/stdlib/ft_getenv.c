/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 11:02:54 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/16 19:07:51 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include "env_internals.h"

/*
** Search for env variable in local env and return its value
** ---
** dsc: Create a local env that must be freed by calling destroy_local_env
**		before end of program and search for env name in it to return its value.
** arg: the name search in local environement (const char *)
** ret: null if env not found or local env failed, the pointer to the value
**		otherwise (char *)
** not: Set errno if ran out of memory.
*/

char	*ft_getenv(const char *name)
{
	char			**local_environ;
	const size_t	name_len = ft_strlen(name);
	int				i;

	i = 0;
	local_environ = get_local_environ();
	if (!local_environ)
		return (NULL);
	while (local_environ[i])
	{
		if (ft_strncmp(local_environ[i], name, name_len) == 0 \
		&& local_environ[i][name_len] == '=')
			return (local_environ[i] + name_len + 1);
		i++;
	}
	return (NULL);
}
