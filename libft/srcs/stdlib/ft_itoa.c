/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 19:22:01 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:32:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Count the digits in the number.
*/

static size_t	count_digits(int nbr)
{
	size_t		len;

	if (nbr == 0)
		return (1);
	len = 0;
	while (nbr)
	{
		len++;
		nbr /= 10;
	}
	return (len);
}

/*
** Integer to ascii.
** ---
** dsc: Allocate a new string that contains the integer into ascii format.
** arg: the integer to convert in ascii (int)
** ret: the freshly new allocated string or null in case of malloc error
**		(char *)
** not: Set ENOMEM if ran out of memory.
*/

char			*ft_itoa(int nbr)
{
	register char	*itoa;
	size_t			digits;
	unsigned		nb;

	digits = nbr < 0 ? count_digits(nbr) + 1 : count_digits(nbr);
	nb = nbr < 0 ? -nbr : nbr;
	itoa = (char*)malloc(sizeof(char) * (digits + 1));
	if (!itoa)
		return (NULL);
	itoa[digits] = '\0';
	while (digits--)
	{
		itoa[digits] = nb % 10 + '0';
		nb /= 10;
	}
	if (nbr < 0)
		itoa[0] = '-';
	return (itoa);
}
