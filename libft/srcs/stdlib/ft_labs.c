/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_labs.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/02 21:56:56 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/02 22:02:33 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>

/*
** Compute the absolute value of a long integer.
** ---
** dsc: You know to read, no ?
** arg: the long number the compute (long)
** ret: the absolute value of the number or LONG_MIN if LONG_MIN, to handler
**		long overflow (long)
** not: n/a
*/

long	ft_labs(long nbr)
{
	return (nbr < 0 && nbr != LONG_MIN ? -nbr : nbr);
}
