/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 18:29:39 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 18:31:41 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Free pointer derefenced by address_ptr and put it to NULL.
** ---
** dsc: Free pointer derefenced by address_ptr and put it to NULL.
** arg: the address of pointer (void **)
** ret: n/a
** not: pointer must have be allocated.
*/

void	ft_memdel(void **address_ptr)
{
	free(*address_ptr);
	*address_ptr = NULL;
}
