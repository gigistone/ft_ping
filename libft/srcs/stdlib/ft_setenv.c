/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_setenv.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 18:31:21 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/21 03:34:14 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <errno.h>
#include "lft_common.h"
#include "lft_string.h"

#include "env_internals.h"

static char	*produce_env(const char *name, const char *value)
{
	const size_t	name_len = ft_strlen(name);
	const size_t	value_len = ft_strlen(value);
	char			*env;

	env = malloc(sizeof(char) * (name_len + value_len + 2));
	if (!env)
		return (NULL);
	while (*name)
		*env++ = *name++;
	*env++ = '=';
	while (*value)
		*env++ = *value++;
	*env = '\0';
	return (env - (name_len + value_len + 1));
}

static int	get_env_index(const char *name, char **local_environ)
{
	const size_t	name_len = ft_strlen(name);
	int				i;

	i = 0;
	while (local_environ[i])
	{
		if (ft_strncmp(name, local_environ[i], name_len) == 0 \
		&& local_environ[i][name_len] == '=')
			return (i);
		i++;
	}
	return (-1);
}

static int	append_local_environ(char *env, char **local_environ)
{
	const size_t	local_environ_len = ft_str_array_len(local_environ);
	int				i;
	char			**new_environ;

	new_environ = malloc(sizeof(char*) * (local_environ_len + 2));
	if (!new_environ)
	{
		free(env);
		return (-1);
	}
	i = 0;
	while (local_environ[i])
	{
		new_environ[i] = local_environ[i];
		i++;
	}
	new_environ[i] = env;
	new_environ[i + 1] = NULL;
	set_local_environ(&new_environ);
	return (0);
}

/*
** Set an environnment variable.
** ---
** dsc: Set an environnement variable in local env. Overwrite it if specified.
** arg: the name of the variable, name must not contain '=' or be NULL or an
**		empty string (const char *)
**		the value of the variable (const char *)
**		overwrite the value of the variable if exist and if non zero (int)
** ret: non zero on failure (int)
** not: Set errno to EINVAL if name not valid or ENOMEM if failed to add name.
*/

int			ft_setenv(const char *name, const char *value, int overwrite)
{
	char	**local_environ;
	char	*env;
	int		i;

	if (!name || !*name || ft_strchr(name, '='))
	{
		errno = EINVAL;
		return (-1);
	}
	if (!(local_environ = get_local_environ()))
		return (-1);
	i = get_env_index(name, local_environ);
	if (i >= 0 && !overwrite)
		return (0);
	env = produce_env(name, value);
	if (!env)
		return (-1);
	if (i >= 0)
	{
		free(local_environ[i]);
		local_environ[i] = env;
	}
	else
		return (append_local_environ(env, local_environ));
	return (0);
}
