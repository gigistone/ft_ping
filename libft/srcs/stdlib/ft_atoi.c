/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 22:02:20 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:37:29 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_ctype.h"

/*
** Convert a string containing an number into an integer.
** ---
** dsc: Skip whitespaces, find the sign (+ or -) and convert the first number
** in base 10 that we found. Return 0 in case of string error.
** arg: The format string that contains the number (const char *)
** ret: The first number that it found, or zero.
** not: The errors and overflows are not correctly handle.
**		Prefer to use strtoll.
*/

int		ft_atoi(const char *str)
{
	int		nbr;
	int		neg;

	nbr = 0;
	while (ft_isspace(*str))
		str++;
	neg = *str == '-' ? -1 : 1;
	if (*str == '+' || *str == '-')
		str++;
	while (*str >= '0' && *str <= '9')
		nbr = nbr * 10 + *str++ - '0';
	return (nbr * neg);
}
