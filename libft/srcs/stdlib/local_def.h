/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   local_def.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/20 21:35:51 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:37:20 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LOCAL_DEF_H

# define LOCAL_DEF_H

# define BASE_STR	"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

#endif
