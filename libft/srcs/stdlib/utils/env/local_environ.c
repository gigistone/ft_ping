/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   local_environ.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 10:45:21 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/17 15:36:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stddef.h>
#include "lft_common.h"

static char	**g_local_environ = NULL;

/*
** Create an array of local environnement variables
** ---
** dsc: Duplicate environ variable to g_local_environ for dynamically
**		handle environnement variables.
** arg: n/a
** ret: the local environnement variable or NULL in case of error (char **)
** not: Set errno if ran out of memory.
*/

char	**get_local_environ(void)
{
	extern char	**environ;

	if (!g_local_environ)
		g_local_environ = ft_str_array_dup(environ);
	if (!g_local_environ)
		return (NULL);
	return (g_local_environ);
}

/*
** Free and destroy local environnement variable
** ---
** dsc: Free allocated environ and his entries.
** arg: n/a
** ret: n/a
** not: n/a
*/

void	destroy_local_environ(void)
{
	if (g_local_environ)
		ft_str_array_del(&g_local_environ);
}

/*
** Change local_environ to the new environnement array.
*/

void	set_local_environ(char ***new_environ)
{
	free(g_local_environ);
	g_local_environ = *new_environ;
}
