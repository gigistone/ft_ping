/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtol.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/01 08:32:11 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:36:49 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include <errno.h>

#include "local_def.h"

#include "lft_ctype.h"
#include "lft_string.h"

/*
** Return the index of c in str, -1 if not found.
*/

static int	find_index(const char *str, int c)
{
	register const char	*iter = str;

	while (*iter)
	{
		if (*iter == c)
			return (iter - str);
		iter++;
	}
	return (-1);
}

/*
** Update the base, check the prefix and if it's valid.
** not: Set errno to EINVAL if not valid.
*/

static int	update_base(const char *str, int base)
{
	if (base == 1 || base > 36)
	{
		errno = EINVAL;
		return (0);
	}
	if (base == 0 && *str == '0' && ft_toupper(str[1]) == 'X')
		return (16);
	else if (base == 0 && *str == '0')
		return (8);
	else if (base == 0)
		return (10);
	return (base);
}

/*
** Skip the prefix for hexadecimal or octal values.
*/

static char	*skip_prefix(const char *str, int base)
{
	if ((base == 8 || base == 16) && *str == '0')
		str++;
	if (base == 16 && ft_toupper(*str) == 'X')
		str++;
	return ((char*)str);
}

/*
** Return LONG_MIN if underflow or LONG_MAX. Set endstr to the last non-digit
** character if necessary and set errno to ERANGE.
*/

static long	handle_over_underflow(const char *str, char **endstr, int neg, \
int base)
{
	while (ft_memchr(BASE_STR, *str, base))
		str++;
	if (endstr)
		*endstr = (char*)str;
	errno = ERANGE;
	return (neg == 1 ? LONG_MAX : LONG_MIN);
}

/*
** Convert a string to a long integer.
** ---
** dsc: Convert the first digits in string into int with the help of the
**		specified base wich is between 2 and 36 inclusive. Accept whitespaces
**		a sign (+ or -) and base prefix (0x for hexadecimal, 0 for octal).
**		The base can be 0, in this case check the prefix to guess the base of
**		number.
**		If endstr is not null stock the first non-digit character in it.
** arg: the string where the number is stock (const char *)
**		the address of the string that stock the first non-digit character
**		(char **)
**		the base value number, must be between 2-36 inclusive (int)
** ret: the number converted or LONG_MIN in case of underflow, or LONG_MAX in
**		case of overflow (long int)
** not: Set errno to ERANGE if underoverflow or overflow. Set errno to EINVAL
**		if base is not valid or found no valable digit and return 0.
*/

long int	ft_strtol(const char *str, char **endstr, int base)
{
	unsigned long long int	nbr;
	char					neg;
	char					has_digit;

	nbr = 0;
	has_digit = 0;
	while (ft_isspace(*str))
		str++;
	neg = *str == '-' ? -1 : 1;
	if (*str == '-' || *str == '+')
		str++;
	base = update_base(str, base);
	str = skip_prefix(str, base);
	while (ft_memchr(BASE_STR, ft_toupper(*str), base))
	{
		has_digit = 1;
		nbr = nbr * base + find_index(BASE_STR, ft_toupper(*str++));
		if ((nbr > LONG_MAX && neg == 1) || nbr > (unsigned long)LLONG_MAX + 1)
			return (handle_over_underflow(str, endstr, neg, base));
	}
	if (endstr)
		*endstr = (char*)str;
	if (!has_digit)
		errno = EINVAL;
	return (nbr * neg);
}
