/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_crealloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 22:10:17 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 22:17:12 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include "lft_stdlib.h"

/*
** Realloc the new area with the new size.
** ---
** dsc: Realloc the new area with the new size, copy it at most n bytes of old
**		size, can truncate it if new size is inferior to old size and free the
**		old pointer to return the new.
**		Fill the extra bytes with zero.
** arg: the old pointer to realloc (void *)
**		the old size in memory of the pointer (size_t)
**		the new size allocated for the new pointer (size_t)
** ret: the new address allocated and copied from the old pointer, or null in
**		case of malloc error (void *)
** not: Set errno to ENOMEM if ran out of memory.
*/

void	*ft_crealloc(void *ptr, size_t old_size, size_t new_size)
{
	const size_t	size_to_cpy = old_size > new_size ? new_size : old_size;
	void			*new;

	new = ft_calloc(new_size, 1);
	if (!new)
	{
		ft_memdel(&ptr);
		return (NULL);
	}
	ft_memcpy(new, ptr, size_to_cpy);
	ft_memdel(&ptr);
	return (new);
}
