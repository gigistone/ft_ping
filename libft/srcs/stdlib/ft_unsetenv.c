/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_unsetenv.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/16 20:31:04 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/16 20:52:16 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <errno.h>
#include <stdlib.h>

#include "lft_string.h"
#include "lft_common.h"

#include "env_internals.h"

static int	get_env_index(const char *name, char **local_environ)
{
	const size_t	name_len = ft_strlen(name);
	int				i;

	i = 0;
	while (local_environ[i])
	{
		if (ft_strncmp(name, local_environ[i], name_len) == 0 \
		&& local_environ[i][name_len] == '=')
			return (i);
		i++;
	}
	return (-1);
}

static int	delete_index(int index, char **strs)
{
	const size_t	strs_len = ft_str_array_len(strs);
	char			**new_environ;
	int				i;

	i = 0;
	new_environ = malloc(sizeof(char*) * strs_len);
	if (!new_environ)
		return (-1);
	while (strs[i])
	{
		if (i != index)
			new_environ[i] = strs[i];
		else
			free(strs[i]);
		i++;
	}
	new_environ[i] = NULL;
	set_local_environ(&new_environ);
	return (0);
}

int			ft_unsetenv(const char *name)
{
	char	**local_environ;
	int		i;

	local_environ = get_local_environ();
	if (!local_environ)
		return (-1);
	if (!name || !*name || ft_strchr(name, '='))
	{
		errno = EINVAL;
		return (-1);
	}
	i = get_env_index(name, local_environ);
	if (i < 0)
		return (0);
	return (delete_index(i, local_environ));
}
