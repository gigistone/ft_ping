/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 01:48:43 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:31:45 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Allocate count * size elements and set them to zero.
** ---
** dsc: Try to allocate count * size elements setted to zero.
** arg: the number of elements to allocated (size_t)
**		the size of each element (size_t)
** ret: the address of the new allocated array, or null if no enough memory.
** not: Set ENOMEM for out of memory. Return a ptr to memory area when try to
**		allocate zero. This memory area can only be passed to free and not be
**		dereferenced.
*/

void	*ft_calloc(size_t count, size_t size)
{
	register size_t			allocate_size;
	register unsigned char	*memory_area;

	allocate_size = count * size;
	memory_area = (unsigned char*)malloc(count * size);
	if (!memory_area)
		return (NULL);
	while (allocate_size--)
		*memory_area++ = 0;
	return (memory_area - count * size);
}
