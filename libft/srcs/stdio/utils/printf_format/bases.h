/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bases.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/07 12:08:54 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 14:32:12 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BASES_H

# define BASES_H

# include "process_fmt.h"

# define B_10				"0123456789"
# define B_8				"01234567"
# define B_16_MIN			"0123456789abcdef"
# define B_16_MAX			"0123456789ABCDEF"

# define PREFIX_B_8			"0"
# define PREFIX_B_16_MIN	"0x"
# define PREFIX_B_16_MAX	"0X"

char	*get_base_str(t_specifier specifier);
char	*get_base_prefix(t_specifier specifier);

#endif
