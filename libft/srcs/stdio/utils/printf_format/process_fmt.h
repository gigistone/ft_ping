/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_fmt.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 11:15:33 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 12:38:07 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROCESS_FMT_H

# define PROCESS_FMT_H

# include <stdarg.h>
# include "../stream/stream.h"
# include "../printf_parse/printf_get_datas.h"

/*
** the number of specifier handled
*/

# define	HANDLE_LEN		7
# define	NULL_STRING		"(null)"

typedef int (*t_processes[HANDLE_LEN])(t_printf_datas, t_stream *, va_list);

int			process_char(t_printf_datas datas, t_stream *stream, va_list args);
int			process_str(t_printf_datas datas, t_stream *stream, va_list args);
int			process_int(t_printf_datas datas, t_stream *stream, va_list args);
int			process_uint(t_printf_datas datas, t_stream *stream, va_list args);
char		*process_fmt(const char *fmt, t_stream *stream, va_list args);

#endif
