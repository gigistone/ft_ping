/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_char.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/16 12:22:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/16 13:40:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./process_fmt.h"

static char	get_char_value(unsigned pos, va_list args)
{
	va_list	copy;
	char	character;

	if (!pos)
		return ((char)va_arg(args, int));
	va_copy(copy, args);
	while (pos--)
		character = (char)va_arg(copy, int);
	return (character);
}

static void	compute_datas(t_printf_datas *datas)
{
	if (datas->field_width > 1)
		datas->field_width--;
	datas->flags &= ~(PRECISION | ZERO_PADD | PREFIX | SPACE_PADD | SIGN_PADD);
	datas->precision = 0;
}

/*
** Format string and output the specified char
** ---
** dsc: Format padding, handle some flags and output the character.
** arg: the datas used to format the string (t_printf_datas)
**		the address of the stream used to output the string (t_stream *)
**		the list of the args passed to printf (va_list)
** ret:	the number of bytes totally outputted or a negative value in case of
**		error (int)
** not: Set errno in case of syscall error.
*/

int			process_char(t_printf_datas datas, t_stream *stream, va_list args)
{
	const char	character = get_char_value(datas.specifier_pos, args);

	compute_datas(&datas);
	if (!(datas.flags & LEFT_PADD))
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	if (stream_add_byte(stream, character) < 0)
		return (-1);
	if (datas.flags & LEFT_PADD)
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	return (stream->outputted);
}
