/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_int.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 08:38:17 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 14:28:24 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./bases.h"
#include "./process_fmt.h"

static intmax_t	get_number_from_varray(t_printf_datas datas, va_list args)
{
	intmax_t	number;
	va_list		copy;

	if (!datas.specifier_pos)
		return (va_arg(args, intmax_t));
	va_copy(copy, args);
	while (datas.specifier_pos--)
		number = va_arg(copy, intmax_t);
	va_end(copy);
	return (number);
}

static intmax_t	get_int(t_printf_datas datas, va_list args)
{
	intmax_t	number;

	number = get_number_from_varray(datas, args);
	if (datas.length_mod == 0)
		return ((int)number);
	if (datas.length_mod == LOW_LOW)
		return ((char)number);
	else if (datas.length_mod == LOW)
		return ((short)number);
	else if (datas.length_mod == HIGH)
		return ((long)number);
	else if (datas.length_mod == HIGH_HIGH)
		return ((long long)number);
	else if (datas.length_mod == SIZE_T)
		return ((size_t)number);
	else if (datas.length_mod == PTRDIFF_T)
		return ((ptrdiff_t)number);
	else
		return (number);
}

static size_t	count_digits(intmax_t nbr, unsigned base)
{
	uintmax_t	nb;
	size_t		digits;

	digits = 1;
	nb = nbr < 0 ? -nbr : nbr;
	while (nb >= base)
	{
		digits++;
		nb /= base;
	}
	return (digits);
}

static void		compute_datas(t_printf_datas *datas, intmax_t number)
{
	const int		len = count_digits(number, 10);
	int				dec;

	dec = len;
	if (datas->flags & PRECISION)
	{
		if (len < datas->precision || number == 0)
		{
			dec = datas->precision;
			if (number != 0)
				datas->precision -= len;
		}
		datas->flags &= ~(ZERO_PADD);
	}
	if (number < 0 || datas->flags & SPACE_PADD || datas->flags & SIGN_PADD)
		dec++;
	datas->field_width = datas->field_width > dec ? datas->field_width - dec \
	: 0;
}

/*
** Format an integer and outputted it.
** ---
** dsc: Compute datas, padding and precision and format integer to display it.
** arg: the datas used to compute format (t_printf_datas)
**		the address of the stream used to output datas (t_stream *)
**		the va_list used to get the number (va_list)
** ret: the number of bytes outputted or a negative value in case of error (int)
** not: Set errno in case of syscall error.
*/

int				process_int(t_printf_datas datas, t_stream *stream, \
va_list args)
{
	const intmax_t	number = get_int(datas, args);

	compute_datas(&datas, number);
	if (!(datas.flags & LEFT_PADD) && !(datas.flags & ZERO_PADD))
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	if (number < 0)
		if (stream_add_byte(stream, '-') < 0)
			return (-1);
	if (number >= 0 && (datas.flags & SIGN_PADD \
	|| datas.flags & SPACE_PADD))
		if (stream_add_byte(stream, datas.flags & SIGN_PADD ? '+' : ' ') < 0)
			return (-1);
	if (datas.flags & ZERO_PADD || datas.flags & PRECISION)
		if (stream_add_nbytes(stream, '0', datas.flags & PRECISION ? \
		datas.precision : datas.field_width) < 0)
			return (-1);
	if (!(datas.flags & PRECISION) \
	|| (datas.flags & PRECISION && number != 0 && datas.precision != 0))
		if (stream_add_unumber(stream, number < 0 ? -number : number, B_10) < 0)
			return (-1);
	if (datas.flags & LEFT_PADD)
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	return (stream->outputted);
}
