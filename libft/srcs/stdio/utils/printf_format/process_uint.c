/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_uint.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/07 11:02:07 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 14:10:25 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "lft_string.h"

#include "bases.h"

static uintmax_t	get_uintmax_from_varray(unsigned pos, va_list args)
{
	uintmax_t	number;
	va_list		copy;

	if (!pos)
		return (va_arg(args, uintmax_t));
	va_copy(copy, args);
	while (pos--)
		number = (va_arg(copy, uintmax_t));
	va_end(copy);
	return (number);
}

static uintmax_t	get_uintmax_value(t_printf_datas datas, va_list args)
{
	const uintmax_t	number = get_uintmax_from_varray(datas.specifier_pos, args);

	if (datas.length_mod == 0)
		return ((unsigned)number);
	else if (datas.length_mod == LOW_LOW)
		return ((unsigned char)number);
	else if (datas.length_mod == LOW)
		return ((unsigned short)number);
	else if (datas.length_mod == HIGH)
		return ((unsigned long)number);
	else if (datas.length_mod == HIGH_HIGH)
		return ((unsigned long long)number);
	else if (datas.length_mod == SIZE_T)
		return ((size_t)number);
	else if (datas.length_mod == PTRDIFF_T)
		return ((ptrdiff_t)number);
	else
		return (number);
}

static size_t		count_digits(uintmax_t number, size_t base)
{
	size_t	digits;

	digits = 1;
	while (number >= base)
	{
		number /= base;
		digits++;
	}
	return (digits);
}

static void			compute_datas(t_printf_datas *datas, uintmax_t number)
{
	const size_t	base_len = ft_strlen(get_base_str(datas->specifier));
	size_t			len;
	size_t			dec;

	len = count_digits(number, base_len);
	dec = len;
	if (datas->flags & PREFIX)
		dec += ft_strlen(get_base_prefix(datas->specifier));
	if (datas->flags & PRECISION)
	{
		if (datas->flags & PREFIX && datas->specifier & OCTAL && number != 0)
			len++;
		if (len < (size_t)datas->precision || number == 0)
		{
			dec = datas->precision;
			if (number != 0)
				datas->precision -= len;
		}
		datas->flags &= ~(ZERO_PADD);
	}
	datas->field_width = (size_t)datas->field_width > dec ? \
	datas->field_width - dec : 0;
}

/*
** Process uint argument and datas, format it and output it to open stream
** ---
** dsc: Compute datas and process them before output the unsigned number in
**		specified base.
** arg: the datas used to process the output (t_printf_datas)
**		the address of the stream used to output the format (t_stream *)
**		the list of the arguments passed to printf, used to get the number
**		(va_list)
** ret: the number of bytes outputted or a negative value in case of error (int)
** not: Set errno in case of system error.
*/

int					process_uint(t_printf_datas datas, t_stream *stream,\
va_list args)
{
	const uintmax_t		number = get_uintmax_value(datas, args);

	compute_datas(&datas, number);
	if (!(datas.flags & LEFT_PADD) && !(datas.flags & ZERO_PADD))
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	if (datas.flags & PREFIX && number != 0)
		if (stream_add_string(stream, get_base_prefix(datas.specifier)) < 0)
			return (-1);
	if (datas.flags & ZERO_PADD || datas.flags & PRECISION)
		if (stream_add_nbytes(stream, '0', datas.flags & PRECISION ? \
		datas.precision : datas.field_width) < 0)
			return (-1);
	if (!(datas.flags & PRECISION) || \
	(datas.flags & PRECISION && number != 0 && datas.precision != 0))
		if (stream_add_unumber(stream, number, \
		get_base_str(datas.specifier)) < 0)
			return (-1);
	if (datas.flags & LEFT_PADD)
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	return (stream->outputted);
}
