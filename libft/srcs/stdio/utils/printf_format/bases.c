/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bases.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/07 11:53:11 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 12:38:39 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "process_fmt.h"
#include "bases.h"

/*
** Map unsigned specifiers to the string that specifie base
** ---
** dsc: Look if number specifier is in a special base (8, 16, 10) ... and return
**		the correct base, well formated.
** arg: the specifier used to determine the number base (t_specifier)
** ret: the base use to encoded the number, base 10 by default (char *)
** not: n/a
*/

char	*get_base_str(t_specifier specifier)
{
	if (specifier == OCTAL)
		return (B_8);
	else if (specifier == HEXA)
		return (B_16_MIN);
	else if (specifier == HEXA_HIGH)
		return (B_16_MAX);
	return (B_10);
}

/*
** Return the prefix appended to encoded base.
** ---
** dsc: Find the prefix appended before number in specified base.
** arg: the specifier used to find the correct prefix (t_specifier)
** ret: the prefix used or an empty string if no prefix (char *)
** not: n/a
*/

char	*get_base_prefix(t_specifier specifier)
{
	if (specifier == OCTAL)
		return (PREFIX_B_8);
	else if (specifier == HEXA)
		return (PREFIX_B_16_MIN);
	else if (specifier == HEXA_HIGH)
		return (PREFIX_B_16_MAX);
	return ("");
}
