/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_str.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/17 00:40:42 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/17 01:19:29 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include "./process_fmt.h"

static char	*get_string(unsigned position, va_list args)
{
	va_list	copy;
	char	*str;

	if (!position)
		return ((str = va_arg(args, char *)) ? str : NULL_STRING);
	va_copy(copy, args);
	while (position--)
		str = va_arg(copy, char *);
	return (str ? str : NULL_STRING);
}

static void	compute_datas(t_printf_datas *datas, const char *str)
{
	const size_t	len = ft_strlen(str);

	if (!(datas->flags & PRECISION) || (size_t)datas->precision > len)
		datas->precision = len;
	datas->field_width = datas->precision > datas->field_width ? 0 \
	: datas->field_width - datas->precision;
	datas->flags &= ~(SPACE_PADD | SIGN_PADD | PREFIX | ZERO_PADD);
}

/*
** Format a string and outputted it.
** ---
** dsc: Format a string, compute field width and precision and outputted it.
** arg: the datas used to compute the format (t_printf_datas)
**		the address of the stream where the format is outputted (t_stream *)
**		the list of arguments used to get the string (va_list)
** ret: the number of bytes outputted or a negative value in case of error (int)
** not: Set errno in case of syscall error.
*/

int			process_str(t_printf_datas datas, t_stream *stream, va_list args)
{
	const char	*str = get_string(datas.specifier_pos, args);

	compute_datas(&datas, str);
	if (!(datas.flags & LEFT_PADD))
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	if (stream_add_nstring(stream, str, datas.precision) < 0)
		return (-1);
	if (datas.flags & LEFT_PADD)
		if (stream_add_nbytes(stream, ' ', datas.field_width) < 0)
			return (-1);
	return (stream->outputted);
}
