/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   process_fmt.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 11:25:07 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 12:37:57 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "./process_fmt.h"

static t_specifier	g_specifiers[HANDLE_LEN] = {
	CHAR,
	STRING,
	INT,
	UNSIGNED,
	OCTAL,
	HEXA,
	HEXA_HIGH
};

static t_processes	g_processes = {
	process_char,
	process_str,
	process_int,
	process_uint,
	process_uint,
	process_uint,
	process_uint,
};

/*
** Process format and display it.
** ---
** dsc: Get all the datas about the format, format it and add it to the stream.
** arg: the format string (const char *)
**		the pointer to the stream's datas (t_stream *)
**		the list of arguments passed to printf (va_list)
** ret: the next character to display in fmt, in case of format error, display
**		the modulo and advance the character by 1. In case of critical error
**		return NULL (char *)
** not: Set errno in case of syscall error.
*/

char	*process_fmt(const char *fmt, t_stream *stream, va_list args)
{
	t_printf_datas		datas;
	char				*nxt_fmt;
	int					ret;
	int					i;

	ret = 0;
	i = 0;
	nxt_fmt = get_fmt_datas(&datas, fmt, args);
	if (!nxt_fmt || datas.specifier == MODULO)
		ret = stream_add_byte(stream, '%');
	else
		while (i < HANDLE_LEN)
		{
			if (datas.specifier == g_specifiers[i])
				ret = g_processes[i](datas, stream, args);
			i++;
		}
	if (ret < 0)
		return (NULL);
	return (nxt_fmt ? nxt_fmt : (char*)fmt);
}
