/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_flush.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 21:34:00 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/02 16:46:06 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <limits.h>
#include "lft_string.h"

#include "./stream.h"

/*
** Outputted all the stream datas to the file descriptor.
** ---
** dsc: Use the filedescriptor to outputted all the remainings datas.
** arg: the address of the stream datas (t_stream *)
** ret: a negative value in case of syscall error, or the number of bytes
**		totally outputed, or INT_MAX if this number int overflow (int)
** not: Set errno in case of syscall error.
*/

int		stream_flush(t_stream *stream)
{
	ssize_t	ret;

	ret = write(stream->fd, stream->buffer, stream->len);
	ft_bzero(stream->buffer, stream->len);
	stream->len = 0;
	if (ret < 0)
	{
		stream->state = ERROR;
		return (-1);
	}
	else
	{
		stream->outputted += ret;
		stream->state = FLUSHED;
	}
	return (stream->outputted > INT_MAX ? INT_MAX : stream->outputted);
}
