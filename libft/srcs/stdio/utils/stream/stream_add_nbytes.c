/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_add_nbytes.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 22:17:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/06 22:35:35 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "./stream.h"

/*
** Add the byte c n times to the stream.
** ---
** dsc: Add the same character c cast in char to the stream n times.
** arg: the address of stream's datas (t_stream *)
**		the character appends, casted in char (int)
**		the number of bytes appends (size_t)
** ret: a negative value in case of error or the total number of bytes outputted
**		or INT_MAX if this number overflow an int (int)
** not: Set errno in case of syscall error.
*/

int		stream_add_nbytes(t_stream *stream, int c, size_t n)
{
	while (n--)
		if (stream_add_byte(stream, c) < 0)
			return (-1);
	return (stream->outputted > INT_MAX ? INT_MAX : stream->outputted);
}
