/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_add_nstring.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 22:29:40 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/07 01:42:35 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "./stream.h"

/*
** Add n bytes of the string.
** ---
** dsc: Add a maximum of n bytes of string to the stream.
** arg: the address of the stream's datas (t_stream *)
**		the string appended to the stream (const char *)
**		the maximum of character to write (size_t)
** ret: a negative value in case of error or the total number of characters
**		outputted or INT_MAX if this number overflow an int (int)
** not: Set errno in case of syscall error.
*/

int		stream_add_nstring(t_stream *stream, const char *str, size_t n)
{
	while (n-- && *str)
		if (stream_add_byte(stream, *str++) < 0)
			return (-1);
	return (stream->outputted > INT_MAX ? INT_MAX : stream->outputted);
}
