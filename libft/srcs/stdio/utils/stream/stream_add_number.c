/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_add_number.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 01:43:32 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/02 16:45:29 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <limits.h>
#include <unistd.h>
#include "lft_string.h"

#include "./stream.h"

/*
** Add a signed number of base base in the stream
** ---
** dsc: Appended the number 'number' written in the specified base to the
**		stream.
** arg: the address of the stream's datas (t_stream *)
**		the signed number, can be of the maximum size authorize on the system
**		(intmax_t)
**		the base used to write the number, can be anything (const char *)
** ret: a negative value in case of error, or the total number of characters
**		writed to fd, or INT_MAX if this number exceed an int (int)
** not: Set errno in case of syscall error.
*/

int		stream_add_number(t_stream *stream, intmax_t number, const char *base)
{
	const uintmax_t	len_base = ft_strlen(base);
	uintmax_t		unumber;
	int				tmp;
	ssize_t			ret;

	ret = 0;
	unumber = number < 0 ? -number : number;
	if (number < 0)
		if ((ret = stream_add_byte(stream, '-')) < 0)
			return (-1);
	if (unumber >= len_base)
	{
		if ((tmp = stream_add_number(stream, unumber / len_base, base)) < 0)
			return (-1);
		ret += tmp;
		if ((tmp = stream_add_number(stream, unumber % len_base, base)) < 0)
			return (-1);
		return (ret + tmp > INT_MAX ? INT_MAX : ret + tmp);
	}
	else
	{
		if ((tmp = stream_add_byte(stream, base[unumber])) < 0)
			return (-1);
		return (ret + tmp > INT_MAX ? INT_MAX : ret + tmp);
	}
}
