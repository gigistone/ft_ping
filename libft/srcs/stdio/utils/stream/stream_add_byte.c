/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_add_byte.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 21:47:22 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/06 21:58:46 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "./stream.h"

/*
** Add one byte to the stream.
** ---
** dsc: Add one byte to the stream, flush the stream if necessary.
** arg: the pointer to the stream datas (t_stream *)
**		the character to add, converted to char * (int)
** ret: negative in case of error, the number of total bytes outputted, or
**		INT_MAX if this number overflow int (int)
** not: Set errno in case of syscall error.
*/

int		stream_add_byte(t_stream *stream, int c)
{
	stream->buffer[stream->len++] = (char)c;
	if (stream->len == BUFFER_SIZE)
		return (stream_flush(stream));
	stream->state = USED;
	return (stream->outputted > INT_MAX ? INT_MAX : stream->outputted);
}
