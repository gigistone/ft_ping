/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 21:28:32 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/02 16:46:57 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./stream.h"
#include "lft_string.h"

/*
** Init the values of the stream structure.
** ---
** dsc: Use the stream pointer and the filedesc for init the stream struc.
** arg: the stream pointer to the structure (t_stream *)
**		the file descriptor used to write datas (int)
** ret: n/a
** not: n/a
*/

void	stream_init(t_stream *stream, int fd)
{
	ft_bzero(stream->buffer, BUFFER_SIZE);
	stream->fd = fd;
	stream->len = 0;
	stream->outputted = 0;
	stream->state = USED;
}
