/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 21:18:04 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/01 22:17:59 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STREAM_H

# define STREAM_H

# include "../buffer.h"
# include <stddef.h>
# include <stdint.h>

typedef enum	e_stream_state
{
	USED,
	FLUSHED,
	ERROR,
}				t_stream_state;

typedef struct	s_stream
{
	char			buffer[BUFFER_SIZE];
	int				fd;
	size_t			outputted;
	size_t			len;
	t_stream_state	state;
}				t_stream;

void			stream_init(t_stream *stream, int fd);
int				stream_add_byte(t_stream *stream, int c);
int				stream_add_nbytes(t_stream *stream, int c, size_t n);
int				stream_add_string(t_stream *stream, const char *str);
int				stream_add_nstring(t_stream *stream, const char *str,\
size_t bytes);
int				stream_add_number(t_stream *stream, intmax_t number, \
const char *base);
int				stream_add_unumber(t_stream *stream, uintmax_t number, \
const char *base);
int				stream_flush(t_stream *stream);

/*
** int				stream_add_wide_string(t_stream *stream, wchar_t *w_str);
*/

#endif
