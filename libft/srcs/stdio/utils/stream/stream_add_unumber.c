/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_add_unumber.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 01:59:41 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/06 12:02:51 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include <limits.h>
#include "lft_string.h"

#include "stream.h"

/*
** Add a unsigned number of base base in the stream
** ---
** dsc: Appended the number 'number' written in the specified base to the
**		stream.
** arg: the address of the stream's datas (t_stream *)
**		the unsigned number, can be of the maximum size authorize on the system
**		(uintmax_t)
**		the base used to write the number, can be anything (const char *)
** ret: a negative value in case of error, or the total number of characters
**		writed to fd, or INT_MAX if this number exceed an int (int)
** not: Set errno in case of syscall error.
*/

int		stream_add_unumber(t_stream *stream, uintmax_t number, \
const char *base)
{
	const size_t	len_base = ft_strlen(base);
	int				tmp;
	size_t			ret;

	ret = 0;
	if (number >= len_base)
	{
		if ((tmp = stream_add_number(stream, number / len_base, base)) < 0)
			return (-1);
		ret += tmp;
		if ((tmp = stream_add_number(stream, number % len_base, base)) < 0)
			return (-1);
		return (ret + tmp > INT_MAX ? INT_MAX : ret + tmp);
	}
	else
	{
		if ((tmp = stream_add_byte(stream, base[number])) < 0)
			return (-1);
		return (ret + tmp > INT_MAX ? INT_MAX : ret + tmp);
	}
}
