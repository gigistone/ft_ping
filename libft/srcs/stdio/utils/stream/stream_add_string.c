/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stream_add_string.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 21:54:48 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/06 22:01:05 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>

#include "./stream.h"

/*
** Add a string to the stream.
** ---
** dsc: Add a string to the stream.
** arg: the pointer to the stream's datas (t_stream *)
**		the string appended (const char *)
** ret: negative value in case of error, or the total number of bytes outputted
**		or INT_MAX if this number overflow an int (int)
** not: Set errno in case of syscall error.
*/

int	stream_add_string(t_stream *stream, const char *str)
{
	while (*str)
		if (stream_add_byte(stream, *str++) < 0)
			return (-1);
	return (stream->outputted > INT_MAX ? INT_MAX : stream->outputted);
}
