/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fds_new.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/21 15:42:19 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/01 22:19:11 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_stdlib.h"
#include "./gnl_fds.h"

struct s_fds	*fds_new(int fd)
{
	struct s_fds	*new;

	new = (struct s_fds*)ft_calloc(1, sizeof(struct s_fds));
	if (!new)
		return (NULL);
	new->fd = fd;
	return (new);
}
