/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   gnl_fds.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 04:52:19 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/01 22:19:09 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GNL_FDS_H

# define GNL_FDS_H

# include "../buffer.h"

struct			s_fds
{
	int		fd;
	char	buffer[BUFFER_SIZE + 1];
};

struct s_fds	*fds_new(int fd);

#endif
