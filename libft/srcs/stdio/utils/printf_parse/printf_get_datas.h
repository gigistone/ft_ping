/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printf_get_datas.h                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 10:47:26 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/06 12:07:50 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTF_GET_DATAS_H

# define PRINTF_GET_DATAS_H

# include "../stream/stream.h"
# include <stdint.h>
# include <stdarg.h>

# define LEN_FLAGS	5
# define FLAGS		"#0- +"
# define LEN_MOD_L	8
# define MOD_L		"hlLjzt"
# define SPEC_LEN	17
# define SPECIFIER	"diouxXeEfgGcspn%"

typedef enum	e_flag
{
	PREFIX = (1u << 0),
	ZERO_PADD = (1u << 1),
	LEFT_PADD = (1u << 2),
	SPACE_PADD = (1u << 3),
	SIGN_PADD = (1u << 4),
	PRECISION = (1u << 5),
}				t_flag;

typedef enum	e_length_mod
{
	LOW_LOW = 1,
	LOW,
	HIGH,
	HIGH_HIGH,
	LONG_DOUBLE,
	INTMAX,
	SIZE_T,
	PTRDIFF_T,
}				t_length_mod;

typedef struct	s_length_token
{
	t_length_mod	token;
	char			*src;
	size_t			len;
}				t_length_token;

typedef enum	e_specifier
{
	NONE,
	INT,
	UNSIGNED,
	OCTAL,
	HEXA,
	HEXA_HIGH,
	EXPOSANT,
	EXPOSANT_HIGH,
	FLOAT,
	DOUBLE_G,
	DOUBLE_G_HIGH,
	CHAR,
	STRING,
	POINTER,
	LEN,
	MODULO,
}				t_specifier;

/*
** This structure contains all datas about format.
** The flags contains infos about flags actived.
** The field_width contains unsigned value of it.
** The precision contains the value of it. A bit is set in flags if precision
** is set but to 0.
** The specifier contains info about the specifier.
** Same for length modifier.
*/

typedef struct	s_printf_datas
{
	uint8_t			flags;
	int				field_width;
	int				precision;
	t_length_mod	length_mod;
	t_specifier		specifier;
	int				specifier_pos;
}				t_printf_datas;

uintmax_t		get_uintmax(const char *str);
char			*get_varray_arg_pos(int *nbr, const char *fmt);
char			*get_numeric_arg(int *nbr, const char *fmt, va_list args);
char			*get_specifier_type(t_printf_datas *datas, const char *fmt);
char			*get_fmt_datas(t_printf_datas *datas, const char *fmt, \
va_list args);

#endif
