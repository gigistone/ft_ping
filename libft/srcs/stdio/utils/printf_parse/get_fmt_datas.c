/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_fmt_datas.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 11:51:02 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/07 14:34:29 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "lft_string.h"

#include "./printf_get_datas.h"

static const t_length_token	g_tokens[LEN_MOD_L] = {
	{LOW_LOW, "hh", 2},
	{HIGH_HIGH, "ll", 2},
	{LOW, "h", 1},
	{HIGH, "l", 1},
	{LONG_DOUBLE, "L", 1},
	{INTMAX, "j", 1},
	{SIZE_T, "z", 1},
	{PTRDIFF_T, "t", 1}
};

static char	*get_flags(t_printf_datas *datas, const char *fmt)
{
	const uint8_t	bitflags[LEN_FLAGS] = {PREFIX, ZERO_PADD, LEFT_PADD, \
	SPACE_PADD, SIGN_PADD};
	int				index;

	datas->flags = 0;
	while ((index = ft_strindex(FLAGS, *fmt)) >= 0)
	{
		datas->flags |= bitflags[index];
		fmt++;
	}
	if (datas->flags & LEFT_PADD && datas->flags & ZERO_PADD)
		datas->flags &= ~ZERO_PADD;
	if (datas->flags & SIGN_PADD && datas->flags & SPACE_PADD)
		datas->flags &= ~SPACE_PADD;
	return ((char*)fmt);
}

static char	*get_field_width(t_printf_datas *datas, const char *fmt, \
va_list args)
{
	datas->field_width = 0;
	fmt = get_numeric_arg(&datas->field_width, fmt, args);
	if (datas->field_width < 0)
	{
		datas->flags |= LEFT_PADD;
		if (datas->flags & ZERO_PADD)
			datas->flags &= ~(ZERO_PADD);
	}
	return ((char*)fmt);
}

static char	*get_precision(t_printf_datas *datas, const char *fmt, \
va_list args)
{
	datas->precision = 0;
	if (*fmt != '.')
		return ((char*)fmt);
	datas->flags |= PRECISION;
	fmt = get_numeric_arg(&datas->precision, fmt + 1, args);
	if (datas->precision < 0)
		datas->precision = 0;
	return ((char*)fmt);
}

static char	*get_length_modifier(t_printf_datas *datas, const char *fmt)
{
	int						i;

	i = 0;
	datas->length_mod = 0;
	if (ft_strindex(MOD_L, *fmt) < 0)
		return ((char*)fmt);
	while (i < LEN_MOD_L)
	{
		if (!ft_strncmp(g_tokens[i].src, fmt, g_tokens[i].len))
		{
			datas->length_mod = g_tokens[i].token;
			return ((char*)fmt + g_tokens[i].len);
		}
		i++;
	}
	return ((char*)fmt);
}

/*
** Get all infos about the format specification.
** ---
** dsc: Parcour all the format string and get the infos, flags, field_width,
**		precision, length_modifier and specifier. Compute all infos to determine
**		the size of the padding etc.
** arg: the pointer to printf infos's datas (t_printf_datas *)
**		the format string (const char *)
**		the argument's list (va_list)
** ret: the address of the next argument after parsing or NULL if format error
**		(char*)
** not: n/a
*/

char		*get_fmt_datas(t_printf_datas *datas, const char *fmt, va_list args)
{
	char		*iter;

	(void)args;
	iter = (char*)fmt;
	iter = get_varray_arg_pos(&datas->specifier_pos, iter);
	iter = get_flags(datas, iter);
	iter = get_field_width(datas, iter, args);
	iter = get_precision(datas, iter, args);
	iter = get_length_modifier(datas, iter);
	return (get_specifier_type(datas, iter));
}
