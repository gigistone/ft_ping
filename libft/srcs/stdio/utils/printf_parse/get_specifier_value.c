/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_specifier_value.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/08/02 16:48:44 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/02 17:06:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include "./printf_get_datas.h"

/*
** Just get the specifier type and check if the format string is good.
** ---
** dsc: Check the specifier type and fill it into printf datas structure, if no
**		specifier is found return NULL to indicate an format string error.
** arg: the pointer to the printf's datas (t_printf_datas *)
**		the format string parsed and used (const char *)
** ret: the address of the next character after specifier in fmt or NULL if no
**		specifier found (char *)
** not: n/a
*/

char	*get_specifier_type(t_printf_datas *datas, const char *fmt)
{
	const t_specifier	specifiers[SPEC_LEN] = {
		INT,
		INT,
		OCTAL,
		UNSIGNED,
		HEXA,
		HEXA_HIGH,
		EXPOSANT,
		EXPOSANT_HIGH,
		FLOAT,
		DOUBLE_G,
		DOUBLE_G_HIGH,
		CHAR,
		STRING,
		POINTER,
		LEN,
		MODULO
	};
	int					index;

	index = ft_strindex(SPECIFIER, *fmt);
	if (index < 0)
		return (NULL);
	datas->specifier = specifiers[index];
	return ((char*)fmt + 1);
}
