/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_padding_arg.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/10 18:35:24 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/16 13:58:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_ctype.h"
#include "./printf_get_datas.h"

static char	*skip_digits(const char *str)
{
	while (ft_isdigit(*str))
		str++;
	return ((char*)str);
}

/*
** Get the numeric arg pos in va_list args if specified
** ---
** dsc: Check if a numeric pos is specified in the format string and get it.
** arg: the format string (const char *)
**		a pointer to the printf parsed datas (t_printf_datas *)
**		a pointer to a copy of the arguments passed to printf (va_list *)
** ret: the address of the next argument of format string or NULL in case of
**		format error (char *)
** not: n/a
*/

char		*get_varray_arg_pos(int *nbr, const char *fmt)
{
	char	*iter;

	*nbr = get_uintmax(fmt);
	if (*(iter = skip_digits(fmt)) == '$')
		return (iter + 1);
	*nbr = 0;
	return ((char*)fmt);
}

static char	*get_field_width_from_varray(int *nbr, \
const char *fmt, va_list args)
{
	va_list	copy;
	int		position;

	fmt = get_varray_arg_pos(&position, fmt);
	va_copy(copy, args);
	if (!position)
		*nbr = va_arg(args, int);
	while (position--)
		*nbr = va_arg(copy, int);
	va_end(copy);
	return ((char*)fmt);
}

/*
** Get numeric arg of the format string
** ---
** dsc: Get the numeric arg of the fmt string, can be used to get field width
**		or precision.
** arg: the address of the printf datas (t_printf_datas *)
**		the format string used (const char *)
**		the va_list of the arguments passed to printf (va_list)
** ret: the next characters of fmt
** not: n/a
*/

char		*get_numeric_arg(int *nbr, const char *fmt, \
va_list args)
{
	*nbr = 0;
	if (*fmt == '*')
		return (get_field_width_from_varray(nbr, fmt + 1, args));
	*nbr = get_uintmax(fmt);
	if (*fmt == '-')
		fmt++;
	return (skip_digits(fmt));
}
