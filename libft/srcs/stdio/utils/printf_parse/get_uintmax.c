/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_uintmax.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/10 18:25:19 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/02 17:41:01 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdint.h>
#include "lft_ctype.h"

/*
** Just convert string to uintmax
** ---
** dsc: Read a string composed with only decimal numbers and return
**		an uintmax.
** arg: the string used to get the number (const char *)
** ret: return the number finded or 0 if no number (uintmax_t)
** not: n/a
*/

uintmax_t	get_uintmax(const char *str)
{
	uintmax_t	nbr;

	nbr = 0;
	while (ft_isdigit(*str))
		nbr = nbr * 10 + *str++ - '0';
	return (nbr);
}
