/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 13:30:52 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/01 22:25:20 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

#include <stdlib.h>
#include "lft_stdlib.h"
#include "lft_common.h"
#include "lft_string.h"

#include "./utils/gnl/gnl_fds.h"

/*
** Create new fd location in vector or return it's address.
** Return NULL if an error occurs.
*/

static char	*get_fd(t_vector **fds, int fd)
{
	struct s_fds	*current;
	int				i;

	i = 0;
	if (!*fds)
		if (!(*fds = ft_vector_new(10)))
			return (NULL);
	while ((current = ft_vector_get(*fds, i++)))
		if (current->fd == fd)
			return (current->buffer);
	current = fds_new(fd);
	if (!current)
		return (NULL);
	if (ft_vector_push_back(*fds, current) < 0)
	{
		free(current);
		return (NULL);
	}
	return (current->buffer);
}

/*
** Automatically free memory if needed.
** Return ret.
*/

static int	free_fd(t_vector **fds, int fd, int ret)
{
	int				i;
	struct s_fds	*current;

	i = 0;
	if (ret == 0)
	{
		while ((current = ft_vector_get(*fds, i)))
		{
			if (current->fd == fd)
				free(ft_vector_erase(*fds, i));
			i++;
		}
		if (ft_vector_len(*fds) == 0)
			ft_vector_clear(fds, free);
	}
	return (ret);
}

/*
** Move any characters after a newline of the buffer, at front.
*/

static void	move_buffer(char *buffer)
{
	char	*begin;

	begin = buffer;
	while (*buffer && *buffer != '\n')
		buffer++;
	while (*buffer)
		*begin++ = *(++buffer);
	*begin = '\0';
}

/*
** Append buffer to line until find newline character.
** ---
** dsc: Read and append buffer to line until find null terminating byte
**		or newline character.
**		Move the characters after newline at the beginning of the buffer, then
**		return the correct value.
** arg: the address of the line where we append characters (char **)
**		the address of the buffer that contains the bytes to append (char *)
** ret: one if it read a line, zero if not, minus one in case of error.
*/

static int	append_line(char **line, char *buffer)
{
	char	*tmp;
	int		ret;

	ret = ft_strchr(buffer, '\n') ? 1 : 0;
	if (!*line)
		*line = ft_strcdup(buffer, "\n");
	else
	{
		tmp = ft_strcjoin(*line, buffer, "\n");
		ft_strdel(line);
		*line = tmp;
	}
	if (!*line)
		return (-1);
	move_buffer(buffer);
	return (ret);
}

/*
** Read a line from the file descriptor.
** ---
** dsc: Read the next line from the file descriptor, allocate it, and assign it
**		to line.
** arg: the file descriptor used to read (int)
**		the line address of the pointer wich get the line (char **)
** ret: one if a line has been read, zero if EOF has been reach, negative number
**		in case of error.
** not: errno is set in case of error, check it.
*/

int			get_next_line(int fd, char **line)
{
	static t_vector	*fds;
	char			*read_buffer;
	int				ret;
	int				bytes;

	if (!(read_buffer = get_fd(&fds, fd)))
	{
		ft_vector_clear(&fds, free);
		return (-1);
	}
	*line = NULL;
	ret = append_line(line, read_buffer);
	while (ret == 0 && (bytes = read(fd, read_buffer, BUFFER_SIZE)) > 0)
	{
		read_buffer[bytes] = '\0';
		ret = append_line(line, read_buffer);
	}
	if (ret < 0 || bytes < 0)
	{
		ft_vector_clear(&fds, free);
		if (line)
			ft_strdel(line);
		return (-1);
	}
	return (free_fd(&fds, fd, ret));
}
