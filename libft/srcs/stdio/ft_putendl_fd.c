/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:37:08 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:40:48 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "lft_string.h"

/*
** Write a string, followed by a newline to file descriptor.
** ---
** dsc: Write a string, followed by a newline to file descriptor.
** arg: the string to write (const char *)
**		the fd where we write (int)
** ret: n/a
** not: Do not handle write errors...
*/

void	ft_putendl_fd(const char *str, int fd)
{
	write(fd, str, ft_strlen(str));
	write(fd, "\n", sizeof(char));
}
