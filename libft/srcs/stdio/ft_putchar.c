/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 19:53:28 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 20:12:14 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/*
** Output character to stdout.
** ---
** dsc: Write character to stdout filedesc.
** arg: the character to write (char)
** ret: n/a
** not: Do not handle write errors...
*/

void	ft_putchar(char c)
{
	write(STDOUT_FILENO, &c, sizeof(c));
}
