/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/06 20:10:29 by lperson-          #+#    #+#             */
/*   Updated: 2020/08/02 17:39:21 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "./utils/printf_format/process_fmt.h"

/*
** Output format string on file descriptor.
** ---
** dsc: Use printf standard format flags into format string to output formated
**		datas, for more info man printf. The datas are outputted on file
**		descriptor.
** arg: the string used to format data, beware of user input to prevent
**		malicious code (const char *)
**		the file descriptor used to write the datas (int)
**		all datas insert in format string, can be of any data type (va_list *)
** ret: the size of the format string outputted or negative value in case of
**		syscall error (int)
** not: Set errno in case of syscall error. Be aware that user can't insert
**		format string to prevent malicious code. Be aware that you used
**		correctly the number of arguments with the format string
*/

int	ft_printf_fd(const char *fmt, int fd, ...)
{
	va_list		arguments;
	t_stream	stream;

	stream_init(&stream, fd);
	va_start(arguments, fd);
	while (*fmt)
	{
		if (*fmt == '%')
		{
			fmt = process_fmt(fmt + 1, &stream, arguments);
			if (!fmt)
			{
				va_end(arguments);
				return (-1);
			}
		}
		else
			stream_add_byte(&stream, *fmt++);
	}
	va_end(arguments);
	if (stream_flush(&stream) < 0)
		return (-1);
	return (stream.outputted);
}
