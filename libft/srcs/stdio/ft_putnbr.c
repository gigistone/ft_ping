/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 20:28:13 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 20:34:27 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_stdio.h"

/*
** Output a number to stdout.
** ---
** dsc: Write a number to stdout filedesc.
** arg: the number to write (int)
** ret: n/a
** not: Do not handle write errors...
*/

void	ft_putnbr(int nbr)
{
	unsigned	nb;

	nb = (nbr < 0) ? -nbr : nbr;
	if (nbr < 0)
		ft_putchar('-');
	if (nb >= 10)
	{
		ft_putnbr(nb / 10);
		ft_putnbr(nb % 10);
	}
	else
		ft_putchar(nb + '0');
}
