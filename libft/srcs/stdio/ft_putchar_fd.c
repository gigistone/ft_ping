/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putchar_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:27:42 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:31:21 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

/*
** Write character to filedesc.
** ---
** dsc: Write character to filedesc.
** arg: the character to write (char)
**		the file descriptor where the character is writed (int)
** ret: n/a
** not: Do not handle write errors...
*/

void	ft_putchar_fd(char c, int fd)
{
	write(fd, &c, sizeof(c));
}
