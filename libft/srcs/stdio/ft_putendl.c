/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 20:10:53 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 20:15:10 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "lft_string.h"

/*
** Output string to stdout with a newline.
** ---
** dsc: Write string to stdout with a trail newline character.
** arg: the string to display (const char *)
** ret: n/a
** not: Do not handle write errors...
*/

void	ft_putendl(const char *str)
{
	write(STDOUT_FILENO, str, ft_strlen(str));
	write(STDOUT_FILENO, "\n", sizeof(char));
}
