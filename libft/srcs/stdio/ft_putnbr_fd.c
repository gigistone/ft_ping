/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:44:12 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:46:33 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_stdio.h"

/*
** Write a number to the file descriptor.
** ---
** dsc: Write a number to the file descriptor.
** arg: the number to write (int)
**		the file descriptor where we write (int)
** ret: n/a
** not: Do not handle write errors...
*/

void	ft_putnbr_fd(int nbr, int fd)
{
	unsigned	nb;

	nb = (nbr < 0) ? -nbr : nbr;
	if (nbr < 0)
		ft_putchar_fd('-', fd);
	if (nb >= 10)
	{
		ft_putnbr_fd(nb / 10, fd);
		ft_putnbr_fd(nb % 10, fd);
	}
	else
		ft_putchar_fd(nb + '0', fd);
}
