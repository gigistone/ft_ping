/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putstr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 03:32:06 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/23 03:35:21 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "lft_string.h"

/*
** Write a string to the file descriptor.
** ---
** dsc: Write the string to the file descriptor.
** arg: the string to write (const char *)
**		the file descriptor where we write (int)
** ret: n/a
** not: Do not handle write errors...
*/

void	ft_putstr_fd(const char *str, int fd)
{
	write(fd, str, ft_strlen(str));
}
