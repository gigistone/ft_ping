/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lmin.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/21 23:33:47 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/21 23:35:43 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Return the minimum number between n1 and n2.
** ---
** dsc: Return the minimum number between n1 and n2.
** arg: the first integer (long)
**		the second integer (long)
** ret: the minimum integer between thoses two (long)
** not: n/a
*/

long	ft_lmin(long n1, long n2)
{
	return (n1 < n2 ? n1 : n2);
}
