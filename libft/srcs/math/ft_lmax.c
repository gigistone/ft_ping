/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lmax.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/21 23:39:54 by lperson-          #+#    #+#             */
/*   Updated: 2020/09/21 23:41:16 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Return the maximum integer between thoses two numbers.
** ---
** dsc: Return the maximum integer between thoses two numbers.
** arg: the first integer (long)
**		the second integer (long)
** ret: the maximum integer between thoses two (long)
** not: n/a
*/

long	ft_lmax(long n1, long n2)
{
	return (n1 > n2 ? n1 : n2);
}
