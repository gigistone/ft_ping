/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_join.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/22 13:17:25 by lperson-          #+#    #+#             */
/*   Updated: 2020/10/22 13:25:48 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_common.h"

/*
** Allocate the concatenation of two strings arrays.
** ---
** dsc: Join two strings arrays and return it in a new allocated array.
** arg: the first string array joined (char **)
**		the second string array joined (char **)
** ret: the string array resulting of the concatenation or NULL in case of error
**		(char **)
** not: Set errno to ENOMEM if ran out of memory
*/

char	**ft_str_array_join(char *strs0[], char *strs1[])
{
	const size_t	len = ft_str_array_len(strs0) + ft_str_array_len(strs1);
	char			**strs_join;

	strs_join = (char**)malloc(sizeof(char *) * (len + 1));
	if (!strs_join)
		return (NULL);
	while (*strs0)
		*strs_join++ = *strs0++;
	while (*strs1)
		*strs_join++ = *strs1++;
	*strs_join = NULL;
	return (strs_join - len);
}
