/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_del.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:47:15 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 06:11:43 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Free each element in strs and strs and set strs to NULL.
** ---
** dsc: Read above.
** arg: the array of strings to free (char **)
** ret: n/a
** not: n/a.
*/

void	ft_str_array_del(char **strs[])
{
	int		i;

	i = 0;
	while ((*strs)[i])
		free((*strs)[i++]);
	free(*strs);
	*strs = NULL;
}
