/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_len.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:11:14 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/21 11:26:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Compute the len of an array of strings.
** ---
** dsc: Compute the len of an array of strings null terminate.
** arg: the array of string (const char *[])
** ret: the number of elements in array (size_t)
** not: n/a.
*/

size_t	ft_str_array_len(char *strs[])
{
	register char	**iter;

	iter = strs;
	while (*iter)
		iter++;
	return (iter - strs);
}
