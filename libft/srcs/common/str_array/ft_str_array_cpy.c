/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_cpy.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:25:31 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 06:02:34 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Copy the content of src into dst.
** ---
** dsc: Copy each pointer in src into dst. Do not allocate anything. Be aware
**		if you free ptr in src.
** arg: the destination array of strings (char **)
**		the source array of strings (char **)
** ret: the address of destination array of strings (char **)
** not: n/a.
*/

char	**ft_str_array_cpy(char *dst[], char *src[])
{
	register char	**iter;

	iter = dst;
	while (*src)
		*iter++ = *src++;
	*iter = NULL;
	return (dst);
}
