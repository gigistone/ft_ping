/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_append.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/10/21 16:21:09 by lperson-          #+#    #+#             */
/*   Updated: 2020/10/22 14:26:19 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_common.h"

/*
** Append a string to the string array.
** --
** dsc: Reallocate the string arrays to have a correct size. Then append the
**		new string at the end.
** arg: the address of the string array (char ***)
**		the new string appended (const char *)
** ret: Non zero in case of error (int)
** not: Set errno to ENOMEM if ran out of memory.
*/

int	ft_str_array_append(char **strs[], const char *append)
{
	const size_t	len = *strs ? ft_str_array_len(*strs) + 1 : 1;
	char			**new;

	new = (char**)malloc(sizeof(char*) * (len + 1));
	if (!new)
		return (-1);
	if (*strs)
		ft_str_array_cpy(new, *strs);
	new[len - 1] = (char*)append;
	new[len] = NULL;
	free(*strs);
	*strs = new;
	return (0);
}
