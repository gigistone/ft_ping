/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_array_dup.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 05:40:54 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 06:17:10 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_stdlib.h"
#include "lft_string.h"
#include "lft_common.h"

/*
** Duplicate an array of string and each strings in it.
** ---
** dsc: You know to read.
** arg: the source array of strings to duplicate (char **)
** ret: the freshly allocate array of strings or NULL in case of malloc error
**		(char **)
** not: Set errno to ENOMEM if ran out of memory.
*/

char	**ft_str_array_dup(char *strs[])
{
	const size_t	len = ft_str_array_len(strs);
	char			**dup;
	int				i;

	dup = ft_calloc(len + 1, sizeof(char*));
	i = 0;
	if (!dup)
		return (NULL);
	while (*strs)
	{
		dup[i] = ft_strdup(*strs++);
		if (!dup[i])
		{
			ft_str_array_del(&dup);
			return (NULL);
		}
		i++;
	}
	return (dup);
}
