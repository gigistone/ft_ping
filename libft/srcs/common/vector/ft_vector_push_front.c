/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_push_front.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 02:00:23 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:39:39 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./vector_utils.h"

/*
** Append new element at the begin of the vector.
** ---
** dsc: Append the element at the beginning of the vector, pushing other
**		elements away and handle capacity and len.
** arg: the vector that is modified (t_vector *)
**		the element pushed at begin (void *)
** ret: negative value in case of malloc error, positive value otherwise (int)
** not: Set errno to ENOMEM if ran out of memory.
*/

int	ft_vector_push_front(t_vector *vector, void *elem)
{
	void	*prec_elem;
	int		i;

	i = 0;
	if (vector_realloc(vector, 1) < 0)
		return (-1);
	while ((prec_elem = vector->data[i]))
	{
		vector->data[i++] = elem;
		elem = prec_elem;
	}
	vector->data[vector->len++] = elem;
	return (1);
}
