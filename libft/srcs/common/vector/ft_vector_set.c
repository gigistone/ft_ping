/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_set.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 21:41:12 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:51:04 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_common.h"

/*
** Set the old element specified at index i to the new element.
** ---
** dsc: Access to the old element with index i, if i is negative start from the
**		end, set the new element at the position i and return the old element.
** arg: the address of the vector used to update the element (t_vector *)
**		the new element inserted at position i (void *)
**		the index used to find the position in the vector, can be negative (int)
** ret: the old element that has been replace or null if index is out of range
**		(void *)
** not: n/a
*/

void	*ft_vector_set(t_vector *vector, void *elem, int i)
{
	const size_t	index = i < 0 ? vector->len + i : (size_t)i;
	void			*old_elem;

	if ((signed)(vector->len + 1) < 0 || index > vector->len)
		return (NULL);
	old_elem = vector->data[index];
	vector->data[index] = elem;
	return (old_elem);
}
