/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_data.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 21:29:28 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:31:29 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_common.h"

/*
** Return the address of the datas stocked into the vector.
** ---
** dsc: Read above.
** arg: the address of the vector used to get the datas (t_vector *)
** ret: the address of the datas container (void **)
** not: n/a
*/

void	**ft_vector_data(t_vector *vector)
{
	return (vector->data);
}
