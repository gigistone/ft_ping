/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_realloc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 15:50:07 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 17:12:02 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "lft_string.h"
#include "lft_stdlib.h"
#include "lft_common.h"

/*
** This function try to resize the vector.
** If the reallocation fails, return -1 but don't touch the vector, thereof
** it can be free by the user in case of failure and the datas are not loss.
*/

int	vector_realloc(t_vector *vector, int is_append)
{
	size_t			size_to_cpy;
	size_t			capacity;
	void			*tmp;

	size_to_cpy = vector->len;
	if (is_append && vector->len >= vector->capacity)
		capacity = vector->capacity ? vector->capacity * 2 : 1;
	else if (!is_append && vector->len * 2 < vector->capacity)
		capacity = vector->capacity > 1 ? vector->capacity / 2 : 0;
	else
		return (1);
	tmp = ft_calloc(capacity, sizeof(void *));
	if (!tmp)
		return (-1);
	ft_memcpy(tmp, vector->data, size_to_cpy * sizeof(void *));
	free(vector->data);
	vector->data = tmp;
	vector->len = size_to_cpy;
	vector->capacity = capacity;
	return (1);
}
