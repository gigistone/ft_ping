/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_clear.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 20:12:33 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:41:33 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "lft_stdlib.h"
#include "lft_common.h"

/*
** Free the vector allocated and his data with freef. Set vector ptr to null.
** ---
** dsc: Free each vector element with freef if freef is not null, free the
**		the vector and set it to null.
** arg: the address of the pointer to the allocated vector (t_vector **)
**		the address of the function used to free the elements
**		(void (*freef)(void *))
** ret: n/a
** not: n/a
*/

void	ft_vector_clear(t_vector **vector, void (*freef)(void *))
{
	int		i;
	void	*elem;

	i = 0;
	if (freef)
		while ((elem = ft_vector_get(*vector, i++)) != NULL)
			freef(elem);
	free((*vector)->data);
	free(*vector);
	*vector = NULL;
}
