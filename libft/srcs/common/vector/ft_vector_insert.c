/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_insert.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 03:51:56 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 17:30:14 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./vector_utils.h"

/*
** Insert an element at the specified index.
** ---
** dsc: Insert an element at the specified index, the index can be negative,
**		in such case we start at the end. Push the element that were in place
**		of one case.
** arg: the address of the vector used to store the element (t_vector *)
**		the address of the element inserted into the vector (void *)
**		the index used to insert the element, can be negative (int)
** ret: negative value in case of malloc error, otherwise positive value (int)
** not: Set errno to ENOMEM if ran out of memory.
*/

int	ft_vector_insert(t_vector *vector, void *elem, int i)
{
	size_t			index;
	void			*prec_element;

	index = i < 0 ? vector->len + i : (size_t)i;
	if (index > vector->len)
	{
		if (i < 0)
			return (ft_vector_push_front(vector, elem));
		else
			return (ft_vector_push_back(vector, elem));
	}
	if (vector_realloc(vector, 1) < 0)
		return (-1);
	while ((prec_element = vector->data[index]))
	{
		vector->data[index++] = elem;
		elem = prec_element;
	}
	vector->data[vector->len++] = elem;
	return (1);
}
