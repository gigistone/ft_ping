/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_pop_back.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 17:22:24 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 17:34:13 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./vector_utils.h"

/*
** Poped the last element of the vector.
** ---
** dsc: Erase the last element of the vector and return it.
**		If vector has no element to pop or a malloc error occurs, return null.
** arg: the address of the vector used to pop the element (t_vector *)
** ret: the address of the element poped or null in case of error (void *)
** not: Set errno to ENOMEM if ran out of memory.
*/

void	*ft_vector_pop_back(t_vector *vector)
{
	void	*poped;

	if (!vector->len || vector_realloc(vector, 0) < 0)
		return (NULL);
	poped = vector->data[vector->len - 1];
	vector->data[--vector->len] = NULL;
	return (poped);
}
