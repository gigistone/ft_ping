/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_get.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 20:33:16 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:15:52 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_common.h"

/*
** Access element at the specified index.
** ---
** dsc: Use index i to access element in vector. If i is negative, start at the
**		end of vector.
** arg: the vector where we get the element (t_vector *)
**		the index used to get the element (int)
** ret: the address of the element or NULL if index out of range (void *)
** not: n/a.
*/

void	*ft_vector_get(t_vector *vector, int i)
{
	const size_t	index = i < 0 ? vector->len + i : (size_t)i;

	if ((signed)(vector->len + i) < 0 || index > vector->len)
		return (NULL);
	return (vector->data[index]);
}
