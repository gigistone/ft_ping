/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_from.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 20:43:48 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:12:18 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "lft_stdlib.h"
#include "lft_string.h"
#include "lft_common.h"

/*
** Copy each elements in data and create a new vector from arguments.
** ---
** dsc: Create a vector of specified capacity and len and copy each elements
**		in data into it.
** arg: the address of the elements to copy (void **)
**		the len of the new vector and the number of elements to copy (size_t)
**		the capacity of the new vector (size_t)
** ret: the newly allocated vector, or null in case of malloc error (t_vector *)
** not: Set errno to ENOMEM if ran out of memory. If the capacity is inferior
**		len set automatically capacity to len * 2.
*/

t_vector	*ft_vector_from(void **data, size_t len, size_t capacity)
{
	t_vector	*new;

	new = (t_vector*)ft_calloc(1, sizeof(t_vector));
	if (!new)
		return (NULL);
	if (capacity <= len)
		capacity = len * 2;
	new->data = (void**)ft_calloc(capacity, sizeof(t_vector));
	if (!new->data)
	{
		free(new);
		return (NULL);
	}
	new->capacity = capacity;
	new->len = len;
	ft_memcpy(new->data, data, sizeof(void *) * len);
	return (new);
}
