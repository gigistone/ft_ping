/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_len.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 21:22:44 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:24:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_common.h"

/*
** Return the len of the vector.
** ---
** dsc: Read above.
** arg: the address of the vector used to compute the len (t_vector *)
** ret: the len of the vector, aka the number of it's elements (size_t)
** not: n/a.
*/

size_t	ft_vector_len(t_vector *vector)
{
	return (vector->len);
}
