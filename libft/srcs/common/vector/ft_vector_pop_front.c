/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_pop_front.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 17:00:56 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 17:22:54 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./vector_utils.h"

/*
** Pop the first element of vector.
** ---
** dsc: Return and erase from the vector, it's first element.
**		Return null and set errno if malloc error. Return null if no elements
**		in vector.
** arg: the address of the vector used to pop the element (t_vector *)
** ret: the address of the element poped or null in case of error (void *)
** not: Set errno if ran out of memory.
*/

void	*ft_vector_pop_front(t_vector *vector)
{
	void	*elem;
	void	*prec_elem;
	size_t	index;

	if (!vector->len || vector_realloc(vector, 0) < 0)
		return (NULL);
	elem = NULL;
	index = vector->len--;
	while (index--)
	{
		prec_elem = vector->data[index];
		vector->data[index] = elem;
		elem = prec_elem;
	}
	return (elem);
}
