/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_capacity.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 21:26:30 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 21:27:50 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_common.h"

/*
** Return the memory capacity of the vector.
** ---
** dsc: read above.
** arg: the address of the vector used to get his capacity (t_vector *)
** ret: the memory capacity of the vector (size_t)
** not: n/a.
*/

size_t	ft_vector_capacity(t_vector *vector)
{
	return (vector->capacity);
}
