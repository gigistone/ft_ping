/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector_utils.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 15:50:18 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 17:12:17 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VECTOR_UTILS_H

# define VECTOR_UTILS_H

# include "lft_common.h"

int		vector_realloc(t_vector *vector, int is_append);

#endif
