/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_erase.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 17:53:35 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 18:06:35 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./vector_utils.h"

/*
** Erase and return the element at index i.
** ---
** dsc: Erase and return the element at index i. If i is negative, start from
**		the end of the vector. Erase the last element if i is out of range and
**		positive, or the first element if i is out of range and negative.
**		If there is no data to erase in vector, or there is a malloc
**		error return null.
** arg: the address of the vector where the data is erased (t_vector *)
**		the index used to find the data, can be negative (int)
** ret: the address of the element erased or null in case of error (void *)
** not: Set errno to ENOMEM if ran out of memory.
*/

void	*ft_vector_erase(t_vector *vector, int i)
{
	const size_t	erase_index = i < 0 ? vector->len + i : (size_t)i;
	size_t			index;
	void			*prec_elem;
	void			*elem;

	if (erase_index > vector->len)
	{
		if (i < 0)
			return (ft_vector_pop_front(vector));
		else
			return (ft_vector_pop_back(vector));
	}
	if (!vector->len || vector_realloc(vector, 0) < 0)
		return (NULL);
	index = vector->len--;
	elem = NULL;
	while (index-- > erase_index)
	{
		prec_elem = vector->data[index];
		vector->data[index] = elem;
		elem = prec_elem;
	}
	return (elem);
}
