/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_new.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/03 20:06:49 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/03 20:54:44 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

#include "lft_stdlib.h"
#include "lft_common.h"

/*
** Allocate a new vector of size capacity.
** ---
** dsc: Allocate a new vector and his data to capacity.
**		Set his capacity value to capacity.
** arg: the initial memory capacity for the vector (size_t)
** ret: the newly allocate vector, NULL in case of malloc error (t_vector *)
** not: Set errno to ENOMEM if ran out of memory.
*/

t_vector	*ft_vector_new(size_t capacity)
{
	t_vector	*new;

	new = (t_vector*)ft_calloc(1, sizeof(t_vector));
	if (!new)
		return (NULL);
	new->data = (void **)ft_calloc(capacity, sizeof(void *));
	if (!new->data)
	{
		free(new);
		return (NULL);
	}
	new->capacity = capacity;
	return (new);
}
