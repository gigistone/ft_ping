/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vector_push_back.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/04/04 02:47:51 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/04 17:22:59 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./vector_utils.h"

/*
** Append the element at the end of the vector
** ---
** dsc: Append the element at the end of the vector, updating the len and the
**		capacity if necessary.
** arg: the vector used to store the elements (t_vector *)
**		the element to append (void *)
** ret: negative value in case of malloc error, positive value otherwise (int)
** not: Set errno to ENOMEM if ran out of memory.
*/

int	ft_vector_push_back(t_vector *vector, void *elem)
{
	if (vector_realloc(vector, 1) < 0)
		return (-1);
	vector->data[vector->len++] = elem;
	return (1);
}
