/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 17:45:15 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 17:51:23 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Search in reverse order for character c in string str.
** ---
** dsc: Search in reverse order for character c in string str. Stop it when
**		found the last occurence.
** arg: the string used by search (const char *)
**		the character we're searching for (int c)
** ret: Return the last address of c in string or null if not find it.
** not: Be aware of overflow c to above 255.
**		If c is the null terminating byte, return his adress in string.
*/

char	*ft_strrchr(const char *str, int c)
{
	register const char	constant_byte = c;
	register char		*s;
	char				*last_address;

	last_address = NULL;
	s = (char*)str;
	while (*s)
	{
		if (*s == constant_byte)
			last_address = s;
		s++;
	}
	if (*s == constant_byte)
		last_address = s;
	return (last_address);
}
