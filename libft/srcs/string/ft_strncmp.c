/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 21:27:12 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 21:32:25 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Compare string one to the second string.
** ---
** dsc: Compare each chars of string one to string two, semantically. Stop when
**		one string reach end, len reach zero or if characters are inequals.
**		Return the difference between characters.
** arg: the first string to compare (const char *)
**		the second string that is compared (const char *)
**		the max len of characters compared (size_t)
** ret: zero if string are equals, > 0 if string one is greater, < 0 is string
**		two one is lesser.
** not: n/a.
*/

int	ft_strncmp(const char *str1, const char *str2, size_t len)
{
	while (len && *str1 && *str1 == *str2)
	{
		len--;
		str1++;
		str2++;
	}
	if (!len)
		return (0);
	return (*str1 - *str2);
}
