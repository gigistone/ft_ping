/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 11:33:03 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 17:23:18 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

/*
** Append src characters at end of dst no more than dstsize - len(dst) - 1.
** ---
** dsc: Append src bytes at end of dst no more than dstsize - len(dst) - 1
**		If len of src > dstsize, truncate src and append a null byte at end.
**		If len of dst > dstsize, do nothing.
** arg: the destination string where we append the characters (char *)
**		the source string used to copied his characters (const char *)
**		the total size of the destination buffer
** ret: return the len of the string it try to create (size_t)
**		if len dst >= dstsize it equals to len dst + len src
**		else it equals to dstsize + len src
** not: Be aware to have enough space in dstsize !
*/

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	const char		*sav_dst = dst;
	const size_t	len_src = ft_strlen(src);
	size_t			len_dst;

	while (dstsize && *dst)
	{
		dst++;
		dstsize--;
	}
	len_dst = dst - sav_dst;
	if (!dstsize)
		return (len_dst + len_src);
	while (--dstsize && *src)
		*dst++ = *src++;
	while (dstsize--)
		*dst++ = '\0';
	*dst = '\0';
	return (len_dst + len_src);
}
