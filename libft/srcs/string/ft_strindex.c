/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strindex.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/07/07 12:10:55 by lperson-          #+#    #+#             */
/*   Updated: 2020/07/07 12:13:53 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Search for the index of character c in the string.
** ---
** dsc: Parcour the string until find the character c or the end, and return
**		the index of it, or a negative value if not found.
** arg: the string used to search (const char *)
**		the character searched in string, converted in char (int)
** ret: the index of the character or a negative value if not found (int)
** not: n/a
*/

int		ft_strindex(const char *str, int c)
{
	int			i;

	i = 0;
	while (str[i])
	{
		if (str[i] == c)
			return (i);
		i++;
	}
	return (-1);
}
