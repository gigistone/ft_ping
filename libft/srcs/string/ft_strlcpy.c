/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 10:55:09 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 11:38:47 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

/*
** Copy up to dstsize - 1 bytes from src into dst.
** ---
** dsc: Copy up to dstsize - 1 bytes from src into dst.
**		Guarantee to NULL terminate the string if dstsize is not zero.
**		Be aware of src truncation if len src > dstsize - 1.
** arg: the string destination of copy (char *)
**		the src string of copy (const char *)
**		the buffer size of dest (size_t)
** ret: the len of src (use it to compare with dstsize to check truncation)
**		(size_t)
** not: Be aware of src truncation and that dstsize is sufficient.
*/

size_t	ft_strlcpy(char *dst, const char *src, size_t dstsize)
{
	const size_t	len_src = ft_strlen(src);

	if (!dstsize--)
		return (len_src);
	while (dstsize && *src)
	{
		dstsize--;
		*dst++ = *src++;
	}
	while (dstsize--)
		*dst++ = '\0';
	*dst = '\0';
	return (len_src);
}
