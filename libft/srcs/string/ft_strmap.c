/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 14:12:46 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 14:52:17 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Iterate and generate a fresh new string.
** ---
** dsc: Allocate a new string generated from the iteration of str.
**		Apply the function f to each character of str to the new string.
** arg: the string that we iterate (const char *)
**		the function that we apply to each characters (int (*f)(int))
** ret: the fresh new string or null in case of malloc error (char *)
** not: Set ENOMEM is ran out of memory.
*/

char	*ft_strmap(const char *str, int (*f)(int))
{
	const size_t	len = ft_strlen(str);
	register char	*dup;

	dup = (char*)malloc(sizeof(char) * (len + 1));
	if (!dup)
		return (NULL);
	while (*str)
		*dup++ = f(*str++);
	*dup = '\0';
	return (dup - len);
}
