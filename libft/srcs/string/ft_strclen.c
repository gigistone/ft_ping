/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 17:43:58 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 17:47:42 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

/*
** Count characters.
** ---
** dsc: Count characters of the string until reach his end or find one of
**		characters specified in set.
** arg: the string used to count (const char *)
**		the characters set used to stop (const char *)
** ret: the total compute length (size_t)
** not: n/a
*/

size_t	ft_strclen(const char *str, const char *set)
{
	register const char	*iter = str;

	while (*iter && !ft_strchr(set, *iter))
		iter++;
	return (iter - str);
}
