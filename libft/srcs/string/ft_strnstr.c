/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 21:00:39 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:41:46 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Search for the substring little in big in the first characters of big.
** ---
** dsc: Search for the substring little in big in the first characters of big.
**		If little is an empty string, return big.
** arg: the big string where we search for little (const char *)
**		the little string that we search for in big (const char *)
**		the number of characters max to scan (size_t)
** ret: a ptr to the address of the first match in big, big itself if little is
**		empty, or null otherwise. (char *)
** not: n/a
*/

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	register const char	*haystack = big;
	register const char	*needle = little;
	size_t				n;

	if (!*needle)
		return ((char*)haystack);
	while (*haystack && len--)
	{
		if (*haystack == *needle)
		{
			n = len;
			big = haystack;
			little = needle;
			while (n-- && *big == *little)
			{
				big++;
				little++;
				if (!*little)
					return ((char*)haystack);
			}
		}
		haystack++;
	}
	return (NULL);
}
