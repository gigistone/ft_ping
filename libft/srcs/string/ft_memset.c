/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 01:43:33 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 01:55:53 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Set the first bytes of dst with c cast in unsigned char.
** ---
** dsc: As i said, it set the first bytes of dst with the constant byte c.
** arg: the area set by bytes (void *)
**		the constant bytes used to set dst, it is casted to unsigned char (int)
**		the number of bytes to set in dst (size_t)
** ret: The address of dst (void *)
** not: Be aware of overflow with c, it must not overflow the value 255.
*/

void	*ft_memset(void *dst, int c, size_t bytes)
{
	register const unsigned char	constant_byte = c;
	register unsigned char			*memdst;

	memdst = dst;
	while (bytes--)
		*memdst++ = constant_byte;
	return (dst);
}
