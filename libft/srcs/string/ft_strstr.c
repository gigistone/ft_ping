/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 15:42:49 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 15:55:27 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Locate a substring.
** ---
** dsc: Find the first occurence of little in big.
** arg: the string where we're searching (const char *)
**		the string we're looking for (const char *)
** ret: the pointer of the located substring, or NULL if the substring is not
**		found. If the substring is empty, return big (char *)
** not: n/a
*/

char	*ft_strstr(const char *big, const char *little)
{
	register const char	*haystack = big;
	register const char *needle = little;

	if (!*little)
		return ((char*)haystack);
	while (*haystack)
	{
		if (*haystack == *needle)
		{
			big = haystack;
			little = needle;
			while (*big == *little)
			{
				big++;
				little++;
				if (!*little)
					return ((char*)haystack);
			}
		}
		haystack++;
	}
	return (NULL);
}
