/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 16:51:53 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 17:10:58 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Trim the string with the character set specified.
** ---
** dsc: Delete each characters of characters set in begin and end of the string.
**		Allocate an return the new string.
** arg: the string to trim (const char *)
**		the set of characters (const char *)
** ret: the new trim string, freshly allocated, or null in case of malloc error
**		(char *)
** not: Set ENOMEM if ran out of memory.
*/

char	*ft_strtrim(const char *str, const char *set)
{
	register char	*trim;
	size_t			len;

	while (*str && ft_strchr(set, *str))
		str++;
	len = ft_strlen(str);
	while (str[len - 1] && ft_strchr(set, str[len - 1]))
		len--;
	trim = (char*)malloc(sizeof(char) * (len + 1));
	if (!trim)
		return (NULL);
	trim[len] = '\0';
	while (len--)
		trim[len] = str[len];
	return (trim);
}
