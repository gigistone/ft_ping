/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 18:47:43 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 18:49:48 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Free the string and set it to NULL.
** ---
** dsc: Free the string and set it to NULL.
** arg: the address of the pointer to the string (char **)
** ret: n/a
** not: The string must have been allocated.
*/

void	ft_strdel(char **address_str)
{
	free(*address_str);
	*address_str = NULL;
}
