/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 17:31:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 17:38:18 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Search for character c in string str.
** ---
** dsc: Search for character c in string str, stop it when found it or reach the
**		end of the string.
** arg: the string where we're looking for c (const char *)
**		the character that we're looking for convert as char (int c)
** ret: the address of c in string or NULL if not found it (char *)
** not: Be aware of overflow above 255 in c.
**		If c is the null terminating byte, return it's address from string.
*/

char	*ft_strchr(const char *str, int c)
{
	register const char	constant_byte = c;

	while (*str && *str != constant_byte)
		str++;
	return (*str == constant_byte ? (char*)str : NULL);
}
