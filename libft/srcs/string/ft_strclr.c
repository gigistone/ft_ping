/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strclr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 13:25:17 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:44:24 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Fill string with null terminating bytes.
** ---
** dsc: Iter to each characters of the string and replace it with the null
**		nerminating byte character.
** arg: the string to nullify (char *)
** ret: n/a
** not: n/a
*/

void	ft_strclr(char *str)
{
	while (*str)
		*str++ = '\0';
}
