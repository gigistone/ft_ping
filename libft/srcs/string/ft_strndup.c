/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strndup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 02:40:25 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 02:51:00 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include <stdlib.h>

/*
** Duplicate a string.
** ---
** dsc: Duplicate at most maxlen characters of string.
** arg: the string that is duplicated (const char *)
**		the maximum characters to duplicate (size_t)
** ret: a pointer to newly duplicated string or null in case of error (char *)
** not: Set ENOMEM if ran out of memory.
*/

char	*ft_strndup(const char *str, size_t maxlen)
{
	register char	*dup;
	const size_t	len = ft_strnlen(str, maxlen);

	dup = (char*)malloc(sizeof(char) * (len + 1));
	if (!dup)
		return (NULL);
	maxlen = len;
	while (maxlen-- && *str)
		*dup++ = *str++;
	*dup = '\0';
	return (dup - len);
}
