/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 16:25:59 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:43:56 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Copy a string.
** ---
** dsc: Copy src into dst at most maxlen.
** arg: the destination string (char *)
**		the source string (char *)
**		the most character to write from src to dst (char *)
** ret: the address of destination string (char *)
** not: Be aware of buffer overflow. Maxlen must be > to len of src, because
**		it wil not append the null terminating byte if it's not found in src.
*/

char	*ft_strncpy(char *dst, const char *src, size_t maxlen)
{
	register char	*iter;

	iter = dst;
	while (maxlen && *src)
	{
		*iter++ = *src++;
		maxlen--;
	}
	while (maxlen--)
		*iter++ = '\0';
	return (dst);
}
