/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 02:07:42 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 02:15:07 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Try to duplicate string into a new allocated address.
** ---
** dsc: Try to duplicate string into a new allocated address.
** arg: the string to duplicate (const char *)
** ret: return a ptr to the new allocated address, this must be free.
**		or null in case of out of memory (char *)
** not: Set ENOMEM in case of out of memory.
*/

char	*ft_strdup(const char *str)
{
	const size_t	len = ft_strlen(str);
	register char	*dup;

	dup = (char*)malloc(sizeof(char) * (len + 1));
	if (!dup)
		return (NULL);
	while (*str)
		*dup++ = *str++;
	*dup = '\0';
	return (dup - len);
}
