/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 03:23:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 03:29:04 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Compare two strings
** ---
** dsc: The strcmp function compares the two strings s1 and s2 lexically.
** arg: the first string to compare (const char *)
**		the string that is compared (const char *)
** ret: zero if string are equals, > 0 if string one is greater, < 0 is string
**		two one is lesser.
** not: n/a
*/

int	ft_strcmp(const char *str1, const char *str2)
{
	while (*str1 && *str1 == *str2)
	{
		str1++;
		str2++;
	}
	return (*str1 - *str2);
}
