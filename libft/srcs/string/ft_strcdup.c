/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcdup.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 17:54:05 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:43:35 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Duplicate a string.
** ---
** dsc: Duplicate the string until reach one of set's characters.
** arg: the string to duplicate (const char *)
**		the set's characters used as delimiters (const char *)
** ret: the freshly new allocated string or null in case of malloc error
**		(char *)
** not: Set ENOMEM if ran out of memory.
*/

char	*ft_strcdup(const char *str, const char *set)
{
	register char	*dup;
	size_t			len;

	len = ft_strclen(str, set);
	dup = (char*)malloc(sizeof(char) * (len + 1));
	if (!dup)
		return (NULL);
	while (*str && !ft_strchr(set, *str))
		*dup++ = *str++;
	*dup = '\0';
	return (dup - len);
}
