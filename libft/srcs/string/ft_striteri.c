/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 14:01:13 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:44:32 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Iterate through a string by indexing its characters.
** dsc: Iterate through each character of the string, applying the function f
**		by passing the character and his index.
** arg: the string that we iterate (char *)
**		the function that we apply to each character
**		(int (*f)(unsigned int, int))
** ret: n/a
** not: n/a
*/

void	ft_striteri(char *str, int (*f)(unsigned int, int))
{
	unsigned int	i;

	i = 0;
	while (str[i])
	{
		str[i] = f(i, str[i]);
		i++;
	}
}
