/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 10:20:12 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 10:26:27 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Compare the first bytes of each memarea and return the difference.
** ---
** dsc: Scan the first bytes of each area, return > 0 if a byte differ or < 0.
**		return 0 in othe case.
** arg: the memarea compare from (const void *)
**		the memarea compare to (const void *)
**		the number of bytes to scan (size_t)
** ret: Return < 0 if byte in mem1 is lesser than mem2, > 0 if byte in mem1 is
**		greater that mem2, 0 otherwise.
** not: n/a.
*/

int		ft_memcmp(const void *mem1, const void *mem2, size_t bytes)
{
	register const unsigned char	*memfrom = mem1;
	register const unsigned char	*memto = mem2;

	while (bytes && *memfrom == *memto)
	{
		bytes--;
		memfrom++;
		memto++;
	}
	if (!bytes)
		return (0);
	return (*memfrom - *memto);
}
