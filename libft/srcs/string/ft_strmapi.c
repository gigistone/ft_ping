/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 14:32:35 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:42:11 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Iterate index by index and generate a fresh new one.
** ---
** dsc: Iterate each characters of the string, passing their index, and apply
**		f to them, generating a new string with this results.
** arg: the string that we iterate through (const char *)
**		the function that we apply to each characters, passing his index
**		(int (*f)(unsigned int, int))
** ret: the new fresh string or null in case of malloc error (char *)
** not: Set ENOMEM if ran out of memory.
*/

char	*ft_strmapi(const char *str, int (*f)(unsigned int, int))
{
	const size_t		len = ft_strlen(str);
	register char		*dup;
	register unsigned	i;

	i = 0;
	dup = (char*)malloc(sizeof(char) * (len + 1));
	if (!dup)
		return (NULL);
	while (str[i])
	{
		*dup++ = f(i, str[i]);
		i++;
	}
	*dup = '\0';
	return (dup - len);
}
