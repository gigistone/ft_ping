/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/17 23:07:32 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:45:15 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Copy n bytes of src into dst.
** ---
** dsc: Copy the first bytes of src into dst.
** par: the address of dst (void *)
**		the address of src used to cpy (const void *)
**		the number of bytes to copy (size_t)
** ret: The address of dst (void *)
** not: The memory areas must not overlap.
*/

void	*ft_memcpy(void *dst, const void *src, size_t bytes)
{
	register unsigned char	*memdst;
	register unsigned char	*memsrc;

	memdst = dst;
	memsrc = (unsigned char *)src;
	while (bytes--)
		*memdst++ = *memsrc++;
	return (dst);
}
