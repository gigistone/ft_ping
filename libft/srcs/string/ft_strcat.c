/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 17:34:04 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 17:37:32 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Concatenate two strings.
** ---
** dsc: The strcat function concatenate dst with src into dst.
** arg: the destination string (char *)
**		the source string (char *)
** ret: the address of destination string (char *)
** not: The areas may not overlap. Be aware of buffer overruns !!!
*/

char	*ft_strcat(char *dst, const char *src)
{
	register char	*iter;

	iter = dst;
	while (*iter)
		iter++;
	while (*src)
		*iter++ = *src++;
	*iter = '\0';
	return (dst);
}
