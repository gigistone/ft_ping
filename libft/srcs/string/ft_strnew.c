/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 18:38:26 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:41:54 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

/*
** Allocate a new string.
** ---
** dsc: Allocate a new string of size + 1 characters (for terminating null byte)
** char: the number of characters or the size to allocate (size_t)
** ret: the new address allocated of the string, or null in case of error
**		(char *)
** not: Set ENOMEM if ran out of memory.
*/

char	*ft_strnew(size_t size)
{
	register char	*new;
	register size_t	sav_size;

	sav_size = size;
	new = (char*)malloc(size + 1);
	if (!new)
		return (NULL);
	while (sav_size--)
		*new++ = '\0';
	*new = '\0';
	return (new - size);
}
