/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 16:04:12 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 16:10:04 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Copy a string.
** ---
** dsc: cpy dst into src, stopping when found null terminating byte.
** arg: the destination string (char *)
**		the src string (const char *)
** ret: the address of dst (char *)
** not: Be aware of buffer overflow, you have to have enough space in dst before
**		cpy. Othewise you may be expose to criticall security issues.
*/

char	*ft_strcpy(char *dst, const char *src)
{
	register char	*iter;

	iter = dst;
	while (*src)
		*iter++ = *src++;
	*iter = '\0';
	return (dst);
}
