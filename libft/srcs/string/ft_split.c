/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 17:26:04 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:44:43 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Count the substrings delimitated by set in str.
*/

static size_t	count_words(const char *str, const char *set)
{
	size_t	len;
	char	in_word;

	len = 0;
	in_word = 0;
	while (*str)
	{
		if (!ft_strchr(set, *str++))
		{
			if (!in_word)
				len++;
			in_word = 1;
		}
		else
			in_word = 0;
	}
	return (len);
}

/*
** Free each elements of split and split itself.
*/

static void		free_split(char **split)
{
	int		i;

	i = 0;
	while (split[i])
		free(split[i++]);
	free(split);
}

/*
** Split a string.
** ---
** dsc: Split a string into a two dimmensionnal arrays of strings, ended by
**		null, using the set of characters as a delimiter.
** arg: the string to split (const char *)
**		the characters set use as delimiter (const char *)
** ret: A two dimmensionnal allocated array of string, terminate by null or
**		null in case of malloc error (char **)
** not: Set ENOMEN if ran out of memory.
*/

char			**ft_split(const char *str, const char *set)
{
	register char	**split;
	size_t			words;

	words = count_words(str, set);
	split = (char**)malloc(sizeof(char *) * (words + 1));
	if (!split)
		return (NULL);
	while (*str)
	{
		if (ft_strchr(set, *str))
			str++;
		else
		{
			*split = ft_strcdup(str, set);
			if (!*split)
			{
				free_split(split);
				return (NULL);
			}
			str += ft_strlen(*split++);
		}
	}
	*split = NULL;
	return (split - words);
}
