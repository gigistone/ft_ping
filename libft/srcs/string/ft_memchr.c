/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 09:54:44 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 10:07:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Search for the constant byte c in the first bytes of src.
** ---
** dsc: Search for c in the first bytes of src, return a ptr to c in src
**		if found it or NULL otherwise.
** arg: the memory area used to search byte (const void *)
**		the constant byte c to find in src, cast as unsigned char (int c)
**		the number of bytes used to search (size_t)
** ret: A pointer to c in src or NULL otherwise (void *)
** not: Be aware of overflowing c above 255.
*/

void	*ft_memchr(const void *src, int c, size_t bytes)
{
	register const unsigned char	*memsrc = src;
	register const unsigned char	constant_byte = c;

	while (bytes--)
	{
		if (*memsrc == constant_byte)
			return ((void*)memsrc);
		memsrc++;
	}
	return (NULL);
}
