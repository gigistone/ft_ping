/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/23 21:00:10 by lperson-          #+#    #+#             */
/*   Updated: 2020/04/20 21:44:16 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Join the two strings until find a set's character.
** ---
** dsc: Duplicate str1 and str2 joined untile find a set's character.
**		If a set's character is find in str1 stop and do not duplicate, the
**		others characters, including str2.
** arg: the first string to join (const char *)
**		the second string to join (const char *)
**		the set that defined unwanted characters (const char *)
** ret: the freshly new allocated string, or null in case of malloc error
**		(char *)
** not: Set ENOMEM if ran out of memory.
*/

char	*ft_strcjoin(const char *str1, const char *str2, const char *set)
{
	register char	*join;
	size_t			len;
	int				is_truncate;

	len = ft_strclen(str1, set);
	is_truncate = str1[len];
	if (!is_truncate)
		len += ft_strclen(str2, set);
	join = (char*)malloc(sizeof(char) * (len + 1));
	if (!join)
		return (NULL);
	while (*str1 && !ft_strchr(set, *str1))
		*join++ = *str1++;
	if (!is_truncate)
		while (*str2 && !ft_strchr(set, *str2))
			*join++ = *str2++;
	*join = '\0';
	return (join - len);
}
