/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 18:17:12 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 18:22:59 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Concatenate two strings.
** ---
** dsc: strncat concatenate at most maxlen characters of src into dst.
**		Appending a null terminating byte the end of dst.
** arg: the destination string (char *)
**		the source string (const char *)
**		the max len to concatenate (size_t)
** ret: the address of dest (char *)
** not: Be aware of buffer overrun if no enough place in dst !!!
**		String may not overlaps.
*/

char	*ft_strncat(char *dst, const char *src, size_t maxlen)
{
	register char	*iter;

	iter = dst;
	while (*iter)
		iter++;
	while (maxlen-- && *src)
		*iter++ = *src++;
	*iter = '\0';
	return (dst);
}
