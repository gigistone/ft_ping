/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnlen.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 02:30:33 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 02:36:57 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Determine the length of a fixed-size string.
** ---
** dsc: This function return the number of characters in the string, excluding
**		the terminating null byte but at most maxlen.
** arg: the string used to count characters (const char *)
**		the max characters to count (size_t)
** ret: the number of characters at most maxlen (size_t)
** not: n/a
*/

size_t	ft_strnlen(const char *str, size_t maxlen)
{
	register const char	*iter = str;

	while (maxlen-- && *iter)
		iter++;
	return (iter - str);
}
