/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 16:27:21 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 16:32:27 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include <stdlib.h>

/*
** Join two strings
** ---
** dsc: Allocate a new string that is the concatenation of str1 with str2.
** arg: the first string to concate (const char *)
**		the second string to concate (const char *)
** ret: the new allocate string or null in case of malloc error (char *)
** not: Set ENOMEM if ran out of memory.
*/

char	*ft_strjoin(const char *str1, const char *str2)
{
	register char	*join;
	size_t			len;

	len = ft_strlen(str1) + ft_strlen(str2);
	join = (char*)malloc(sizeof(char) * (len + 1));
	if (!join)
		return (NULL);
	while (*str1)
		*join++ = *str1++;
	while (*str2)
		*join++ = *str2++;
	*join = '\0';
	return (join - len);
}
