/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 15:20:29 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 15:23:37 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Compare the two strings.
** ---
** dsc: Compare each characters of the two strings.
** arg: the first string to compare (const char *)
**		the second string compared (const char *)
** ret: return nonzero if they are equals, zero otherwise (int)
** not: n/a
*/

int	ft_strequ(const char *str1, const char *str2)
{
	while (*str1 && *str1 == *str2)
	{
		str1++;
		str2++;
	}
	if (*str1 == *str2)
		return (1);
	return (0);
}
