/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 02:44:17 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 03:00:37 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Cpy the first bytes of src into dst, stop when cpy c in dst.
** ---
** dsc: Cpy the first bytes of src into dst, stopping when find c in src.
**		Cpy it and return the next address in dst.
**		If not found just cpy and return null.
** arg: the area to destination bytes (void *)
**		the area of srcs bytes (const void *)
**		the constant byte c to search in src (int)
**		the number of bytes to cpy (size_t)
** ret: the next address after c in dst, if find it, NULL otherwise
** not: Be aware of c overflow (it is cast to unsigned char).
*/

void	*ft_memccpy(void *dst, const void *src, int c, size_t bytes)
{
	register unsigned char			*memdst;
	register const unsigned char	*memsrc;
	register const unsigned char	constant_byte = c;

	memdst = dst;
	memsrc = src;
	while (bytes--)
	{
		*memdst = *memsrc++;
		if (*memdst++ == constant_byte)
			return (memdst);
	}
	return (NULL);
}
