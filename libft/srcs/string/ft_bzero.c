/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 02:09:03 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 02:12:43 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Erase the first bytes of data dst with putting zero in place.
** ---
** dsc: Read above.
** arg: the area to erase data (void *)
**		the number of bytes to erase (size_t)
** ret: n/a
** not: Deprecated, prefer to use memset(dst, 0, bytes).
*/

void	ft_bzero(void *dst, size_t bytes)
{
	register unsigned char	*memdst;

	memdst = dst;
	while (bytes--)
		*memdst++ = 0;
}
