/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 13:47:36 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 13:53:44 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Iterate through a string.
** ---
** dsc: Apply the function f to each characters of string, then update it.
** arg: the string that we iterate (char *)
**		the function apply to each characters (int (f*)(int))
** ret: n/a
** not: n/a
*/

void	ft_striter(char *str, int (*f)(int))
{
	while (*str)
	{
		*str = f(*str);
		str++;
	}
}
