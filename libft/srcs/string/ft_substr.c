/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 15:50:14 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 16:09:32 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

/*
** Extract a substring in str.
** ---
** dsc: Extract a substring from the string, starting at the index specified
**		and of maxlen specified.
** arg: the string where the substring is extract (const char *)
**		the starting index of the substract (unsigned int)
**		the max len of the substracted string (size_t)
** ret: the substring freshly allocated, or null in case of malloc error, or
**		an empty string if start out of range.
** not: Set ENOMEN if ran out of memory.
*/

char	*ft_substr(const char *str, unsigned int start, size_t maxlen)
{
	register char	*substring;
	size_t			len;

	len = ft_strlen(str) >= start ? ft_strnlen(str + start, maxlen) : 0;
	substring = (char*)malloc(sizeof(char) * (len + 1));
	if (!substring)
		return (NULL);
	substring[len] = '\0';
	while (len--)
		substring[len] = str[start + len];
	return (substring);
}
