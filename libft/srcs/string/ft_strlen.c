/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/17 14:28:44 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 11:14:07 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Count characters in string.
** ---
** dsc: Count the number of characters in 8-bits string. Stop when found '\0'.
** par: the string (const char *)
** ret: The number of characters as size_t (size_t)
** not: n/a
*/

size_t	ft_strlen(const char *s)
{
	register const char	*iter = s;

	while (*iter)
		iter++;
	return (iter - s);
}
