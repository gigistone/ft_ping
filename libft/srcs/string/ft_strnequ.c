/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/22 15:25:21 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/22 15:37:40 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Compare the first characters of the two strings.
** ---
** dsc: Compare each characters of the two strings until maxlen reach zero.
** arg: the first string to compare (const char *)
**		the second string compared (const char *)
**		the maximum characters compared (size_t)
** ret: nonzero if the first characters are equals, zero otherwise (int)
** not: n/a
*/

int	ft_strnequ(const char *str1, const char *str2, size_t maxlen)
{
	while (maxlen && *str1 && *str1 == *str2)
	{
		maxlen--;
		str1++;
		str2++;
	}
	if (!maxlen || *str1 == *str2)
		return (1);
	return (0);
}
