/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 09:25:09 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 09:46:11 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>

/*
** Move no more than bytes, bytes in srcs to dst. Handle overlap areas.
** ---
** dsc: Move the specified numbers of src to dst. Handle overlap areas.
** arg: the destination memory area that received the bytes (void *)
**		the src memory area from the bytes are copied (const void *)
**		the number of bytes to move / copied (size_t)
** ret: the address of destination memory area (void *)
** not: In case that you didn't read, that handle overlap areas...
*/

void	*ft_memmove(void *dst, const void *src, size_t bytes)
{
	register unsigned char			*memdst;
	register const unsigned char	*memsrc = src;

	memdst = dst;
	if (src < dst)
		while (bytes--)
			memdst[bytes] = memsrc[bytes];
	else
		while (bytes--)
			*memdst++ = *memsrc++;
	return (dst);
}
