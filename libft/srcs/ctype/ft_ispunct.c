/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ispunct.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 15:00:14 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 15:03:15 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_ctype.h"

/*
** Check for any printable character that is not space or alphanumeric.
** ---
** dsc: Check all printables characters that is not space or alphanumeric.
** arg: the character that is checked (int)
** ret: nonzero if true, zero otherwise (int)
** not: n/a
*/

int	ft_ispunct(int c)
{
	return (ft_isprint(c) && c != ' ' && !ft_isalnum(c));
}
