/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalpha.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 23:33:13 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 23:39:52 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if character c is alphabetic.
** ---
** dst: Check if character c is in between a -> z or A -> Z.
** arg: the character that is checked (int)
** ret: nonzero if true, zeor otherwise (int)
** not: n/a
*/

int	ft_isalpha(int c)
{
	return ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'));
}
