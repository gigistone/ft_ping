/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isprint.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 13:07:32 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 13:11:37 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if the character is printable or not.
** ---
** dsc: Check if the character is between space code (32) and tilde (126)
** arg: the character that is checked (int)
** ret: nonzero if true, zero otherwise
** not: n/a
*/

int	ft_isprint(int c)
{
	return (c >= ' ' && c <= '~');
}
