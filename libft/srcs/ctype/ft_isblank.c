/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isblank.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 13:21:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 13:25:50 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if the character is blank or not.
** ---
** dsc: check if the character is a space or a tab.
** arg: the character that is checked (int)
** ret: nonzero if true, zero otherwise (int)
** not: n/a
*/

int	ft_isblank(int c)
{
	return (c == ' ' || c == '\t');
}
