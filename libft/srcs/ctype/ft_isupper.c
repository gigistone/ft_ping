/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 14:49:16 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 14:50:41 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if the character is an alphabetic character in uppercase.
** ---
** dsc: Check if the character is between 'A' and 'Z'.
** arg: the character that is checked (int)
** ret: nonzero if true, zero otherwise (int)
** not: n/a
*/

int	ft_isupper(int c)
{
	return (c >= 'A' && c <= 'Z');
}
