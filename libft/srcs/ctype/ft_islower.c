/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_islower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 14:43:41 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 14:45:36 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if the character is an alphabetic lowercase character.
** ---
** dsc: Check if it's between 'a' and 'z'.
** arg: The character that is checked (int)
** ret: nonzero if true, zero otherwise (int)
** not: n/a
*/

int	ft_islower(int c)
{
	return (c >= 'a' && c <= 'z');
}
