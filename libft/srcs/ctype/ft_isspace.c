/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isspace.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 22:20:07 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 22:23:43 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if c is a whitespace.
** ---
** dsc: Whitespaces characters are spaces, newline, form-feed, carriage-return
**		horizontal tab and vertical tab.
** arg: the character that is checked (int)
** ret: nonzero if it's whitespaces or zero if not (int)
*/

int	ft_isspace(int c)
{
	return (c == ' ' || (c >= '\t' && c <= '\r'));
}
