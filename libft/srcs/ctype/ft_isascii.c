/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isascii.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 12:55:47 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 12:58:20 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check wether c is a 7-bit unsigned char that fits into the ASCII character
** set.
** ---
** dsc: Check the values between 0-127.
** arg: the character that is checked (int)
** ret: a nonzero value if it's true, zero otherwise (int)
** not: n/a
*/

int	ft_isascii(int c)
{
	return (c >= '\0' && c <= 127);
}
