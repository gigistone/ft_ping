/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isdigit.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 23:39:31 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 23:41:26 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if character is a digit number.
** ---
** dsc: Check if c is between '1' and '9'.
** arg: the character that is checked (int)
** ret: nonzero if true, zero otherwise.
** not: n/a
*/

int	ft_isdigit(int c)
{
	return (c >= '0' && c <= '9');
}
