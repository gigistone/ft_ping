/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tolower.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 01:31:38 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 01:35:42 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Return character in lowercase.
** ---
** dsc: Check if character is in uppercase and return it in lowercase, or return
**		the character.
** arg: the character to put in lowercase (int)
** ret: the character in lowercase or just the character itself (int)
** not: n/a
*/

int	ft_tolower(int c)
{
	return (c >= 'A' && c <= 'Z' ? c + 32 : c);
}
