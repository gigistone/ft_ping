/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isgraph.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 14:18:33 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 14:20:39 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check for any printable character, except space.
** dsc: Check for character between '!' and '~'.
** arg: the character that is checked (int)
** ret: nonzero if true, zero otherwise (int)
** not: n/a
*/

int	ft_isgraph(int c)
{
	return (c >= '!' && c <= '~');
}
