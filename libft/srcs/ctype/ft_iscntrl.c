/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iscntrl.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/19 13:35:17 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/19 13:38:53 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if the character is a control character.
** ---
** dsc: check if character code is between 0 and 31 or equals 127.
** arg: the character that is check (int)
** ret: nonzero if true, zero otherwise (int)
** not: n/a
*/

int	ft_iscntrl(int c)
{
	return ((c >= '\0' && c <= 31) || c == 127);
}
