/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_toupper.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/20 01:21:03 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/20 01:23:58 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Return character in uppercase.
** ---
** dsc: If character is in lowercase, return it in uppercase or just the
** character.
** arg: the character to put in uppercase (int)
** ret: the character in uppercase or just the character (int)
** not: n/a
*/

int	ft_toupper(int c)
{
	return (c >= 'a' && c <= 'z' ? c - 32 : c);
}
