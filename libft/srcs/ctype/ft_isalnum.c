/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_isalnum.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lperson- <lperson-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/18 23:47:05 by lperson-          #+#    #+#             */
/*   Updated: 2020/03/18 23:49:30 by lperson-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
** Check if character is alphanumeric.
** ---
** dsc: Check if it's an alphabetic character or a digit.
** arg: the character that is checked (int)
** ret: nonzero if true, zero otherwise (int)
** not: n/a
*/

int	ft_isalnum(int c)
{
	return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'z') \
	|| (c >= 'A' && c <= 'Z'));
}
