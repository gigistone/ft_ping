#include "ft_ping.h"

/*
	global pvar with default values
*/
struct s_ping_var pvar = {
	.opts.ttl = TTL_DEFAULT,
	.opts.compute_rtt = 1,
	.opts.data_size = PACKET_SIZE_DEFAULT
};

/*
	termination program function.
	Called after SIGINT or count reach 0 (see -c option)
*/
void terminate(void) {
	struct timeval	ping_loop_end;

	close(pvar.sockfd);
	gettimeofday(&ping_loop_end, NULL);
	tvsub(&ping_loop_end, &pvar.stats.timing);
	ft_memcpy(&pvar.stats.timing, &ping_loop_end, sizeof(struct timeval));
	display_summary();
	free_dom_info_values(&pvar.dom_info);

	if (pvar.stats.errors || pvar.stats.pckt_last_received == 0) {
		exit(PING_EXIT_PCKT_ERROR);
	} else {
		exit(PING_EXIT_SUCCES);
	}
}

static void handle_termination(int sig_type) {
    if (sig_type == SIGINT) {
		terminate();
	}
}

/*
	Handles param parsing (get options, display opt errors etc)
*/
static int handle_params(const char **params, size_t len) {
	switch(parse_params(params, len)) {
		case PARSE_RES_OK: {
			if (pvar.opts.show_help) {
				display_help();
				exit(PING_EXIT_OTHER_FAILURE);
			}
			break;
		}
		case PARSE_RES_ERROR: {
			display_help();
			exit(PING_EXIT_ARG_FAILURE);
		}
		case PARSE_RES_NO_DEST: {
			fprintf(stderr, "ping: usage error: Destination address required\n");
			exit(PING_EXIT_RESOLVE_ERROR);
		}
		default: {
			display_help();
			exit(PING_EXIT_ARG_FAILURE);
		}
	}
	return 0;
}


int main(int ac, char const *av[]) {

	handle_params(av + 1, ac - 1);
	
	// this program cannot used without root privileges
	if (getuid() != 0) {
		fprintf(stderr, USER_PRIVILEGE_ERROR_TXT);
		exit(PING_EXIT_OTHER_FAILURE);
	}

	if (resolve_domain(pvar.opts.domain) == -1) {
		exit(PING_EXIT_RESOLVE_ERROR);
	}

	pvar.sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);

	if (pvar.sockfd == -1) {
		free_dom_info_values(&pvar.dom_info);
		exit(PING_EXIT_OTHER_FAILURE);
	}

	if (configure_socket(pvar.sockfd) == -1) {
		free_dom_info_values(&pvar.dom_info);
		exit(PING_EXIT_OTHER_FAILURE);
	}

	display_title(&pvar.dom_info);
	signal(SIGINT, handle_termination);
	gettimeofday(&pvar.stats.timing, NULL);
	handle_ping_loop();
	return (PING_EXIT_SUCCES);
}
