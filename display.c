#include "ft_ping.h"

static char *format_icmp_error_msg(uint8_t icmp_type, uint8_t icmp_code) {

	static char *tab[] = {
		"Time to live exceeded",
		"fragment reassembly time exceeded",
		"Error outbound or message not yet implemented for this icmp type/code",
		"Destination Host Unreachable",
		"Destination Net Unreachable"
	};

	if (icmp_type == ICMP_TIME_EXCEEDED) {
		if (icmp_code == ICMP_EXC_TTL) {
			return tab[0];
		}
		else if (icmp_code == ICMP_EXC_FRAGTIME) {
			return tab[1];
		}
	} else if (icmp_type == ICMP_DEST_UNREACH) {
		if (icmp_code == ICMP_HOST_UNREACH) {
			return tab[3];
		} else if (icmp_code == ICMP_UNREACH_NET) {
			return tab[4];
		}
	}
	return tab[2];
}

void display_title(t_dom_info *dominfo) {
	const unsigned int size = pvar.opts.data_size;
	const unsigned int extra_s = size + ICMP_MINLEN + 20;
	
	printf("PING %s (%s) %u(%u) bytes of data.\n",
		dominfo->addrinf->ai_canonname, dominfo->addr_name,
		size, extra_s);
}

void display_help(void) {
	fprintf(stderr, ""PING_USAGE_TXT"");
}

void display_summary(void) {
	printf("\n--- %s ping statistics ---\n", pvar.dom_info.addr_name);
	printf("%u packets transmitted,", pvar.stats.pckt_sended);
	
	if (pvar.stats.errors) {
		printf(" +%u errors,", pvar.stats.errors);
	}
	
	printf(" %u received, %.0f%% packet loss, time %li ms\n",
		pvar.stats.pckt_received,
		(float)(100 - (float)pvar.stats.pckt_received * 100 / pvar.stats.pckt_sended),
		pvar.stats.timing.tv_sec * 1000 +  pvar.stats.timing.tv_usec / 1000);
	
	if (pvar.stats.pckt_received == 0)
		return;
	
	if (pvar.opts.compute_rtt) {
		float avg = pvar.stats.sum / pvar.stats.pckt_received;
		float avg2 = pvar.stats.sum2 / pvar.stats.pckt_received;
		double mdev = sqrt(avg2 - avg * avg);
		
		printf("rtt min/avg/max/mdev = %.3f/%.3f/%.3f/%.3f ms\n",
			pvar.stats.min,
			avg,
			pvar.stats.max,
			mdev);
	}
}

void	display_ping_stat(t_ping_result *res) {
	if (res->icmphdr.type == ICMP_ECHOREPLY) {
		printf("%u bytes from %s: icmp_seq=%u ttl=%u",
			pvar.opts.data_size + ICMP_MINLEN,
			pvar.dom_info.addrinf->ai_canonname,
			res->sequence,
			res->ttl);
		
		if (pvar.opts.compute_rtt) {
			const float triptime = get_elapsed_time_ms(res->rtt_time);
			printf(" time=%.2f ms\n", triptime);
		}
		else {
			printf("\n");
		}
	}
	else {
		if (pvar.opts.verbose) {
			printf("From %s (%s) icmp_seq=%u icmp_type=%u icmp_code=%u\n",
				pvar.dom_info.addrinf->ai_canonname,
				inet_ntoa(res->iphdr.ip_src),
				res->sequence,
				res->icmphdr.type,
				res->icmphdr.code
			);
		} else {
			printf("From %s (%s) icmp_seq=%u %s\n",
				pvar.dom_info.addrinf->ai_canonname,
				inet_ntoa(res->iphdr.ip_src),
				res->sequence,
				format_icmp_error_msg(res->icmphdr.type, res->icmphdr.code)
			);
		}
	}
}