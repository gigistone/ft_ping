NAME := ft_ping
LIBFT:= libft/libft.a
LIBFT_DIR:= libft

SRCS := main.c icmp.c \
		ping.c domain.c \
		param_parser.c display.c \
		utils.c net.c

OBJS := $(SRCS:.c=.o)
DEP = $(OBJS:.o=.d)

CC := gcc
LD := gcc
LDARGS := -L ./libft -lm -lft
CFLAGS := -Wall -Wextra -Werror -MMD -I . -I ./libft/include
RM := rm -f

.PHONY: clean fclean re all

all: $(LIBFT) $(NAME)

$(NAME): $(OBJS)
	$(LD) $^ -o $@ $(LDARGS)

clean:
	$(RM) $(OBJS) $(DEP)

fclean: clean
	$(RM) $(NAME)

re: fclean all

$(LIBFT):
	$(MAKE) -C $(LIBFT_DIR)

-include $(DEP)