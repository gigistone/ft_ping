#ifndef FT_PING_H
# define FT_PING_H

#include <sys/socket.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/time.h>
#include <netinet/ip_icmp.h>
#include <netinet/ip.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include "lft_string.h"
#include "lft_stdlib.h"

extern struct s_ping_var pvar;

#define PROG_NAME "ft_ping"

#ifdef IPDEFTTL
# define TTL_DEFAULT IPDEFTTL
#else
# define TTL_DEFAULT (64)
#endif

#ifdef MAXTTL
# define TTL_MAX MAXTTL
#else
# define TTL_MAX (255)
#endif

#define INET4_ADDR_STR_LEN	(20)

#define PACKET_SIZE_DEFAULT (64 - ICMP_MINLEN)
#define PACKET_SIZE_MIN (0)
#define PACKET_SIZE_MAX (IP_MAXPACKET - ICMP_MINLEN - 1)

#define get_elapsed_time_ms(t) \
    ((t.tv_sec * 1000) + ((float)t.tv_usec / 1000))

/*
 **
 ** EXIT CODE STATUS
 ** based on gnu implementation
 **
*/
#define PING_EXIT_SUCCES (0)
#define PING_EXIT_ARG_FAILURE (1)
#define PING_EXIT_PCKT_ERROR (1)
#define PING_EXIT_RESOLVE_ERROR (2)
#define PING_EXIT_OTHER_FAILURE (2)


/*
 **
 ** STRING CONSTANTS
 **
*/
#define PING_USAGE_TXT  "Usage\n"\
                        "  ft_ping [options] <destination>\n"\
                        "Options:\n"\
                        "  <destination>\t\tdns name or ip address\n"\
                        "  -c <count>\t\tstop after <count> replies\n"\
                        "  -h\t\t\tprint help and exit\n"\
                        "  -O report outstanding replies\n"\
                        "  -q\t\t\tquiet output\n"\
                        "  -s <size>\t\tuse <size> as number of data bytes to be sent\n"\
                        "  -t <ttl>\t\tdefine time to live\n"\
                        "  -v\t\t\tverbose output\n"

#define USER_PRIVILEGE_ERROR_TXT \
        "Error: This command has to be run "\
		"with superuser privileges (under the root user on "\
		"most systems).\n"

#define PING_ARG_ERR_PREFIX "ft_ping: invalid argument:"


/*
 * an enumeration of each option and its flag equivalent 
*/
enum	e_param_opt {
	POPT_TTL = 't',
    POPT_VERBOSE = 'v',
    POPT_HELP = 'h',
    POPT_PCKT_SIZE = 's',
    POPT_COUNT = 'c',
    POPT_OUTSTAND = 'O',
    POPT_QUIET = 'q',
};

/*
 * an enumeration of each state returned by parse_params function
*/
enum	e_parse_result
{
	PARSE_RES_OK,
	PARSE_RES_ERROR,
	PARSE_RES_NO_DEST,
};

/*
 * an enumeration of each state returned by send_ping function
*/
enum e_ping_result {
    PINGR_OK,
    PINGR_TIMEO,
    PINGR_ERR,
};

/*
 * Contains informations relative to the result
 * of the send_ping function
*/
typedef struct 	s_ping_result {
	float		    rtt_ms;
    struct timeval  rtt_time;
	uint8_t		    ttl;
	uint8_t		    isvalid_rtt;
    uint16_t        sequence;
    struct  ip      iphdr;
    struct icmphdr  icmphdr;
}				t_ping_result;

/*
 * Contain the options relative to the ping
 * command behaviour
*/
typedef struct  s_ping_opt {
    char    *domain;
    uint8_t ttl;
    uint16_t data_size;
    uint32_t count;
    unsigned int    show_help: 1;
    unsigned int    verbose: 1;
    unsigned int    compute_rtt: 1;
    unsigned int    outstanding: 1;
    unsigned int    quiet: 1;
}               t_ping_opt;

/*
 * Represent a ICMP packet (hdr + data)
*/
typedef struct          s_icmp {
    struct icmphdr  hdr;
    uint8_t         msg[PACKET_SIZE_MAX];
}                       t_icmp;

/*
 * Represent an ICMP in-queue data
*/
struct s_icmp_data {
	union {
		char data[PACKET_SIZE_MAX];
		struct {
			struct ip ip;
			struct icmphdr icmphdr;
		} origin;
	} un;
};

/*
 * Contain data about ping sent to host
 * used to display statistics
*/
typedef struct          s_ping_stats {
    uint32_t        pckt_received;
    uint32_t        pckt_sended;
    uint32_t        pckt_last_received;
    uint32_t        errors;
    float           sum;
    float           sum2;
    float           min;
    float           max;
    float           avg;
    float           mdev;
    struct timeval  timing;
}                       t_ping_stats;

/*
 * Contains some infos about a domain name
*/
typedef struct	s_dom_info {
	char	addr_name[INET4_ADDR_STR_LEN];
	struct addrinfo *addrinf;
}				t_dom_info;

/*
 * Encapsulates some variables used at multiple
 * parts in the program. 
 * (options, ping loop status, ping statistics, ...)
*/
struct s_ping_var {
    struct s_ping_opt   opts;
    struct s_ping_stats stats;
    t_dom_info          dom_info;
    int                 sockfd;
};

int	        resolve_domain(const char *domain);
void        free_dom_info(t_dom_info **dom_info);
void	    free_dom_info_values(t_dom_info *dominfo);
void	    configure_icmp_echo(t_icmp *icmp, uint16_t sequence);
int	        configure_socket(int sock_fd);

void        handle_ping_loop(void);
int         send_ping(void);

enum e_parse_result parse_params(const char **params, size_t len);

void tvsub( struct timeval *out, struct timeval *in );
void terminate(void);

void display_help(void);
void display_summary(void);
void display_ping_stat(t_ping_result *res);
void display_title(t_dom_info *dominfo);


uint16_t ft_htons(uint16_t n);
uint16_t ft_ntohs(uint16_t n);
#endif
