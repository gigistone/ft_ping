#include "ft_ping.h"

/*
	compute datagram checksum
*/
static uint16_t checksum(void *b, int len) {
	uint16_t *buf = b;
    unsigned int sum = 0;
    uint16_t result; 
  
    for ( sum = 0; len > 1; len -= 2 )
        sum += *buf++; 
    if ( len == 1 ) 
        sum += *(unsigned char*)buf;
    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
    result = ~sum;
    return result;
}

/*
	Configure icmp echo and add timestamp in data section if
	possible
*/
void	configure_icmp_echo(t_icmp *icmp, uint16_t sequence) {
	struct timeval cur_time;
	ft_memset(icmp, 0, sizeof(t_icmp));
	icmp->hdr.type = ICMP_ECHO;
	icmp->hdr.code = 0;
	icmp->hdr.un.echo.id = ft_htons(getpid());
	icmp->hdr.un.echo.sequence = ft_htons(sequence);
	
	// fill data section if exists
	if (pvar.opts.data_size > 0) {
		for (size_t i = 0; i < (size_t)pvar.opts.data_size - 1; i++) {
			icmp->msg[i] = 0;
		}
		icmp->msg[pvar.opts.data_size - 1] = 2;
	}

	// insert timeval for rtt computing if possible
	if (pvar.opts.compute_rtt) {
		gettimeofday(&cur_time, NULL);
		ft_memcpy(icmp->msg, &cur_time, sizeof(cur_time));
	}

	icmp->hdr.checksum = checksum(icmp, ICMP_MINLEN + pvar.opts.data_size);
}
