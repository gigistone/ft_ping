/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   domain.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/24 16:12:12 by bboutoil          #+#    #+#             */
/*   Updated: 2021/01/30 23:44:42 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ping.h"

void	free_dom_info_values(t_dom_info *dominfo) {
	freeaddrinfo(dominfo->addrinf);
}

void free_dom_info(t_dom_info **dom_info) {
	free_dom_info_values(*dom_info);
	free(dom_info);
	*dom_info = NULL;
}

int	resolve_domain(const char *domain) {
	struct addrinfo hint = {0};
	int				errcode;

	hint.ai_family = AF_INET;
	hint.ai_flags = AI_CANONNAME;

	if ((errcode = getaddrinfo(domain, "80", &hint, &pvar.dom_info.addrinf)) != 0) {
		fprintf(stderr, "ft_ping: %s: ", domain);
		switch(errcode) {
			case EAI_NONAME:
				fprintf(stderr, "Name or service not known\n");
				break;
			case EAI_FAMILY:
				fprintf(stderr, "ai_family not supported\n");
				break;
			case EAI_AGAIN:
				fprintf(stderr, "Temporary failure in name resolution\n");
				break;
			default:
				fprintf(stderr, "Resolve domain error\n");
				break;
		}
		return (-1);
	}

	inet_ntop(AF_INET, (const void *)&pvar.dom_info.addrinf->ai_addr->sa_data[2],
		pvar.dom_info.addr_name, sizeof(struct sockaddr));
	return (0);
}
